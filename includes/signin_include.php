<?php

session_start();

if(isset($_POST['submit'])){
	include ('../config.php');
	include ('../db_connection.php');
	$username = mysqli_real_escape_string($dbCon, $_POST['username']);
	$pwd = mysqli_real_escape_string($dbCon, $_POST['password']);

	//check if both fields are filled
	if (empty($username) || empty ($pwd)){
		
		header("Location: ../log-in/?login=emptyfield");
		exit();

		}		
	 else {
	 	//loop for searching three tables
		$tables = array("student", "teacher", "admin");
		$verify = 0;
		$y = 0;
		for($x = 0; $x <=2; $x++){
			$sql = "SELECT * FROM " . $tables[$x] . " WHERE username = '$username'";
			$result = mysqli_query($dbCon, $sql);
			$resultChk = mysqli_num_rows($result);
			$y = $x;
			if($resultChk >= 1){
				$x = 2;
				$verify = 1;
			}
		}
		if ($verify < 1){
			header("Location: ../log-in/?login=noDatainDB");
			exit();
		} else {
			//verify hashed passwords
			if ($row = mysqli_fetch_assoc($result)){
				$pwdCheck2 = password_verify($pwd, $row['password']);
				if($row['password'] == 'password' AND $pwd == 'password'){
					if($row['user_type'] == 1){
						
						$_SESSION['u_id'] = $row['admin_id'];
						$_SESSION['u_fname'] = $row['first_name'];
						$_SESSION['u_lname'] = $row['last_name'];
						$_SESSION['user_type'] = $row['user_type'];
						$_SESSION['username'] = $row['username'];
						header("Location: ../log-in/changepwd_index.php");
					}
					if($row['user_type'] == 2){
						
						$_SESSION['u_id'] = $row['teacher_id'];
						$_SESSION['u_fname'] = $row['first_name'];
						$_SESSION['u_lname'] = $row['last_name'];
						$_SESSION['user_type'] = $row['user_type'];
						$_SESSION['username'] = $row['username'];
						header("Location: ../log-in/changepwd_index.php");
					}
					if($row['user_type'] == 3){
						
						$_SESSION['u_id'] = $row['student_id'];
						$_SESSION['u_fname'] = $row['first_name'];
						$_SESSION['u_lname'] = $row['last_name'];
						$_SESSION['user_type'] = $row['user_type'];
						$_SESSION['username'] = $row['username'];
						header("Location: ../log-in/changepwd_index.php");
					}
					
				}
				elseif($pwdCheck2 == false){
					header("Location: ../log-in/?login=error");
				}
				elseif ($pwdCheck2 == true){
					//check if account is using default password
					$ifDefaultPwd = true;
					$c = $row['default_pw'];
					if($c == 0){
						$ifDefaultPwd = false;
					}else{
						$ifDefaultPwd = true;
					}
					//login the user according to usertype
					if (($row['user_type']) == 1 or ($row['password'] == 'password')){
						if($ifDefaultPwd == true){
							header("Location: ../log-in/changepwd_index.php");
							$_SESSION['u_id'] = $row['admin_id'];
							$_SESSION['u_fname'] = $row['first_name'];
							$_SESSION['u_lname'] = $row['last_name'];
							$_SESSION['user_type'] = $row['user_type'];
							$_SESSION['username'] = $row['username'];
						}elseif($ifDefaultPwd==false){
							$_SESSION['u_id'] = $row['admin_id'];
							$_SESSION['u_fname'] = $row['first_name'];
							$_SESSION['u_lname'] = $row['last_name'];
							$_SESSION['user_type'] = $row['user_type'];
							$_SESSION['username'] = $row['username'];
							header("Location: ../admin/");
							exit();
						}
					}
					elseif ($row['user_type'] == 2){
						if($ifDefaultPwd == true){
							header("Location: ../log-in/changepwd_index.php");
							$_SESSION['u_id'] = $row['teacher_id'];
							$_SESSION['u_fname'] = $row['first_name'];
							$_SESSION['u_lname'] = $row['last_name'];
							$_SESSION['user_type'] = $row['user_type'];
							$_SESSION['username'] = $row['username'];
						}elseif($ifDefaultPwd==false){
							$_SESSION['u_id'] = $row['teacher_id'];
							$_SESSION['u_fname'] = $row['first_name'];
							$_SESSION['u_lname'] = $row['last_name'];
							$_SESSION['user_type'] = $row['user_type'];
							$_SESSION['username'] = $row['username'];
							header("Location: ../teachers/");
							exit();
						}
					}
					elseif ($row['user_type'] == 3){
						if($ifDefaultPwd == true){
							header("Location: ../log-in/changepwd_index.php");
							$_SESSION['u_id'] = $row['student_id'];
							$_SESSION['u_fname'] = $row['first_name'];
							$_SESSION['u_lname'] = $row['last_name'];
							$_SESSION['user_type'] = $row['user_type'];
							$_SESSION['username'] = $row['username'];
						}elseif($ifDefaultPwd==false){	
							$_SESSION['u_id'] = $row['student_id'];
							$_SESSION['u_fname'] = $row['first_name'];
							$_SESSION['u_lname'] = $row['last_name'];
							$_SESSION['user_type'] = $row['user_type'];
							$_SESSION['username'] = $row['username'];
							header("Location: ../students/");
							exit();
						}
					}
				}	
			}
		}
	}
} else {
	header("Location: ../log-in/?submitbuttonfail");
	exit();
}



?>