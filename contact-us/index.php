<?php include("../partials/header.php"); ?>
<?php 
//    <----PHPmailer---->
require '../vendor/phpmailer/phpmailer/src/PHPMailer.php';
//    <----SMTP---->
require '../vendor/phpmailer/phpmailer/src/SMTP.php';

//    <----Exception---->
require '../vendor/phpmailer/phpmailer/src/Exception.php';

//    <----Composer---->
require '../vendor/autoload.php'; ?>
<?php
    $sql = "SELECT * FROM general_settings WHERE gs_id=1";
     $result = mysqli_query($dbCon, $sql);
    if (mysqli_num_rows($result) > 0) { $row = mysqli_fetch_assoc($result); }
     else $row = array("gs_name"=>"", "gs_address"=>"", "gs_contact_number"=>"", "gs_contactUs_recepient_email"=>"");
      $add = $row['gs_address'];
      $contact= $row['gs_contact_number'];
      $name= $row['gs_name'];
      $imap = $row['gs_map_iframe'];
      $receiver = $row['gs_contactUs_recepient_email'];
    ?>

      <!--Script REcaptcha -->
  <script src='https://www.google.com/recaptcha/api.js'></script>

    <section id="contact-us" class="bg-grey py-5">
      <div class="container">
        <div class="row">
          <div class="col-lg-6">
            <h2>Contact Us</h2>
            <p class="my-0"><?php echo $name ?></p>
            <p class="my-0"><?php echo $add ?></p>
            <p class="my-0"><?php echo $contact ?></p>
            <form class="mt-3 pr-0 pr-lg-5 mb-5 mb-lg-0" action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
              <div class="form-group">
                <label for="form-name">Name</label>
                <input type="text" class="form-control form-control-sm" name="name" id="form-name" aria-describedby="">
              </div>
              <div class="form-group">
                <label for="form-email">Email</label>
                <input type="email" class="form-control form-control-sm" name="email" id="form-email" aria-describedby="">
              </div>
              <div class="form-group">
                <label for="form-message">Message</label>
                <textarea type="textfield" class="form-control form-control-sm" name="message" id="form-message" rows="5"></textarea>
              </div>
              <div class="form-group">
                <div class="g-recaptcha" data-sitekey="6LdBMHAUAAAAAI4ljHAkGKvuKneSD7fmplgZvI6c"></div>
              </div>
              <button name="submit" type="submit" class="btn btn-primary bg-theme-color">Submit</button> </br>
              <?php
                if ($_SERVER["REQUEST_METHOD"] == "POST"){
                    // access
                      $secretKey = '6LdBMHAUAAAAANALCYuKboAc-TS3RCRoMYwtt0W1';
                      $captcha = $_POST['g-recaptcha-response'];

                      if(!$captcha){
                        echo '</br><div class="alert alert-warning alert-dismissible" role="alert"> <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                            </button>
                            Please check the the captcha form.</div>';
                        exit;
                      }
                      $ip = $_SERVER['REMOTE_ADDR'];
                      $response=file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=".$secretKey."&response=".$captcha."&remoteip=".$ip);
                      $responseKeys = json_decode($response,true);

                      $mail = new PHPMailer\PHPMailer\PHPMailer;
                      //Server Settings 
                      $mail->IsSMTP();
                      $mail->Host = 'smtp.gmail.com';
                      $mail->SMTPAuth = true;
                      $mail->Username= 'pacocatholicschoolhelpdesk@gmail.com';
                      $mail->Password= 'pixl8technologies';
                      $mail->SMTPSecure='tls';
                      $mail->Port = 587;
                      // Post variables
                      $name = $_POST['name'];
                      $email= $_POST['email'];
                      $message= $_POST['message'];
                    
                if ( empty($name) OR !filter_var($email, FILTER_VALIDATE_EMAIL) OR empty($message)) {
                            # Set a 400 (bad request) response code and exit.
                            echo '</br><div class="alert alert-warning alert-dismissible" role="alert"> <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                            </button>
                            Please complete the form and try again.</div>';
                        }

                  if(intval($responseKeys["success"]) !== 1) {
                    echo '</br><div class="alert alert-warning alert-dismissible" role="alert"> <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                            </button>
                            Please check the the captcha form.</div>';
                    

                    } else{
                      $name = $_POST['name'];
                      $email= $_POST['email'];
                      $message= $_POST['message'];
                      $headers = "Sender Name: " . $name ."\r\nSender Email: ". $email;
                      $body = $headers . "\r\nMessage:  " . $message;
                      

                          //Recipients
                          $mail->AddReplyTo($email,'[Inquiry Sender]'); //<-email to be replied to
                          $mail->setFrom('pacocatholicschoolhelpdesk@gmail.com', 'Paco-Inquiry'); //<-gmail used for sending and subject
                            $mail->addAddress( $receiver,'Report'); //<-email Admin

                            //Content
                            $mail->Subject= "This is an Automated Message";
                            $mail->Body = $body;

                            if($mail->send()){
                                  echo '</br><div class="alert alert-success alert-dismissible" role="alert"> <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                            </button>
                            Thank You! Your message has been sent.</div>';
                                 
                            }
                            else{
                              echo '</br><div class="alert alert-warning alert-dismissible" role="alert"> <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                            </button>
                            Oops! Something went wrong, we couldnt send your message.</div>';
                              }  
                      }
                    }
                ?>
            </form>

          </div>
          <div class="col-lg-6">
            <?php
            $sql = "SELECT * FROM general_settings WHERE gs_id=1";
            $result = mysqli_query($dbCon, $sql);
            if (mysqli_num_rows($result) > 0) { $row = mysqli_fetch_assoc($result); }
            else $row = array("gs_name"=>"", "gs_address"=>"", "gs_contact_number"=>"");
            $add = $row['gs_address'];
            $contact= $row['gs_contact_number'];
            $imap = $row['gs_map_iframe'];
            ?>
            <h2>Map</h2>
            <p id="map-is-loaded" class="d-none">Click on the map to explore</p>
            <p id="map-is-loading">Map is loading...</p>
            <?php echo $imap; ?>
          </div>
        </div>
      </div>
    </section>

<?php include("../partials/footer.php"); ?>