REPLACE INTO `general_settings` (
  `gs_id`,
  `gs_name`,
  `gs_logo_url`,
  `gs_logo_alt_name`,
  `gs_founded_content`,
  `gs_address`,
  `gs_contact_number`,
  `gs_facebook_link`,
  `gs_twitter_link`,
  `gs_instagram_link`,
  `gs_about_history_content`,
  `gs_about_mv_content`,
  `gs_about_corevalues_content`,
  `gs_about_administration_content`,
  `gs_about_personnel_content`,
  `gs_map_iframe`,
  `gs_contactUs_recepient_email`,
  `gs_home_featured_img_url`,
  `gs_home_featured_content`) VALUES (
  1,
  'Paco Catholic School',
  'paco-catholic-school-logo.png',
  'Paco Catholic School Logo',
  'Manila 1912',
  '1521 Paz St. Paco, 1007 Manila, Philippines',
  'Tel Nos. 1234567 &vert; 1234567',
  'http://www.facebook.com/paco-catholic-school',
  'http://www.twitter.com/paco-catholic-school',
  'http://www.instagram.com/paco-catholic-school',
  '<h4>History of Paco Catholic School</h4>
    <br>
    <p>In 1580, the early Franciscan missionaries founded the town of Dilao (now known as Paco), located on the left side of the Pasig River, bounded by Pandacan on the North, Sta. Ana on the East, Malate on the South and Ermita on the West. Ten years later, Paroquia de Dilao was established with Rev. Fray Juan de Garrobillas as its first Parish Priest.</p>
    <p>In 1762, the Parish was relocated near the Pasig River. Years later, the Franciscan Superior Governor incorporated the two smaller towns of Santiago and Pena de Francia (Penafrancia) to the existing parish. The expanded parish was then transferred where Our Lady of Penafrancia church now stands. Finally, the Franciscan Superior Governor ordered the new town to be called San Fernando de Dilao.</p>
    <p>After the Spanish Franciscans left in 1900, the Archdiocese of Manila entrusted the parish to the Belgian Scheut Missionaries, popularly known as the CICM (Congregatio Immaculti Cordis Mariae/Congregation of the Immaculate Heart of Mary). CICM managed the parish from 1908-1984.</p>
    <p>Fr. Raymond Esquenet, was the first CICM to be appointed parish priest of Paco by the Belgian Superior of the Order. He took over the management of the parish in Oct. 1908 with Fr. Maurice Lefebvre as his assistant. Since the last Spanish-built church in the present site was destroyed and completely burned during the Spanish-American war in Feb. 1899, the parisioners had to go to a small chapel at the corner of J. Zamora and Canonigo Sts. (now Quirino Avenue extension) for church services for the next 9 years.</p>
    <p>Meantime, Fr. Esquenet made use of a small chapel along Penafrancia Street, which became an extension of the Parish and where he started a small school for about 50 children.</p>
    <p>After Fr. Esquenet was assigned to another parish in Lipa, Batangas in Sept. 1912, Fr. Godfried (Godofredo Aldenhuijsen, popularly known as Fr. Godo, took over the parish. Aside from the parish work, Fr. Godofredo continued what Fr. Esquenet had started educating the young in his small chapel.</p>
    <p>During the time of Fr. Godofredo, Paco Catholic School emerged as an institution to reckon with, becoming the largest parochial school in the Far East. He also established other institutions, like the Pasig Catholic College in 1913, St. Andrew School 1917 and Cainta Catholic College in 1961.</p>
    <p>In June 1913, following a marked increase in enrolment, Fr. Godofredo asked the Belgian mothers (CMSA now ICM) stays from St. Theresa’s College to help run the school in Penafrancia. Thereafter, enrolment steadily increased by 1 grade level every year until the primary course (Grade 1-4) was completed. Finally in 1916, the grade school was finally recognized by the government.</p>
    <p>When Fr. Godofredo was transferred to Pasig in September 1919, Fr. Josef Billiet became the parish priest of Paco, a position he held for 10 years. Finding it too inconvenient to administer a growing school, which was quite a distance from the convent of Paco, he had a wooden building of 5 rooms constructed along Trece de Agosto Street on the north and along the estero on the south of the present site of Paco Catholic School. The students of Penafrancia transferred to the new building in the early 1920’s.</p>
    <p>March 1931, marked the return of Fr. Godofredo to Paco after an absence of 12 years. In 1932, the old church, started by Fr. Esquenet in 1908 and completed by Fr. Godofredo in 1912, was reconverted into four classrooms in 1932. A second floor was added to serve as the Mother’s convent. On May 21, 1933, the nuns came to leave permanently in the church school compound.</p>
    <p>In 1933, the intermediate level (Grades 5-7) was granted government recognition. A year after, a three-storey concrete edifice was constructed in the first year course in high school was offered. Paco Catholic School accepted its first 13 students in the new high school building, named the Sacred Heart building, now the Fr. Godofredo Aldenhuijsen Heritage Center. From then on, one year level was added every year.</p>
    <p>As then, 13 freshmen formed the first batch of high school graduating class for S.Y. 1937-1938. Paco Catholic School then received her full recognition for a secondary course from the government.</p>
    <p>When World War II broke out in the Pacific on December 8, 1941, the school closed. However, in 1942, on the occasion of the pastoral visitation, the archbishop of Manila insisted that at least the high school level be reopened.</p>
    <p>In July 1944, Fr. Joseph de Bal temporarily became the school director. Fr. Godofredo being a Dutch, was detained with other foreign nationals in Laguna. Later, Fr. Godofredo was deposed as the Parish Priest by order of the Japanese authorities, but was reinstated on the same day through the intervention of the Archbishop. Thus, the school was placed under the supervision of the Archdiocese of Manila. The battles of liberation forced the school to close again in September 1944.</p>
    <p>At the end of the war in 1945, Fr. Godofredo returned to Paco. The devastation of the church and the school buildings was so extensive that a canvass roof had to be placed over the old church. The place served as a temporary church on Sundays and as a school on weekdays. With the help of American engineers, more repairs were made. Paco Catholic School was able to start classes in July 1945 with 1,500 students. To accommodate all the students, the double session was introduced girls in the morning and boys in the afternoon.</p>
    <p>Repairs on the damaged buildings were made between 1946 and 1955, including the conversion of the Mother’s convent into a girl’s high school building.</p>
    <p>The CICM Fathers and the Belgian Mothers (now ICM Sisters) continued their administration of the school, with the population, reaching 7, 000 in 1964. Six years later, a new rectory of the Sisters and the five-storey St. Joseph’s Building, which is still being used at present were constructed. The Belgian ICM sisters decided to withdraw their involvement in PCS, leaving their Filipino counterpart as Principal of the Grade School Department. From 1970 to 1984, the CICM Fathers remained as Directors of the school while the principalship of both Grade School and High School departments was given to lay administrators.</p>
    <p>Fr. Carlos Van Ooteghem, the last CICM parish priest to serve PCS, managed the school from 1980-1984. He stayed on as Coadjutor in the parish until that the Karel Hall, the Covered Court, Fr. Godofredo and Practical Arts (PA) buildings were constructed. After 72 years of dedicated service to the ministry and education, the CICM turned over the management of the school to the Archdiocese of Manila in 1984.</p>
    <p>Auxiliary Bishop of Manila, Teodoro Bacani Jr., became the first Filipino Director. He managed the school from 1984-1993. Fr. Danilo Canceran succeeded Bishop Bacani as School Director in June 1993.</p>
    <p>In 1995, the five-storey San Lorenzo (SLR) Building, which replaced the Department of Religious Education (DRE) building, was built under the administration of Parish Priest Bishop Teodoro Bacani and School Director Fr. Danilo Canceran.</p>
    <p>The population in PCS continued to increase, especially when Kindergarten 1 was introduced in 1995. The start of the Nursery Level was a year later.</p>
    <p>In 1996, Monsignor Domingo A. Cirilos Jr. was appointed Parish Priest and Director of the institution. In less than a year of his incumbency, he had the altar of the church renovated.</p>
    <p>In 1997, the two old buildings along Trece de Agosto up to the estero were demolished to make way for a new five-storey Pope John Paul II (PJP II) Building, replacing the Holy Cross and Our Lady’s Building. This was completed in 1998. It was also in the same year when the Early Childhood Education (Nursery, Kinder and Preparatory) was completed.</p>
    <p>May 1999, saw the construction of the Jaime Cardinal Sin (JCS) building, housing 33 rooms for the high school department and a 1000-seater auditorium, completed 1 year and 2 months later. The modern structure was blessed in July 14, 2000 fittingly by Cardinal Sin himself.</p>
    <p>During the 89th PCS Foundation Day Celebration, Parish Priests and School Director Msgr. Domingo A. Cirilos Jr. led the groundbreaking ceremony for the construction of a 10-storey school edifice. The magnificent structure, named after Blessed, now Saint Pedro Calungsod, was blessed and inaugurated on November 8, 2002 in a grand ceremony coinciding with the 19th PCS Foundation Day Anniversary Celebration.</p>
    <p>After undergoing a series of construction, the school now stands with pride, giving Paco district’s skyline a new profile. The school has continuously improved its facilities and maintained its high standard of education through the years by keeping abreast with changes and trends in education.</p>
    <p>With the steady growth of the student population and recognition of the critical role of learning in the early years, Paco Catholic School made the Early Childhood Education (ECE) a separate department from the grade school in June 2003. In June 2006, the Special Education Department (SPED) was created in response to the special needs of children who are cognitively capable but behaviorally changed. Dr. Loida L. Hilario, then the principal of the ECE Department, was appointed principal of the new department.</p>
    <p>In May 2010, Msgr. Rolando R. Dela Cruz was appointed Parish Priest and School Director, a position he continues to serve up to the present. Upon his appointment, initiated, among other things, the improvement in the artistic design of the church’s sanctuary, impeccably redefining it. As school head, he continued the great legacy of his predecessors while at the same time, introducing new elements in the current academic formation and school practices that would address the needs of the present time. It will be during his term, perhaps as by Providence, that the school Centennial, a very important milestone in the existence of PCS, was celebrated.</p>
    <p>With the implementation of the K to 12 Curriculum in School Year 2011-2012, ECE was integrated with the Grade School Department while the Special Education Program was placed under the School Director.</p>
    <p>Beginning school year 2012-2013, air conditioning units were installed in all classrooms to improve the learning environment. Then, the following constructions and renovations/relocations took place: The Our Lady of Candelaria Chapel, the waiting area near the Grade School gate, the grade school faculty room, the book room; and the offices of the Center for Christian Formation (CCF), Maintenance, Registrar, Human Resource Management & Development (HRMD), and the Saint Pedro Calungsod Buildding. Subsequently, the Instructional Media Center was also air conditioned. All these were for the improvement of the delivery of services to the clientele.</p>
    <p>Major changes in the Roman Catholic Archbishop of Manila Educational System (RCAM-ES) were introduced in Academic Year 2014-2015 among these were the separation of the management of the school from the parish and the clustering of school under the supervision of one school director with an assistant director resulting in Paco Catholic School placed in Cluster 1 with, Fr. Maxell Lowell C. Aranilla as School Director, Fr. Rany P. Geraldino as Assistant Director for Finance and Administration and Mrs. Dina B. Abariso as Assistant Director for Academics. In School Year 2015-2016, Fr. Lorenz Moises J. Festin took the place of Fr. Rany P. Geraldino as Assistant Director for Finance and Administration. It is under their leadership that the ECE and SPED Area were officially placed under the Grade School Department. Likewise, Senior High School, as an integral part of the K to 12 program was established.</p>
    <p>Today, Paco Catholic School continues to provide its clientele with quality catholic education, the very mission that brought about its birth a century ago.</p>',
  '<h4>Vision</h4>
    <p>A People called the Father in Jesus Christ to be Community of the persons with Fullest of Life witnessing to the Kingdom of God by living the Paschal mystery in the power of the Holy Spirit with Mary as Companion.</p>
    <h4>Identity</h4>
    <p>Paco Catholic School, an evangelizing arm of the church is an institution of learning and formation, offering quality catholic education. It envisions its students to be total persons, sensitive to the plight of the poor and responsive to the needs of the dynamic Philippine society and global challenges.</p>
    <h4>Mission</h4>
    <p>We commit too ourselves to:</p>
    <ol>
    <li>Make PCS a home and institution of excellent quality education with emphasis on Gospel values integrated in all learning experiences;</li>
    <li>Nurture within the community an atmosphere of service and genuine concern for the upliftment of the deprived, depressed and underprivileged;</li>
    <li>Provide opportunities and tools for the wholistic development of students to make them locally effective and globally competitive;</li>
    <li>Establish effective linkages with the home and the community to sustain efficient and responsible stewardship of God’s creation.</li>
    </ol>
    <h6>Institutional Goals</h6>
    <ol>
    <li>To provide in its curricular offerings learning experiences deeply rooted in Gospel values.</li>
    <li>To deepen within the community the love for Christian service and concern for our less fortunate brethren.</li>
    <li>To harness students’ potentials and talents in all areas of endeavor, making them highly competitive.</li>
    <li>To create an environment where students develop a sense of duty and purpose, personal, civic and moral responsibility and commitment to God and country through responsible stewardship.</li>
    </ol>
    <h6>Objectives of the Institution</h6>
    <p>This institution seeks to produce:<p>
    <ol>
    <li>A morally upright person with unwavering faith in God and constant love for his fellowmen.</li>
    <li>An individual who values himself/herself in order to preserve family unity and to efficiently discharge his responsibilities.</li>
    <li>A Filipino citizen who is proud of his/her race and his culture and works to promote world peace and unity in society.</li>
    <li>An individual who pursues an honest living, loves things Philippines and is responsive to the needs and changes of the times.</li>
    <li>A Filipino citizen who loves and willingly serves the Republic of the Philippines, intelligently, intelligently exercise his/her individual and collective rights and faithfully practices the ideal of democracy.</li>
    <li>A person who fosters harmony, goodwill and brotherhood among the people of the world.</li>
    <li>An individual who lives healthily, uses his/her leisure time wisely to be physically fit for the development of self and community.</li>
    </ol>',
  '<h4>Core Values</h4>
    <br>
    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quisque id diam vel quam elementum pulvinar etiam. Risus quis varius quam quisque. In est ante in nibh mauris cursus. Orci porta non pulvinar neque laoreet suspendisse interdum. Eget gravida cum sociis natoque. Sollicitudin nibh sit amet commodo nulla facilisi nullam vehicula ipsum. Leo a diam sollicitudin tempor id. Sit amet nisl purus in mollis nunc sed. Non consectetur a erat nam at lectus. Amet mauris commodo quis imperdiet massa tincidunt nunc pulvinar. Id faucibus nisl tincidunt eget. Diam vulputate ut pharetra sit amet. Dolor magna eget est lorem ipsum dolor sit. Pulvinar elementum integer enim neque. Volutpat blandit aliquam etiam erat. Egestas sed tempus urna et pharetra pharetra massa massa.</p>
    <p>Sagittis purus sit amet volutpat consequat mauris nunc congue nisi. Interdum posuere lorem ipsum dolor. A arcu cursus vitae congue mauris rhoncus aenean vel. Pulvinar sapien et ligula ullamcorper malesuada. Diam phasellus vestibulum lorem sed risus ultricies tristique nulla. Nisl suscipit adipiscing bibendum est ultricies integer quis auctor elit. Dis parturient montes nascetur ridiculus. Arcu non sodales neque sodales ut etiam sit amet nisl. Leo a diam sollicitudin tempor id eu nisl nunc mi. Arcu dictum varius duis at consectetur. Quis imperdiet massa tincidunt nunc pulvinar sapien. Risus nullam eget felis eget nunc lobortis mattis aliquam. Quisque egestas diam in arcu. Nullam eget felis eget nunc lobortis mattis.</p>
    <p>Eu mi bibendum neque egestas congue quisque egestas diam. Dolor sed viverra ipsum nunc aliquet bibendum enim facilisis gravida. Vulputate eu scelerisque felis imperdiet proin fermentum leo vel. Odio euismod lacinia at quis risus sed vulputate odio. Diam volutpat commodo sed egestas egestas fringilla phasellus. Viverra maecenas accumsan lacus vel facilisis volutpat est velit egestas. Viverra vitae congue eu consequat. Scelerisque purus semper eget duis at tellus. Turpis egestas sed tempus urna et. Feugiat scelerisque varius morbi enim nunc faucibus. Maecenas ultricies mi eget mauris pharetra et.</p>',
  '<h4>Administration</h4>
    <br>
    <p>Sapien et ligula ullamcorper malesuada proin libero. At elementum eu facilisis sed odio morbi. Dignissim diam quis enim lobortis scelerisque fermentum dui. Egestas purus viverra accumsan in nisl nisi scelerisque. Amet venenatis urna cursus eget nunc. Arcu non sodales neque sodales. Cum sociis natoque penatibus et magnis dis parturient. Potenti nullam ac tortor vitae. Malesuada nunc vel risus commodo viverra maecenas accumsan. Amet justo donec enim diam vulputate ut pharetra sit amet. Tellus pellentesque eu tincidunt tortor aliquam nulla facilisi cras. Nibh venenatis cras sed felis eget velit. Habitasse platea dictumst quisque sagittis purus sit amet volutpat. Sit amet purus gravida quis blandit turpis. Amet facilisis magna etiam tempor orci eu lobortis elementum nibh. Facilisis mauris sit amet massa vitae tortor condimentum lacinia quis. Risus viverra adipiscing at in tellus integer feugiat. Leo integer malesuada nunc vel risus commodo viverra.</p>
    <p>A diam maecenas sed enim ut sem viverra aliquet eget. At volutpat diam ut venenatis tellus in metus vulputate. Ut aliquam purus sit amet luctus venenatis lectus magna fringilla. Senectus et netus et malesuada fames ac turpis egestas maecenas. Arcu non odio euismod lacinia at quis risus sed. Vitae justo eget magna fermentum iaculis eu non diam. Placerat vestibulum lectus mauris ultrices eros in. Nunc congue nisi vitae suscipit tellus mauris a diam maecenas. Mi sit amet mauris commodo. Mauris pellentesque pulvinar pellentesque habitant morbi tristique senectus et. Orci a scelerisque purus semper eget duis. Proin libero nunc consequat interdum varius sit amet mattis.</p>
    <p>Eu volutpat odio facilisis mauris sit amet massa vitae. Dictumst vestibulum rhoncus est pellentesque elit ullamcorper dignissim cras. Adipiscing at in tellus integer feugiat. Eleifend mi in nulla posuere. Vitae auctor eu augue ut lectus arcu bibendum at. Aliquam ultrices sagittis orci a scelerisque purus semper eget. Ac feugiat sed lectus vestibulum mattis ullamcorper. Faucibus purus in massa tempor nec feugiat nisl. Pellentesque sit amet porttitor eget. Fermentum odio eu feugiat pretium nibh ipsum consequat nisl vel. Est lorem ipsum dolor sit amet consectetur adipiscing. Egestas diam in arcu cursus euismod quis viverra nibh. Cras adipiscing enim eu turpis egestas pretium aenean pharetra magna. Eu facilisis sed odio morbi quis commodo odio.</p>
    <p>Neque vitae tempus quam pellentesque nec nam aliquam. Id faucibus nisl tincidunt eget nullam non nisi est. Pharetra et ultrices neque ornare aenean euismod. Lobortis scelerisque fermentum dui faucibus in ornare. Senectus et netus et malesuada. Tellus molestie nunc non blandit massa enim nec. Orci ac auctor augue mauris augue neque gravida. Urna duis convallis convallis tellus. Fusce id velit ut tortor. Fusce ut placerat orci nulla pellentesque dignissim enim. Id eu nisl nunc mi ipsum.</p>',
  '<h4>Personnel</h4>
    <br>
    <p>Duis at consectetur lorem donec. Arcu ac tortor dignissim convallis aenean. Lectus vestibulum mattis ullamcorper velit sed ullamcorper morbi tincidunt ornare. Donec et odio pellentesque diam volutpat. Convallis convallis tellus id interdum velit. Lorem ipsum dolor sit amet consectetur adipiscing elit. Nisi lacus sed viverra tellus in hac habitasse platea. Risus sed vulputate odio ut enim blandit. Neque gravida in fermentum et sollicitudin ac orci phasellus. Id consectetur purus ut faucibus pulvinar elementum integer enim. Cursus euismod quis viverra nibh cras. Massa ultricies mi quis hendrerit dolor magna eget. Mi quis hendrerit dolor magna eget est lorem ipsum. Eu sem integer vitae justo eget magna fermentum iaculis. Nunc vel risus commodo viverra maecenas accumsan lacus vel facilisis. Consectetur a erat nam at lectus urna duis convallis convallis. Aliquam id diam maecenas ultricies mi eget. Ac ut consequat semper viverra nam libero justo. Amet consectetur adipiscing elit ut aliquam purus. Mauris a diam maecenas sed enim ut sem viverra aliquet.</p>
    <p>Nulla aliquet porttitor lacus luctus accumsan tortor. Pretium aenean pharetra magna ac. Habitant morbi tristique senectus et netus et malesuada. Sociis natoque penatibus et magnis dis. Sed augue lacus viverra vitae congue eu consequat ac felis. In cursus turpis massa tincidunt dui ut. Dui id ornare arcu odio ut sem. Risus sed vulputate odio ut enim blandit volutpat maecenas. Ut porttitor leo a diam sollicitudin tempor id. Feugiat in fermentum posuere urna. Arcu bibendum at varius vel pharetra vel turpis nunc. In tellus integer feugiat scelerisque varius. Vulputate dignissim suspendisse in est ante in nibh. Nulla aliquet porttitor lacus luctus. Et tortor at risus viverra adipiscing at in tellus. Dui faucibus in ornare quam viverra orci sagittis eu. Integer eget aliquet nibh praesent tristique. At urna condimentum mattis pellentesque id nibh tortor id aliquet. Donec massa sapien faucibus et molestie ac.</p>
    <p>Eu volutpat odio facilisis mauris sit amet massa vitae. Dictumst vestibulum rhoncus est pellentesque elit ullamcorper dignissim cras. Adipiscing at in tellus integer feugiat. Eleifend mi in nulla posuere. Vitae auctor eu augue ut lectus arcu bibendum at. Aliquam ultrices sagittis orci a scelerisque purus semper eget. Ac feugiat sed lectus vestibulum mattis ullamcorper. Faucibus purus in massa tempor nec feugiat nisl. Pellentesque sit amet porttitor eget. Fermentum odio eu feugiat pretium nibh ipsum consequat nisl vel. Est lorem ipsum dolor sit amet consectetur adipiscing. Egestas diam in arcu cursus euismod quis viverra nibh. Cras adipiscing enim eu turpis egestas pretium aenean pharetra magna. Eu facilisis sed odio morbi quis commodo odio.</p>',
  '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3861.3496956144054!2d120.99260724985378!3d14.579138889766162!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3397c98f393d3741%3A0xa0db21ce9d2a4645!2sPaco+Catholic+School+Service!5e0!3m2!1sen!2sph!4v1532772624710" width="100%" height="350" frameborder="0" style="border:0" allowfullscreen></iframe>',
  'reynellbobihis@gmail.com',
  'pacocs-featured.jpg',
  '<p class="lead">This is a great place to talk about your webpage. This template is purposefully unstyled so you can use it as a boilerplate or starting point for you own landing page designs! This template features:</p>
    <ul>
    <li>Clickable nav links that smooth scroll to page sections</li>
    <li>Responsive behavior when clicking nav links perfect for a one page website</li>
    <li>Bootstrap\'s scrollspy feature which highlights which section of the page you\'re on in the navbar</li>
    <li>Minimal custom CSS so you are free to explore your own unique design options</li>
    </ul>'
  );

DELETE FROM `image_carousel`;

INSERT INTO `image_carousel` ( `img_url`, `img_order_number`) VALUES
  ('pacocs-carousel-image-1.jpg', 1),
  ('pacocs-carousel-image-2.jpg', 2),
  ('pacocs-carousel-image-3.jpg', 3);



  -- Classes
DELETE FROM `class`;

INSERT INTO `class` ( `class_name` ) VALUES
  ('1 - Einstein'),
  ('1 - Newton'),
  ('1 - Curie'),
  ('1 - Galilei'),
  ('1 - Hawking'),
  ('2 - Darwin'),
  ('2 - Tesla'),
  ('2 - Edison'),
  ('2 - Copernicus'),
  ('2 - Faraday'),
  ('3 - Da Vinci'),
  ('3 - Boyle'),
  ('3 - Graham Bell'),
  ('3 - Archimedes'),
  ('3 - Aristotle');

-- Subjects
DELETE FROM `subject`;

INSERT INTO `subject` ( `subject_name` ) VALUES
  ('English 1'),
  ('Math 1'),
  ('Science 1'),
  ('Filipino 1'),
  ('GMRC 1'),
  ('English 2'),
  ('Math 2'),
  ('Science 2'),
  ('Filipino 2'),
  ('GMRC 2'),
  ('English 3'),
  ('Math 3'),
  ('Science 3'),
  ('Filipino 3'),
  ('GMRC 3');

-- Subjects in each classes
DELETE FROM `subject_class_relationship`;

INSERT INTO `subject_class_relationship` ( `class_id`, `subject_id` ) VALUES
  (1,1), (1,2), (1,3), (1,4), (1,5),
  (2,1), (2,2), (2,3), (2,4), (2,5),
  (3,1), (3,2), (3,3), (3,4), (3,5),
  (4,1), (4,2), (4,3), (4,4), (4,5),
  (5,1), (5,2), (5,3), (5,4), (5,5),
  (6,6), (6,7), (6,8), (6,9), (6,10),
  (7,6), (7,7), (7,8), (7,9), (7,10),
  (8,6), (8,7), (8,8), (8,9), (8,10),
  (9,6), (9,7), (9,8), (9,9), (9,10),
  (10,6), (10,7), (10,8), (10,9), (10,10),
  (11,11), (11,12), (11,13), (11,14), (11,15),
  (12,11), (12,12), (12,13), (12,14), (12,15),
  (13,11), (13,12), (13,13), (13,14), (13,15),
  (14,11), (14,12), (14,13), (14,14), (14,15),
  (15,11), (15,12), (15,13), (15,14), (15,15);

-- Teachers
DELETE FROM `teacher`;

INSERT INTO `teacher` ( `last_name`, `first_name`, `username`, `password`, `user_type`, `default_pw` ) VALUES
  ( 'Quill', 'Rubinowicz', 'quill', 'password', 2, 1 ),
  ( 'Pressey', 'Mord', 'mord', 'password', 2, 1 ),
  ( 'Dolley', 'Raleigh', 'raleigh', 'password', 2, 1 ),
  ( 'Compford', 'Marcile', 'marcile', 'password', 2, 1 ),
  ( 'Gaine', 'Myrle', 'myrle', 'password', 2, 1 );

-- Students
-- DELETE FROM `student`;

-- INSERT INTO `student` ( `first_name`, `last_name`, `middle_name`, `username`, `password`, `email`, `contact_number`, `class_id`, `user_type`, `default_pw` ) VALUES
--   ('Florri', 'Purcer', 'Easthope', 'florri', 'password'),


REPLACE INTO `post` (`post_id`, `post_title`, `post_short_description`, `post_content`, `post_type`, `post_date_start`, `post_date_end`) VALUES
(1, 'Ons of event recurrenc', 'Elementum curabitur vitae nunc sed velit dignissim sodales ut eu.', 'Elementum curabitur vitae nunc sed velit dignissim sodales ut eu. Nulla facilisi cras fermentum odio eu feugiat pretium nibh ipsum. In ante metus dictum at tempor commodo. Eget sit amet tellus cras adipiscing enim eu turpis egestas. Viverra nam libero justo laoreet sit. Libero justo laoreet sit amet cursus sit.', 'an', '2018-09-05', '0000-00-00'),
(2, 'Risus in hendrerit gravida rutrum quisque non', 'Netus et malesuada fames ac turpis. Consectetur purus ut', 'Netus et malesuada fames ac turpis. Consectetur purus ut faucibus pulvinar elementum. Quam nulla porttitor massa id neque. Diam maecenas ultricies mi eget mauris pharetra. Neque egestas congue quisque egestas diam in arcu. Tortor at risus viverra adipiscing at in tellus integer. In arcu cursus euismod quis viverra. Amet consectetur adipiscing elit pellentesque habitant morbi tristique senectus et. Commodo quis imperdiet massa tincidunt nunc. Gravida arcu ac tortor dignissim convallis. Blandit libero volutpat sed cras ornare arcu dui. Ultrices dui sapien eget mi proin sed libero enim. Eros in cursus turpis massa tincidunt dui ut ornare. Scelerisque purus semper eget duis at tellus at urna condimentum.', 'an', '2018-09-06', '0000-00-00'),
(3, 'Dolor morbi non arcu', 'Turpis in eu mi bibendum neque egestas congue quisque egestas. Risus viverra adipiscing', 'Turpis in eu mi bibendum neque egestas congue quisque egestas. Risus viverra adipiscing at in tellus integer feugiat. Enim eu turpis egestas pretium aenean pharetra magna. Mattis pellentesque id nibh tortor id aliquet lectus. Suspendisse interdum consectetur libero id faucibus. Nullam non nisi est sit amet. Cursus turpis massa tincidunt dui ut ornare lectus sit amet. Senectus et netus et malesuada fames ac. Magna ac placerat vestibulum lectus mauris ultrices eros in cursus. Pulvinar neque laoreet suspendisse interdum consectetur libero id faucibus nisl. Egestas egestas fringilla phasellus faucibus scelerisque eleifend donec.', 'an', '2018-09-05', '0000-00-00'),
(4, 'Magna ac placerat vestibulum', 'Ac tortor vitae purus faucibus ornare suspendisse sed nisi', 'Ac tortor vitae purus faucibus ornare suspendisse sed nisi. Venenatis tellus in metus vulputate. Amet porttitor eget dolor morbi non arcu risus quis. Et sollicitudin ac orci phasellus egestas tellus rutrum tellus. Lacinia quis vel eros donec ac odio tempor. Mauris augue neque gravida in fermentum. Proin sed libero enim sed faucibus. Nunc eget lorem dolor sed viverra. In hac habitasse platea dictumst quisque sagittis.', 'an', '2018-09-19', '0000-00-00'),
(5, 'Sed cras ornare arcu dui vivamus arcu', 'Orci a scelerisque purus semper eget duis. Turpis tincidunt id aliquet risus feugiat in ante metus.', 'Orci a scelerisque purus semper eget duis. Turpis tincidunt id aliquet risus feugiat in ante metus. Massa eget egestas purus viverra accumsan in. Ultricies tristique nulla aliquet enim tortor at auctor urna nunc. Felis donec et odio pellentesque diam volutpat commodo. Non nisi est sit amet facilisis magna. Nec nam aliquam sem et tortor consequat id. Consequat id porta nibh venenatis cras sed felis. Quis ipsum suspendisse ultrices gravida dictum. Blandit aliquam etiam erat velit scelerisque in dictum non. Aliquam purus sit amet luctus venenatis lectus magna fringilla urna. Ornare massa eget egestas purus viverra.', 'an', '2018-09-21', '0000-00-00'),
(6, 'In est ante in nibh mauris cursus mattis', 'Ipsum nunc aliquet bibendum enim facilisis gravida neque. Habitant morbi tristique senectus et netus et malesuada.', 'Ipsum nunc aliquet bibendum enim facilisis gravida neque. Habitant morbi tristique senectus et netus et malesuada. Euismod elementum nisi quis eleifend quam adipiscing vitae. Auctor augue mauris augue neque gravida in fermentum et sollicitudin. Consectetur adipiscing elit pellentesque habitant. Vitae purus faucibus ornare suspendisse. Viverra aliquet eget sit amet. A pellentesque sit amet porttitor eget dolor morbi. Egestas purus viverra accumsan in nisl nisi scelerisque eu ultrices. Eleifend mi in nulla posuere sollicitudin aliquam ultrices. Netus et malesuada fames ac turpis egestas maecenas. Semper auctor neque vitae tempus quam pellentesque nec nam aliquam. Enim tortor at auctor urna nunc. Nibh sit amet commodo nulla facilisi. Rutrum tellus pellentesque eu tincidunt tortor aliquam nulla. Facilisis mauris sit amet massa vitae tortor condimentum. Nunc congue nisi vitae suscipit tellus mauris. Quis lectus nulla at volutpat diam ut.', 'an', '2018-09-30', '0000-00-00'),
(7, 'Blandit libero volutpat sed cras', 'Cras semper auctor neque vitae tempus. Orci porta non pulvinar neque', 'Cras semper auctor neque vitae tempus. Orci porta non pulvinar neque. Morbi quis commodo odio aenean sed adipiscing. Tempor orci eu lobortis elementum nibh. Lectus urna duis convallis convallis tellus id interdum. Morbi tristique senectus et netus et malesuada fames. Pellentesque id nibh tortor id aliquet lectus. Ac auctor augue mauris augue neque gravida in. ', 'ev', '2018-09-05', '2018-09-06'),
(8, 'Urna id volutpat lacus laoreet non curabitur', 'Sed cras ornare arcu dui', 'Sed cras ornare arcu dui. Commodo elit at imperdiet dui accumsan. Sed nisi lacus sed viverra. Sed felis eget velit aliquet sagittis id. Sollicitudin tempor id eu nisl nunc mi ipsum. Maecenas accumsan lacus vel facilisis volutpat est velit egestas dui. Semper eget duis at tellus at. Purus in massa tempor nec. Elementum eu facilisis sed odio morbi. Elementum nibh tellus molestie nunc non', 'ev', '2018-09-18', '2018-09-20'),
(9, 'Ultrices neque ornare aenean euismod', 'Velit scelerisque in dictum non. Nullam vehicula ipsum a arcu.', 'Velit scelerisque in dictum non. Nullam vehicula ipsum a arcu. Vivamus arcu felis bibendum ut tristique et egestas quis ipsum. Viverra orci sagittis eu volutpat. Mi sit amet mauris commodo quis imperdiet. Nibh mauris cursus mattis molestie a iaculis at. Vel eros donec ac odio tempor. ', 'ev', '2018-09-28', '2018-09-29'),
(10, 'At tellus at urna condimentum mattis pellentesque', 'Tortor vitae purus faucibus ornare suspendisse sed nisi lacus.', 'Tortor vitae purus faucibus ornare suspendisse sed nisi lacus. Varius morbi enim nunc faucibus a pellentesque. Volutpat blandit aliquam etiam erat velit scelerisque. Venenatis tellus in metus vulputate eu scelerisque. Donec ac odio tempor orci dapibus ultrices in iaculis nunc. Tortor condimentum lacinia quis vel. Metus aliquam eleifend mi in nulla posuere sollicitudin aliquam ultrices.', 'ev', '2018-09-09', '2018-09-10'),
(11, 'Praesent semper feugiat nibh sed pulvinar proin gravida.', 'Suscipit adipiscing bibendum est ultricies integer quis auctor elit. Purus gravida quis blandit turpis cursus in.', 'Suscipit adipiscing bibendum est ultricies integer quis auctor elit. Purus gravida quis blandit turpis cursus in. Aliquam vestibulum morbi blandit cursus risus. Id ornare arcu odio ut sem nulla. Platea dictumst quisque sagittis purus sit. Facilisis mauris sit amet massa vitae tortor condimentum lacinia quis. Urna porttitor rhoncus dolor purus non enim praesent. Lorem mollis aliquam ut porttitor leo a. Mi quis hendrerit dolor magna eget est. Tellus cras adipiscing enim eu turpis egestas. Ultrices mi tempus imperdiet nulla malesuada pellentesque elit. Sed ullamcorper morbi tincidunt ornare.', 'ev', '2018-09-01', '2018-09-01'),
(12, 'Ante metus dictum at tempor', 'Nunc pulvinar sapien et ligula ullamcorper malesuada proin libero nunc.', 'Nunc pulvinar sapien et ligula ullamcorper malesuada proin libero nunc. Ac turpis egestas sed tempus urna et pharetra pharetra massa. Sed enim ut sem viverra aliquet. Integer enim neque volutpat ac tincidunt vitae semper. Viverra aliquet eget sit amet tellus cras adipiscing. Id nibh tortor id aliquet lectus. Aliquam ultrices sagittis orci a. Non arcu risus quis varius quam quisque id diam. Nec feugiat nisl pretium fusce id velit ut tortor pretium. Lacus vel facilisis volutpat est. Viverra accumsan in nisl nisi scelerisque eu ultrices vitae. Hac habitasse platea dictumst vestibulum. Nec feugiat nisl pretium fusce id velit ut. Risus ultricies tristique nulla aliquet. Enim diam vulputate ut pharetra sit amet aliquam. Enim diam vulputate ut pharetra sit amet. Porttitor massa id neque aliquam vestibulum morbi. Auctor urna nunc id cursus metus aliquam eleifend mi in.', 'ev', '2018-08-31', '2018-09-01'),
(13, 'At ultrices mi tempus imperdiet nulla malesuada', 'Pulvinar proin gravida hendrerit lectus. Sit amet dictum sit amet justo donec enim. ', 'Pulvinar proin gravida hendrerit lectus. Sit amet dictum sit amet justo donec enim.  Nunc mattis enim ut tellus elementum sagittis. Eu turpis egestas pretium aenean. Rutrum tellus pellentesque eu tincidunt. Nunc sed velit dignissim sodales ut. Pellentesque habitant morbi tristique senectus et netus et. Id porta nibh venenatis cras sed felis eget velit. Diam maecenas ultricies mi eget. Orci dapibus ultrices in iaculis. Ante in nibh mauris cursus mattis molestie. Quis auctor elit sed vulputate.', 'ev', '2018-09-11', '2018-09-12');