<?php include("../partials/header.php");?>

<script>
    function goLastMonth(month, year){
        if(month == 1) {
            --year;
            month = 13;
        }
        --month
        var monthstring= ""+month+"";
        var monthlength = monthstring.length;
            
        if(monthlength <=1){
            monthstring = "0" + monthstring;
        }
        document.location.href ="<?php $_SERVER['PHP_SELF'];?>?month="+monthstring+"&year="+year;
    }

    function goNextMonth(month, year){
        if(month == 12) {
            ++year;
            month = 0;
        }
        ++month
        var monthstring= ""+month+"";
        var monthlength = monthstring.length;

        if(monthlength <=1){
            monthstring = "0" + monthstring;
        }
        document.location.href ="<?php $_SERVER['PHP_SELF'];?>?month="+monthstring+"&year="+year;
    }
</script>

<?php
    if (isset($_GET['day'])){
        $day = $_GET['day'];
    } else {
        $day = date("j");
    }

    if(isset($_GET['month'])){
        $month = $_GET['month'];
    } else {
        $month = date("n");
    }
    
    if(isset($_GET['year'])){
        $year = $_GET['year'];
    } else{
        $year = date("Y");
    }
    $currentTimeStamp = strtotime( "$day-$month-$year");
    $monthName = date("F", $currentTimeStamp);
    $numDays = date("t", $currentTimeStamp);
    $counter = 0;
?>

<?php
    if(isset($_GET['add'])){
        $title =$_POST['txttitle'];
        $detail =$_POST['txtdetail'];
        $eventdate = $month."/".$day."/".$year;
        $sqlinsert = "INSERT into eventcalendar(Title,Detail,eventDate,dateAdded) values ('".$title."','".$detail."','".$eventdate."',now())";
        $resultinginsert = mysqli_query($dbCon, $sqlinsert);

        if($resultinginsert ){
            echo "Event was successfully Added...";
        } else{
            echo "Event Failed to be Added....";
        }
    }
?>

    <section id="calendar" class="bg-grey py-5">
      <div class="container">
        <ul class="list-inline mb-0">
            <li class="list-inline-item"><a href="javascript:void(0)" class="btn btn-link" name='previousbutton' onclick ="goLastMonth(<?php echo $month.",".$year?>)"><span class="fa fa-arrow-left"></span> PREV</a></li>
            <li class="list-inline-item"><span class="title"><?php echo $monthName.", ".$year; ?></span></li>
            <li class="list-inline-item"><a href="javascript:void(0)" class="btn btn-link" name='nextbutton' onclick ="goNextMonth(<?php echo $month.",".$year?>)">NEXT <span class="fa fa-arrow-right"></span></a></li>
        </ul>
        <p class="text-right"><a href="<?php echo $root_dir; ?>/calendar/index.php">Today</a></p>
        <div class="table-responsive">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>SUN</th>
                        <th>MON</th>
                        <th>TUE</th>
                        <th>WED</th>
                        <th>THU</th>
                        <th>FRI</th>
                        <th>SAT</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        echo "<tr>";

                        for($i = 1; $i < $numDays+1; $i++, $counter++){
                            $timeStamp = strtotime("$year-$month-$i");
                            
                            if($i == 1) {
                                $firstDay = date("w", $timeStamp);

                                for($j = 0; $j < $firstDay; $j++, $counter++) {
                                    echo "<td>&nbsp;</td>";
                                }
                            }

                            if($counter % 7 == 0) {
                                echo "</tr><tr>";
                            }
                            $monthstring = $month;
                            $monthlength = strlen($monthstring);
                            $daystring = $i;
                            $daylength = strlen($daystring);
                            
                            if($monthlength <= 1){
                                $monthstring = "0".$monthstring;
                            }

                            if($daylength <=1){
                                $daystring = "0".$daystring;
                            }
                            $todaysDate = date("Y-m-d");
                            // $dateToCompare = $monthstring. '/' . $daystring. '/' . $year;
                            $dateToCompare = $year . '-' . $monthstring. '-' . $daystring;
                            $sqlCount = "SELECT * FROM post where ('".$dateToCompare."' BETWEEN post_date_start AND post_date_end) OR post_date_start='".$dateToCompare."'";
                            $noOfEvent = mysqli_num_rows(mysqli_query($dbCon, $sqlCount));
                            echo "<td class='text-center" .
                                (($todaysDate == $dateToCompare) ? " today" : "") .
                                (($noOfEvent >= 1) ? " has-posts'><a href='".$_SERVER['PHP_SELF']."?month=".$monthstring."&day=".$daystring."&year=".$year."&v=true'>".$i."</a>" : "'>".$i);
                            echo "</td>";
                        }
                        while ($counter % 7 !== 0) {
                            echo '<td></td>';
                            $counter++;
                        }
                        echo "</tr>";
                    ?>
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="7">
                            <h3 class="text-center"><?php echo date_format(date_create($year."-".$month."-".$day),"F j, Y") ?></h3>
                            <ul class="list-inline">
                            <?php
                                //echo "<hr>";
                                //echo "<a href='".$_SERVER['PHP_SELF']."?month=".$month."&day=".$day."&year=".$year."&v=true&f=true'>Add Event</a>";
                                if(isset($_GET['f'])) {
                                    include("eventform.php");
                                }
                                $sqlEvent = "SELECT * FROM post where ('".$year."-".$month."-".$day."' BETWEEN post_date_start AND post_date_end) OR post_date_start='".$year."-".$month."-".$day."'";
                                $resultEvents = mysqli_query($dbCon, $sqlEvent);
                                echo "<hr>";
                                $eventscount = 0;
                                while ($events = mysqli_fetch_array($resultEvents)){
                                    echo "<li class='list-inline-item row my-3'>";
                                    echo "<div class='icon col-2 d-flex'><span class='fa-fw select-all far'>" .
                                        (($events['post_type'] === "ev") ? "" : "") .
                                        "</span></div>";
                                    echo "<div class='content col-10'>";
                                    echo "<h4 class='heading mb-0'>".$events['post_title']."</h4>";
                                    echo "<p class='date mb-0'>"."<small>".
                                        date_format(date_create($events['post_date_start']),"F j, Y") .
                                        (($events['post_type'] === "ev") ? " to ". date_format(date_create($events['post_date_end']),"F j, Y") : "").
                                        "</small>"."</p>";
                                    echo "<p class='subheading mb-0'>".$events['post_content']."</p>";
                                    echo "</div>"."</li>";
                                    $eventscount++;
                                }
                                if ($eventscount == 0) {
                                    echo '<div class="text-center p-20">No events and announcements.</div>';
                                }
                            ?>
                            </ul>
                        </td>
                    </tr>
                </tfoot>
            </table>
        </div>
      </div>
    </section>

<?php include("../partials/footer.php"); ?>