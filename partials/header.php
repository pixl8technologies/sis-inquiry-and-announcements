<?php
error_reporting(E_ALL &~ E_NOTICE);
session_start();





$directoryURI = $_SERVER['REQUEST_URI'];
if ( strpos( $directoryURI, '/calendar/' ) !== false ) $activeMenu = 'calendar';
else if ( strpos( $directoryURI, '/contact-us/' ) !== false ) $activeMenu = 'contact-us';
else if ( strpos( $directoryURI, '/about/' ) !== false ) $activeMenu = 'about';
else if ( strpos( $directoryURI, '/calendar/' ) !== false ) $activeMenu = 'calendar';
else if ( strpos( $directoryURI, '/log-in/' ) !== false ) $activeMenu = 'log-in';
else if ( strpos( $directoryURI, '/students/' ) !== false ) $activeMenu = 'students';
else if ( strpos( $directoryURI, '/teachers/' ) !== false ) $activeMenu = 'teachers';
else $activeMenu = 'home';

include(($activeMenu == 'home') ? 'config.php' : '../config.php');
include(($activeMenu == 'home') ? 'db_connection.php' : '../db_connection.php');

$sql = "SELECT * FROM general_settings WHERE gs_id=1";
$result = mysqli_query($dbCon, $sql);
if (mysqli_num_rows($result) > 0) { $row = mysqli_fetch_assoc($result); }
else $row = array("gs_name"=>"", "gs_address"=>"", "gs_contact_number"=>"","gs_founded_content"=>"");
$add = $row['gs_address'];
$contact= $row['gs_contact_number'];
$name= $row['gs_name'];
$content= $row['gs_founded_content'];
$logo= $row['gs_logo_url'];
$logo_alt= $row['gs_logo_alt_name'];
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Paco Catholic School</title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo $root_dir; ?>/css/bootstrap.min.css" rel="stylesheet">

    <!-- Fontawesome CSS -->
    <link href="<?php echo $root_dir; ?>/css/fontawesome.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="<?php echo $root_dir; ?>/css/main.css" rel="stylesheet">

    <!-- favicon  -->
    <link rel="shortcut icon" href="<?php echo $root_dir; ?>/admin/remark/base/assets/images/favicon2.ico">
  </head>

  <body id="page-top">

    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg text-primary fixed-top bg-white " id="mainNav">
      <div class="container">
		<img src="<?php echo $root_dir; ?>/assets/uploads/<?php echo $logo; ?>" class="img-fluid" alt="<?php echo $logo_alt; ?>" >
		<a class="navbar-brand text-center d-none d-sm-block" href="<?php echo $root_dir; ?>/">
			<p><b><?php echo $name; ?></b><br><small><?php echo $content; ?></small></p>
		</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon d-none"> </span>
			<span class="fa fa-bars"> </span>
		</button>
		<div class="collapse navbar-collapse mt-3 mt-lg-0" id="navbarResponsive">
			<ul class="navbar-nav ml-auto active ">
				<li class="nav-item<?php echo ($activeMenu == 'home') ? ' active' : '' ; ?>">
					<a class="nav-link" href="<?php echo $root_dir; ?>/">Home</a>
				</li>
				<li class="nav-item<?php echo ($activeMenu == 'calendar') ? ' active' : '' ; ?>">
					<a class="nav-link" href="<?php echo $root_dir; ?>/calendar/">Calendar</a>
				</li>
				<li class="nav-item<?php echo ($activeMenu == 'contact-us') ? ' active' : '' ; ?>">
					<a class="nav-link" href="<?php echo $root_dir; ?>/contact-us/">Contact Us</a>
				</li>
				<li class="nav-item<?php echo ($activeMenu == 'about') ? ' active' : '' ; ?>">
					<a class="nav-link" href="<?php echo $root_dir; ?>/about/">About</a>
				</li>
				<?php if(isset($_SESSION['user_type']) && $_SESSION['user_type'] === '1' ) { ?>
				<li class="nav-item">
					<a class="nav-link" href="<?php echo $root_dir; ?>/admin/">Admin</a>
				</li>
				<?php } ?>
				<?php if(isset($_SESSION['user_type']) && $_SESSION['user_type'] === '2' ) { ?>
				<li class="nav-item<?php echo ($activeMenu == 'teachers') ? ' active' : '' ; ?>">
					<a class="nav-link" href="<?php echo $root_dir; ?>/teachers/">Portal</a>
				</li>
				<?php } ?>
				<?php if(isset($_SESSION['user_type']) && $_SESSION['user_type'] === '3' ) { ?>
				<li class="nav-item<?php echo ($activeMenu == 'students') ? ' active' : '' ; ?>">
					<a class="nav-link" href="<?php echo $root_dir; ?>/students/">Portal</a>
				</li>
				<?php } ?>
				<li class="nav-item<?php echo ($activeMenu == 'log-in') ? ' active' : '' ; ?>">
					<?php if(isset($_SESSION['username'])){ ?>
					<a class="nav-link" href="<?php echo $root_dir; ?>/includes/logout.php">Logout</a>
					<?php }else{ ?>
					<a class="nav-link" href="<?php echo $root_dir; ?>/log-in/">Login</a>
				<?php } ?>
				</li>
			</ul>
		</div>
	</div>
</nav>

<div class="page-content">