
	</div>
  <?php
    $sql = "SELECT * FROM general_settings WHERE gs_id=1";
     $result = mysqli_query($dbCon, $sql);
    if (mysqli_num_rows($result) > 0) { $row = mysqli_fetch_assoc($result); }
     else $row = array("gs_name"=>"", "gs_address"=>"", "gs_contact_number"=>"", "gs_facebook_link"=>"", "gs_twitter_link"=>"","gs_instagram_link"=>"");
      $add = $row['gs_address'];
      $contact= $row['gs_contact_number'];
      $name= $row['gs_name'];
      $fb= $row['gs_facebook_link'];
      $twitter= $row['gs_twitter_link'];
      $insta= $row['gs_instagram_link'];
    ?>
    <!-- Footer -->
    <footer class="py-3">
      <div class="container " id="div1">
	<div class=row>
		 <div class=col-md-6>
       		 <p><?php echo $name ?> <br>
       		 <?php echo $add ?><br>
       		 <?php echo $contact ?></p>
                </div>
		<div class=col-md-6>
		  <p class="m-0 text-left flex-wrap ml-auto pt-3" >
		    <span class="fac fa-lg fa-pull-right fac ">
			<a class="fab fa-instagram fa-fw  fac " href="<?php echo $insta ?>" target="_blank"></a>
	 	    </span>
		    <span class="fac fa-lg fa-pull-right  fac ">
			<a class="fab fa-facebook-f fa-fw " href="<?php echo $fb ?>" target="_blank"></a>
		    </span>
		    <span class="fac fa-lg fa-pull-right fac ">
			<a class="fab fa-twitter fa-fw " href="<?php echo $twitter ?>" target="_blank"></a>
		    </span>
		  </p>
		</div>        
      	</div>
</div>
      <!-- /.container -->

    </footer>

    <!-- Bootstrap core JavaScript -->
    <script src="<?php echo $root_dir; ?>/js/jquery.min.js"></script>
    <script src="<?php echo $root_dir; ?>/js/bootstrap.bundle.min.js"></script>
    <script src="<?php echo $root_dir; ?>/js/bootstrap.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="<?php echo $root_dir; ?>/js/jquery.easing.min.js"></script>

    <!-- Custom JavaScript for this theme -->
    <script src="<?php echo $root_dir; ?>/js/custom.js"></script>

  </body>

</html>
