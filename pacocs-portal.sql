-- Classes
DELETE FROM `class`;

INSERT INTO `class` ( `class_name` ) VALUES
  ('1 - Einstein'),
  ('1 - Newton'),
  ('1 - Curie'),
  ('1 - Galilei'),
  ('1 - Hawking'),
  ('2 - Darwin'),
  ('2 - Tesla'),
  ('2 - Edison'),
  ('2 - Copernicus'),
  ('2 - Faraday'),
  ('3 - Da Vinci'),
  ('3 - Boyle'),
  ('3 - Graham Bell'),
  ('3 - Archimedes'),
  ('3 - Aristotle');

-- Subjects
DELETE FROM `subject`;

INSERT INTO `subject` ( `subject_name` ) VALUES
  ('English 1'),
  ('Math 1'),
  ('Science 1'),
  ('Filipino 1'),
  ('GMRC 1'),
  ('English 2'),
  ('Math 2'),
  ('Science 2'),
  ('Filipino 2'),
  ('GMRC 2'),
  ('English 3'),
  ('Math 3'),
  ('Science 3'),
  ('Filipino 3'),
  ('GMRC 3');

-- Subjects in each classes
DELETE FROM `subject_class_relationship`;

INSERT INTO `subject_class_relationship` ( `class_id`, `subject_id` ) VALUES
  (1,1), (1,2), (1,3), (1,4), (1,5),
  (2,1), (2,2), (2,3), (2,4), (2,5),
  (3,1), (3,2), (3,3), (3,4), (3,5),
  (4,1), (4,2), (4,3), (4,4), (4,5),
  (5,1), (5,2), (5,3), (5,4), (5,5),
  (6,6), (6,7), (6,8), (6,9), (6,10),
  (7,6), (7,7), (7,8), (7,9), (7,10),
  (8,6), (8,7), (8,8), (8,9), (8,10),
  (9,6), (9,7), (9,8), (9,9), (9,10),
  (10,6), (10,7), (10,8), (10,9), (10,10),
  (11,11), (11,12), (11,13), (11,14), (11,15),
  (12,11), (12,12), (12,13), (12,14), (12,15),
  (13,11), (13,12), (13,13), (13,14), (13,15),
  (14,11), (14,12), (14,13), (14,14), (14,15),
  (15,11), (15,12), (15,13), (15,14), (15,15);

-- Teachers
DELETE FROM `teacher`;

INSERT INTO `teacher` ( `last_name`, `first_name`, `username`, `password`, `user_type`, `default_pw` ) VALUES
  ( 'Quill', 'Rubinowicz', 'quill', 'password', 2, 1 ),
  ( 'Pressey', 'Mord', 'mord', 'password', 2, 1 ),
  ( 'Dolley', 'Raleigh', 'raleigh', 'password', 2, 1 ),
  ( 'Compford', 'Marcile', 'marcile', 'password', 2, 1 ),
  ( 'Gaine', 'Myrle', 'myrle', 'password', 2, 1 );

-- Teachers
DELETE FROM `student`;

INSERT INTO `student` ( `last_name`, `first_name`, `class_id`, `username`, `password`, `user_type`, `default_pw` ) VALUES
  ( 'Bobihis', 'Reynell', 6, 'reynell', 'password', 2, 1 ),
  ( 'Bobihis', 'Shane', 1, 'shane', 'password', 2, 1 ),
  ( 'Bobihis', 'Jaina', 11, 'jaina', 'password', 2, 1 );