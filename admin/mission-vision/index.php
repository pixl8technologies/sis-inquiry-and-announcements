<?php include("../header.php"); ?>

    <!-- Page -->
    <div class="page view-all-page">
      <div class="page-header">
        <h1 class="page-title">Featured Article</h1>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="<?php echo $root_dir; ?>/">Home</a></li>
          <li class="breadcrumb-item"><a href="<?php echo $root_dir; ?>/admin">Admin</a></li>
          <li class="breadcrumb-item">About Page Settings</li>
          <li class="breadcrumb-item active">Mission &amp; Vision</li>
        </ol>
      </div>
      <div class="page-content container-fluid">
        <div class="row" data-plugin="matchHeight" data-by-row="true">
          <div class="col-md-12">
            <div class="panel">
              <div class="panel-body">

                <!-- Insert/Update the database and display the result -->
                <div class="row">
                  <?php if( isset($_POST['admin-wysiwyg-textarea']) ) {
                    $sql = "INSERT INTO general_settings (gs_id, gs_about_mv_content) VALUES (1, '";
                    $sql .= mysqli_real_escape_string( $dbCon , $_POST['admin-wysiwyg-textarea'] );
                    $sql .= "') ON DUPLICATE KEY UPDATE gs_about_mv_content='";
                    $sql .= mysqli_real_escape_string( $dbCon , $_POST['admin-wysiwyg-textarea'] );
                    $sql .= "'";
                    if ( !(mysqli_query($dbCon, $sql)) ) { ?>
                      <div class="col-lg-12">
                        <div class="alert alert-warning alert-dismissible" role="alert">
                          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                          ERROR : <?php echo mysqli_error($dbCon); ?>
                        </div>
                      </div>
                      <?php } else { ?>
                      <div class="col-lg-12">
                        <div class="alert alert-success alert-dismissible" role="alert">
                          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                          SUCCESS : The Mission &amp; Vision content has been updated.
                        </div>
                      </div>
                    <?php } } ?>
                </div>
                
                <!-- The wysiwyg form -->
                <div class="row">
                  <div class="admin-wysiwyg col-lg-12" data-php="<?php echo $root_dir; ?>/featured/featured.php">
                    <div data-plugin="summernote" data-plugin-options='{"toolbar":[["style", ["bold", "italic", "underline", "clear"]],["color", ["color"]],["para", ["ul", "ol", "paragraph"]]]}'><?php
                      $sql = "SELECT gs_about_mv_content FROM general_settings WHERE gs_id=1";
                      $result = mysqli_query($dbCon, $sql);
                      if (mysqli_num_rows($result) > 0) { $row = mysqli_fetch_assoc($result); }
                      else $row = array("gs_about_mv_content"=>"");
                      echo $row['gs_about_mv_content'];
                      ?></div>
                    <div class="form-group text-right">
                      <button class="admin-wysiwyg-reset btn btn-default mr-10">Reset</button>
                      <button class="admin-wysiwyg-submit btn btn-primary">Save</button>
                    </div>
                    <div class="admin-wysiwyg-original d-none"><?php echo $row['gs_about_mv_content']; ?></div>
                    <form class="admin-wysiwyg-form d-none" action="" method="post">
                      <textarea name="admin-wysiwyg-textarea" class="admin-wysiwyg-textarea"></textarea>
                    </form>
                  </div>
                </div>

              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- End Page -->

<?php include("../footer.php"); ?>