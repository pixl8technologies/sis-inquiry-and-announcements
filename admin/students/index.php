<?php include("../header.php"); ?>

    <!-- Page -->
    <div class="page view-all-page">
      <div class="page-header">
        <h1 class="page-title">Student Accounts</h1>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="<?php echo $root_dir; ?>/">Home</a></li>
          <li class="breadcrumb-item"><a href="<?php echo $root_dir; ?>/admin">Admin</a></li>
          <li class="breadcrumb-item">Portal</li>
          <li class="breadcrumb-item active">Students</li>
        </ol>
       <div class="page-header-actions d-none">
        <form class="form-inline d-inline-block mr-10">
        <div class="form-group footable-filtering-search">
          <div class="input-group">
            <input type="text" class="form-control" placeholder="Search" style="padding: 18px;">
            <div class="input-group-btn">
              <button type="button" class="btn btn-primary"><i class="wb-search" aria-hidden="true"></i></button>
            </div>
          </div>
        </div>
        </form>
        </div>
      </div>
      <div class="page-content container-fluid">
        <div class="row" data-plugin="matchHeight" data-by-row="true">

          <div class="col-md-5">
            <!-- Panel Accordion -->
            <div class="panel">
              <div class="panel-body collapse show">

                <!-- Add Student -->
                <?php
                if (isset($_POST['add_student_username'])) {
                  $sql = "INSERT INTO student (first_name, last_name, middle_name, class_id, username, password, email, contact_number, user_type, default_pw) VALUES ('" . 
                    mysqli_real_escape_string( $dbCon , $_POST['add_student_first_name'] ) . "', '" .
                    mysqli_real_escape_string( $dbCon , $_POST['add_student_last_name'] ) . "', '" .
                    mysqli_real_escape_string( $dbCon , $_POST['add_student_middle_name'] ) . "', '" .
                    mysqli_real_escape_string( $dbCon , $_POST['add_student_class'] ) . "', '" .
                    mysqli_real_escape_string( $dbCon , $_POST['add_student_username'] ) . "', '" .
                    mysqli_real_escape_string( $dbCon , $_POST['add_student_password'] ) . "', '" .
                    mysqli_real_escape_string( $dbCon , $_POST['add_student_email'] ) . "', '" .
                    mysqli_real_escape_string( $dbCon , $_POST['add_student_contact_number'] ) . "', 3, 1)";
                  if ( !(mysqli_query($dbCon, $sql)) ) { ?>
                  <div class="alert alert-warning alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    ERROR : <?php echo mysqli_error($dbCon); ?>
                  </div>
                  <?php } else { ?>
                  <div class="alert alert-success alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    SUCCESS : student <strong><?php echo $_POST['add_student_first_name']; ?></strong> has been added.
                  </div>
                <?php } } ?>

                <h4 class="mb-25">Add a student Account</h4>

                <form action="" method="post">
                  <div class="row text-left">
                    <div class="col-md-12">
                      <label class="form-control-label">Full Name</label>
                    </div>
                    <div class="col-md-4 form-group">
                      <input type="text" class="form-control" name="add_student_first_name" placeholder="First Name">
                    </div>
                    <div class="col-md-4 form-group">
                      <input type="text" class="form-control" name="add_student_middle_name" placeholder="Middle Name">
                    </div>
                    <div class="col-md-4 form-group">
                      <input type="text" class="form-control" name="add_student_last_name" placeholder="Last Name">
                    </div>
                    <div class="col-md-7 form-group">
                      <input type="email" class="form-control" name="add_student_email" placeholder="Email">
                    </div>
                    <div class="col-md-5 form-group">
                      <input type="text" class="form-control" name="add_student_contact_number" placeholder="Contact No.">
                    </div>
                    <div class="col-md-6 form-group">
                      <select name="add_student_class" class="teacher-subjects-handled-class form-control" data-plugin="select2">
                        <?php $result2 = mysqli_query($dbCon, "SELECT * FROM class"); if (mysqli_num_rows($result2) > 0) { while($row = mysqli_fetch_assoc($result2)) { ?>
                        <option value="<?= $row['class_id'] ?>"><?= $row['class_name'] ?></option>
                        <?php } } ?>
                      </select>
                    </div>
                    <div class="col-md-12">
                      <label class="form-control-label">Login Details</label>
                    </div>
                    <div class="col-md-6 form-group">
                      <input type="text" class="form-control" name="add_student_username" placeholder="Username" required>
                    </div>
                    <div class="col-md-6 form-group">
                      <input type="text" class="form-control" value="password" disabled >
                      <input type="text" class="d-none" name="add_student_password" value="password" >
                    </div>
                    <div class="col-md-12 text-right">
                      <button class="btn btn-default mr-10" data-dismiss="modal" type="button">Cancel</button>
                      <button class="btn btn-primary" type="submit">Save</button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
            <!-- End Panel Accordion -->
          </div>

          <div class="col-md-7">
            <!-- Panel Accordion -->
            <div class="panel">
              <div class="panel-body collapse show">

                <!-- Edit student -->
                <?php
                if (isset($_POST['edit_student'])) {
                  $error = "";
                  $sql = "UPDATE student SET " .
                    "first_name='" . $_POST['edit_student_first_name'] . "', " .
                    "middle_name='" . $_POST['edit_student_middle_name'] . "', " .
                    "last_name='" . $_POST['edit_student_last_name'] . "', " .
                    "email='" . $_POST['edit_student_email'] . "', " .
                    "contact_number='" . $_POST['edit_student_contact_number'] . "', " .
                    "class_id='" . $_POST['edit_student_class'] . "', " .
                    "username='" . $_POST['edit_student_username'] . "', " .
                    
                    "default_pw=0 WHERE student_id=" . $_POST['edit_student'];
                    if(isset($_POST['edit_student_password'])){
                    
                      $password = "password";
                      $sql = "UPDATE student SET password = 'password' WHERE student_id =" .$_POST['edit_student'];
                      if ( !(mysqli_query($dbCon, $sql)) ) $error .= mysqli_error($dbCon);
                    }
                  if ( !(mysqli_query($dbCon, $sql)) ) { ?>
                  <div class="alert alert-warning alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    ERROR : <?php echo mysqli_error($dbCon); ?>
                  </div>
                  <?php } else { ?>
                  <div class="alert alert-success alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    SUCCESS : student <strong><?php echo $_POST['edit_student_first_name']; ?></strong> (ID = <?php echo $_POST['edit_student']; ?>) data has been updated.
                  </div>
                  <?php }
                  ?>
                <?php } ?>

                <!-- Delete student -->
                <?php
                if (isset($_POST['delete_student'])) {
                  $sql = "DELETE FROM student WHERE student_id=" . $_POST['delete_student'];
                  if ( !(mysqli_query($dbCon, $sql)) ) { ?>
                  <div class="alert alert-warning alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    ERROR : <?php echo mysqli_error($dbCon); ?>
                  </div>
                  <?php } else { ?>
                  <div class="alert alert-success alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    SUCCESS : Student <strong><?php echo $_POST['delete_student_name']; ?></strong> (ID = <?php echo $_POST['delete_student']; ?>) has been deleted.
                  </div>
                <?php } } ?>

                <h4 class="mb-25">All Student Accounts</h4>

                <table class="table table-hover table-bordered table-striped toggle-arrow-tiny table-sm text-center" id="student-delete-table">
                  <thead>
                    <tr>
                      <th class="py-10">ID</th>
                      <th class="py-10">Name</th>
                      <th class="py-10">Username</th>
                      <th class="py-10">Password</th>
                      <th class="py-10" style="min-width: 65px;">Actions</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php
                    $result = mysqli_query($dbCon, "SELECT * FROM student ORDER BY last_name");
                    if (mysqli_num_rows($result) > 0) { while($row_students = mysqli_fetch_assoc($result)) { ?>
                    <tr>
                      <td><?php echo $row_students['student_id'];?></td>
                      <td data-toggle="modal" data-target="#student-edit-modal-<?php echo $row_students['student_id'];?>" class="cursor-pointer py-10 px-25 text-left"><?php echo $row_students['last_name'];?>, <?php echo $row_students['first_name'];?> <?php echo $row_students['middle_name'];?></td>
                      <td data-toggle="modal" data-target="#student-edit-modal-<?php echo $row_students['student_id'];?>" class="cursor-pointer py-10 px-25"><?php echo $row_students['username'];?></td>
                      <td data-toggle="modal" data-target="#student-edit-modal-<?php echo $row_students['student_id'];?>" class="cursor-pointer py-10 px-25"><?php echo $row_students['password'] === 'password' ? 'default' : '********';?></td>
                      <td>
                        <a data-toggle="modal" data-target="#student-edit-modal-<?php echo $row_students['student_id'];?>" href="javascript:void(0)"><i class="icon wb-pencil p-5" style="color: #76838f;" aria-hidden="true"></i></a>
                        <!-- Modal -->
                        <div class="modal fade modal-fade-in-scale-up" id="student-edit-modal-<?php echo $row_students['student_id'];?>" aria-hidden="false" role="dialog" tabindex="-1">
                          <div class="modal-dialog modal-simple">
                            <form class="modal-content" action="" method="post">
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                  <span aria-hidden="true">×</span>
                                </button>
                                <h4 class="modal-title">Edit Student</h4>
                              </div>
                              <div class="modal-body">
                                <div class="row text-left">
                                  <input type="text" class="d-none" name="edit_student" value="<?php echo $row_students['student_id'];?>">
                                  <div class="col-md-12">
                                    <label class="form-control-label">Full Name</label>
                                  </div>
                                  <div class="col-md-4 form-group">
                                    <input type="text" class="form-control" name="edit_student_first_name" placeholder="First Name" value="<?php echo $row_students['first_name'];?>">
                                  </div>
                                  <div class="col-md-4 form-group">
                                    <input type="text" class="form-control" name="edit_student_middle_name" placeholder="Middle Name" value="<?php echo $row_students['middle_name'];?>">
                                  </div>
                                  <div class="col-md-4 form-group">
                                    <input type="text" class="form-control" name="edit_student_last_name" placeholder="Last Name" value="<?php echo $row_students['last_name'];?>">
                                  </div>
                                  <div class="col-md-7 form-group">
                                    <input type="email" class="form-control" name="edit_student_email" placeholder="Email" value="<?php echo $row_students['email'];?>">
                                  </div>
                                  <div class="col-md-5 form-group">
                                    <input type="text" class="form-control" name="edit_student_contact_number" placeholder="Contact No." value="<?php echo $row_students['contact_number'];?>">
                                  </div>
                                  <div class="col-md-6 form-group">
                                    <select name="edit_student_class" class="teacher-subjects-handled-class form-control" sdata-plugin="select2">
                                      <?php $result2 = mysqli_query($dbCon, "SELECT * FROM class"); if (mysqli_num_rows($result2) > 0) { while($row = mysqli_fetch_assoc($result2)) { ?>
                                      <option value="<?= $row['class_id'] ?>" <?= $row_students['class_id'] === $row['class_id'] ? 'selected' : '' ?>><?= $row['class_name'] ?></option>
                                      <?php } } ?>
                                    </select>
                                  </div>
                                  <div class="col-md-12">
                                    <label class="form-control-label">Login Details</label>
                                  </div>
                                  <div class="col-md-6 form-group">
                                    <input type="text" class="form-control" name="edit_student_username" placeholder="Username" value="<?php echo $row_students['username'];?>" required>
                                  </div>
                                  <div class="col-md-6 form-group">
                                  <div class="checkbox-custom checkbox-primary">
                                      <input name="edit_student_password" type="checkbox" id="edit_student_password" />
                                      <label for="edit_student_password">Reset Password</label>
                                    </div>
                                  </div>
                                  <div class="col-md-12 text-right">
                                    <button class="btn btn-default mr-10" data-dismiss="modal" type="button">Cancel</button>
                                    <button class="btn btn-primary" type="submit">Save</button>
                                  </div>
                                </div>
                              </div>
                            </form>
                          </div>
                        </div>
                        <!-- End Modal -->
                        <form action="" method="post" class="form-button" >
                          <input type="text" class="d-none" name="delete_student" value="<?php echo $row_students['student_id'];?>">
                          <input type="text" class="d-none" name="delete_student_name" value="<?php echo $row_students['first_name'];?>">
                          <button type="submit"><i class="icon wb-trash p-5" style="color: #76838f;" aria-hidden="true"></i></button>
                        </form>
                      </td>
                    </tr>
                    <?php } } else { ?>
                    <tr><td colspan="999" class="text-center">No records.</td></tr>
                    <?php } ?>
                  </tbody>
                </table>

              </div>
            </div>
            <!-- End Panel Accordion -->
          </div>

        </div>
      </div>
    </div>
    <!-- End Page -->

<?php include("../footer.php"); ?>