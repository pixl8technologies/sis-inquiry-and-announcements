<?php include("../header.php"); ?>

    <!-- Page -->
    <div class="page view-all-page">
      <div class="page-header">
        <h1 class="page-title">Subjects & Classes</h1>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="<?php echo $root_dir; ?>/">Home</a></li>
          <li class="breadcrumb-item"><a href="<?php echo $root_dir; ?>/admin">Admin</a></li>
          <li class="breadcrumb-item">Portal</li>
          <li class="breadcrumb-item active">Subjects & Classes</li>
        </ol>
       <div class="page-header-actions d-none">
        <form class="form-inline d-inline-block mr-10">
        <div class="form-group footable-filtering-search">
          <div class="input-group">
            <input type="text" class="form-control" placeholder="Search" style="padding: 18px;">
            <div class="input-group-btn">
              <button type="button" class="btn btn-primary"><i class="wb-search" aria-hidden="true"></i></button>
            </div>
          </div>
        </div>
        </form>
        </div>
      </div>
      <div class="page-content container-fluid">
        <div class="row" data-plugin="matchHeight" data-by-row="true">

          <div class="col-md-4">
            <!-- Panel Accordion -->
            <div class="panel">
              <div class="panel-body collapse show">

                <!-- Add Subject -->
                <?php
                if (isset($_POST['add_subject'])) {
                  $sql = "INSERT INTO subject (`subject_name`) VALUES ('";
                  $sql .= mysqli_real_escape_string( $dbCon , $_POST['add_subject'] ) . "')";
                  if ( !(mysqli_query($dbCon, $sql)) ) { ?>
                  <div class="alert alert-warning alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    ERROR : <?php echo mysqli_error($dbCon); ?>
                  </div>
                  <?php } else { ?>
                  <div class="alert alert-success alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    SUCCESS : Subject <strong><?php echo $_POST['add_subject']; ?></strong> has been added.
                  </div>
                <?php } } ?>

                <!-- Edit Subject -->
                <?php
                if (isset($_POST['edit_subject'])) {
                  $sql = "UPDATE subject SET subject_name='" . $_POST['edit_subject_name'] . "' WHERE subject_id=" . $_POST['edit_subject'] . "";
                  if ( !(mysqli_query($dbCon, $sql)) ) { ?>
                  <div class="alert alert-warning alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    ERROR : <?php echo mysqli_error($dbCon); ?>
                  </div>
                  <?php } else { ?>
                  <div class="alert alert-success alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    SUCCESS : Subject <strong><?php echo $_POST['edit_subject_name_old']; ?></strong> (ID = <?php echo $_POST['edit_subject']; ?>) has been updated to <strong><?php echo $_POST['edit_subject_name']; ?></strong>.
                  </div>
                <?php } } ?>

                <!-- Delete Subject -->
                <?php
                if (isset($_POST['delete_subject'])) {
                  $sql = "DELETE FROM subject_class_relationship WHERE subject_id=" . $_POST['delete_subject']; 
                  if ( !(mysqli_query($dbCon, $sql)) ) $error = mysqli_error($dbCon);
                  $sql = "DELETE FROM subject WHERE subject_id=" . $_POST['delete_subject'];
                  if ( !(mysqli_query($dbCon, $sql)) ) { ?>
                  <div class="alert alert-warning alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    ERROR : <?php echo (isset($error) ? $error : '') . mysqli_error($dbCon); ?>
                  </div>
                  <?php } else { ?>
                  <div class="alert alert-success alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    SUCCESS : Subject <strong><?php echo $_POST['delete_subject_name']; ?></strong> (ID = <?php echo $_POST['delete_subject']; ?>) has been deleted.
                  </div>
                <?php } } ?>

                <h4>Subjects</h4>

                <table class="table table-hover table-bordered table-striped toggle-arrow-tiny table-sm text-center" id="subject-delete-table">
                  <thead>
                    <tr>
                      <th class="py-10 text-center">ID</th>
                      <th class="py-10 text-center">Name</th>
                      <th class="py-10 text-center" style="min-width: 65px;">Actions</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php
                    $result = mysqli_query($dbCon, "SELECT * FROM subject ORDER BY subject_name");
                    if (mysqli_num_rows($result) > 0) { while($row_subjects = mysqli_fetch_assoc($result)) { ?>
                    <tr>
                      <td><?php echo $row_subjects['subject_id'];?></td>
                      <td class="py-10 px-15 text-left"><?php echo $row_subjects['subject_name'];?></td>
                      <td>
                        <a data-toggle="modal" data-target="#subject-edit-modal-<?php echo $row_subjects['subject_id'];?>" href="#"><i class="icon wb-pencil p-5" style="color: #76838f;" aria-hidden="true"></i></a>
                        <!-- Modal -->
                        <div class="modal fade modal-fade-in-scale-up" id="subject-edit-modal-<?php echo $row_subjects['subject_id'];?>" aria-hidden="false" role="dialog" tabindex="-1">
                          <div class="modal-dialog modal-simple">
                            <form class="modal-content" action="" method="post">
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                  <span aria-hidden="true">×</span>
                                </button>
                                <h4 class="modal-title">Edit Subject</h4>
                              </div>
                              <div class="modal-body">
                                <div class="row">
                                  <div class="col-md-8 form-group">
                                    <input type="text" class="d-none" name="edit_subject" value="<?php echo $row_subjects['subject_id'];?>">
                                    <input type="text" class="d-none" name="edit_subject_name_old" value="<?php echo $row_subjects['subject_name'];?>">
                                    <input type="text" class="form-control" name="edit_subject_name" placeholder="<?php echo $row_subjects['subject_name'];?>" value="<?php echo $row_subjects['subject_name'];?>">
                                  </div>
                                  <div class="col-md-4 text-right">
                                    <button class="btn btn-default mr-10" data-dismiss="modal" type="button">Cancel</button>
                                    <button class="btn btn-primary" type="submit">Save</button>
                                  </div>
                                </div>
                              </div>
                            </form>
                          </div>
                        </div>
                        <!-- End Modal -->
                        <form action="" method="post" class="form-button" >
                          <input type="text" class="d-none" name="delete_subject" value="<?php echo $row_subjects['subject_id'];?>">
                          <input type="text" class="d-none" name="delete_subject_name" value="<?php echo $row_subjects['subject_name'];?>">
                          <button type="submit"><i class="icon wb-trash p-5" style="color: #76838f;" aria-hidden="true"></i></button>
                        </form>
                      </td>
                    </tr>
                    <?php } } else { ?>
                    <tr><td colspan="999" class="text-center">No records.</td></tr>
                    <?php } ?>
                  </tbody>
                </table>

                <div class="text-right mt-30">
                  <form action="" method="post">
                    <div class="input-group">
                      <input type="text" name="add_subject" class="form-control py-20" placeholder="Subject Description">
                      <button type="submit" class="btn btn-primary input-group-addon">Add Subject</button>
                    </div>
                  </form>
                </div>

              </div>
            </div>
            <!-- End Panel Accordion -->
          </div>

          <div class="col-md-8">
            <!-- Panel Accordion -->
            <div class="panel">
              <div class="panel-body collapse show">

                <!-- Add Class -->
                <?php
                if (isset($_POST['add_class'])) {
                  $sql = "INSERT INTO class (`class_name`) VALUES ('";
                  $sql .= mysqli_real_escape_string( $dbCon , $_POST['add_class'] ) . "')";
                  if ( !(mysqli_query($dbCon, $sql)) ) { ?>
                  <div class="alert alert-warning alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    ERROR : <?php echo mysqli_error($dbCon); ?>
                  </div>
                  <?php } else { ?>
                  <div class="alert alert-success alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    SUCCESS : Class <strong><?php echo $_POST['add_class']; ?></strong> has been added.
                  </div>
                <?php }
                  $class_id_inserted = mysqli_insert_id($dbCon);
                  $sql = "INSERT INTO subject_class_relationship (`class_id`, `subject_id`) VALUES ";
                  foreach ($_POST as $key => $value) { 
                    if (strpos($key, 'add_class_subject_id_') === 0) {
                      $sql .= "(" . $class_id_inserted . ", " . str_replace('add_class_subject_id_', '', $key) . "), ";
                    }
                  }
                  $sql = substr( $sql, 0, (strlen($sql) - 2) );
                  if ( !(mysqli_query($dbCon, $sql)) ) { ?>
                  <div class="alert alert-warning alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    ERROR : <?php echo mysqli_error($dbCon); ?>
                  </div>
                  <?php } else { ?>
                  <div class="alert alert-success alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    SUCCESS : Subjects has been added to <strong><?php echo $_POST['add_class']; ?></strong>.
                  </div>
                <?php }
                } ?>

                <!-- Edit Class -->
                <?php
                if (isset($_POST['edit_class'])) {
                  $error = "";
                  $sql = "UPDATE class SET class_name='" . $_POST['edit_class_name'] . "' WHERE class_id=" . $_POST['edit_class'] . "";
                  if ( !(mysqli_query($dbCon, $sql)) ) $error .= mysqli_error($dbCon);

                  $sql = "DELETE FROM subject_class_relationship WHERE class_id=" . $_POST['edit_class']; 
                  if ( !(mysqli_query($dbCon, $sql)) ) $error = mysqli_error($dbCon);
                  $sql = "INSERT INTO subject_class_relationship (`class_id`, `subject_id`) VALUES ";
                  $count = 0;
                  foreach ($_POST as $key => $value) { 
                    if (strpos($key, 'add_class_subject_id_') === 0) {
                      $count++;
                      $sql .= "(" . $_POST['edit_class'] . ", " . str_replace('add_class_subject_id_', '', $key) . "), ";
                    }
                  }
                  $sql = substr( $sql, 0, (strlen($sql) - 2) );
                  if ($count == 0) $sql = "SELECT * FROM subject_class_relationship";
                  if ( !(mysqli_query($dbCon, $sql)) || !($error == "") ) { ?>
                  <div class="alert alert-warning alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    ERROR :  <?php echo (isset($error) ? $error : '') . mysqli_error($dbCon); ?>
                  </div>
                  <?php } else { ?>
                  <div class="alert alert-success alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    SUCCESS : Subjects in class <strong><?php echo $_POST['edit_class_name']; ?></strong> has been updated.
                  </div>
                <?php }
                } ?>

                <!-- Delete Class -->
                <?php
                if (isset($_POST['delete_class'])) {
                  $sql = "DELETE FROM subject_class_relationship WHERE class_id=" . $_POST['delete_class']; 
                  if ( !(mysqli_query($dbCon, $sql)) ) $error = mysqli_error($dbCon);
                  $sql = "DELETE FROM class WHERE class_id=" . $_POST['delete_class'];
                  if ( !(mysqli_query($dbCon, $sql)) ) { ?>
                  <div class="alert alert-warning alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    ERROR : <?php echo (isset($error) ? $error : '') . mysqli_error($dbCon); ?>
                  </div>
                  <?php } else { ?>
                  <div class="alert alert-success alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    SUCCESS : Class <strong><?php echo $_POST['delete_class_name']; ?></strong> (ID = <?php echo $_POST['delete_class']; ?>) has been deleted.
                  </div>
                <?php } } ?>

                <h4>Classes</h4>

                <table class="table table-hover table-bordered table-striped toggle-arrow-tiny table-sm text-center" id="class-delete-table">
                  <thead>
                    <tr>
                      <th class="py-10">ID</th>
                      <th class="py-10">Name</th>
                      <th class="py-10">Subjects</th>
                      <th class="py-10" style="min-width: 65px;">Actions</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php
                    $result = mysqli_query($dbCon, "SELECT * FROM class ORDER BY class_name");
                    if (mysqli_num_rows($result) > 0) { while($row_classes = mysqli_fetch_assoc($result)) { ?>
                    <tr>
                      <td><?php echo $row_classes['class_id'];?></td>
                      <td class="cursor-pointer py-10 px-15 text-left" data-toggle="modal" data-target="#class-edit-modal-<?php echo $row_classes['class_id'];?>"><?php echo $row_classes['class_name'];?></td>
                      <td class="cursor-pointer py-10 px-15 text-left" data-toggle="modal" data-target="#class-edit-modal-<?php echo $row_classes['class_id'];?>">
                        <?php
                        $result_subjects_in_classes = mysqli_query($dbCon, "SELECT * FROM subject_class_relationship WHERE class_id=".$row_classes['class_id']);
                        $subjects = '';
                        if (mysqli_num_rows($result_subjects_in_classes) > 0)
                          while($row_subjects_in_classes = mysqli_fetch_assoc($result_subjects_in_classes)) {
                            $result_name_subjects_in_classes = mysqli_query($dbCon, "SELECT * FROM subject WHERE subject_id=".$row_subjects_in_classes['subject_id']);
                            $subjects .= mysqli_fetch_assoc($result_name_subjects_in_classes)['subject_name'].', ';
                          }
                          $subjects = substr( $subjects, 0, (strlen($subjects) - 2) );
                          echo $subjects;
                        ?>
                      </td>
                      <td>
                        <a data-toggle="modal" data-target="#class-edit-modal-<?php echo $row_classes['class_id'];?>" href="javascript:void(0)"><i class="icon wb-pencil p-5" style="color: #76838f;" aria-hidden="true"></i></a>
                        <!-- Edit Class Modal -->
                        <div class="modal fade modal-fade-in-scale-up text-left" id="class-edit-modal-<?php echo $row_classes['class_id'];?>" aria-hidden="false" role="dialog" tabindex="-1">
                          <div class="modal-dialog modal-simple">
                            <form class="modal-content" action="" method="post">
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                  <span aria-hidden="true">×</span>
                                </button>
                                <h4 class="modal-title">Edit Class <strong><?php echo $row_classes['class_name'];?></strong></h4>
                              </div>
                              <div class="modal-body">
                                <div class="row">
                                  <div class="col-xl-7 form-group mx-auto">
                                    <input type="text" class="d-none" name="edit_class" value="<?php echo $row_classes['class_id'];?>" >
                                    <input type="text" class="d-none" name="edit_class_name_old" value="<?php echo $row_classes['class_name'];?>" >
                                    <input type="text" class="form-control" name="edit_class_name" placeholder="<?php echo $row_classes['class_name'];?>" value="<?php echo $row_classes['class_name'];?>" >
                                  </div>
                                </div>
                                <div class="row form-group">
                                  <?php
                                  $result_subjects = mysqli_query($dbCon, "SELECT * FROM subject ORDER BY subject_name");
                                  if (mysqli_num_rows($result_subjects) > 0) { while($row_subjects = mysqli_fetch_assoc($result_subjects)) { ?>
                                  <div class="col-xl-3">
                                    <div class="checkbox-custom checkbox-primary">
                                      <?php
                                      $checked = "";
                                      $result_subjects_in_class = mysqli_query($dbCon, "SELECT * FROM subject_class_relationship WHERE class_id=".$row_classes['class_id']." AND subject_id=".$row_subjects['subject_id']);
                                      if (mysqli_num_rows($result_subjects_in_class) > 0) $checked = "checked";
                                      ?>
                                      <input data-name="add_class_subject_id_<?php echo $row_subjects['subject_id'];?>" type="checkbox" id="subject-check-<?php echo $row_subjects['subject_id'];?>" <?php echo $checked === "checked" ? 'name="add_class_subject_id_'.$row_subjects['subject_id'].'"' : ''; ?> <?php echo $checked; ?> />
                                      <label for="subject-check-<?php echo $row_subjects['subject_id'];?>"><?php echo $row_subjects['subject_name'];?></label>
                                    </div>
                                  </div>
                                  <?php } } else { ?>
                                  <div class="col-xl-12 text-center">
                                    No subjects found.
                                  </div>
                                  <?php } ?>
                                </div>
                                <div class="float-right">
                                  <button class="btn btn-default mr-10" data-dismiss="modal" type="button">Close</button>
                                  <button class="btn btn-primary" type="submit">Save</button>
                                </div>
                              </div>
                            </form>
                          </div>
                        </div>
                        <!-- End Edit Class Modal -->
                        <form action="" method="post" class="form-button" >
                          <input type="text" class="d-none" name="delete_class" value="<?php echo $row_classes['class_id'];?>">
                          <input type="text" class="d-none" name="delete_class_name" value="<?php echo $row_classes['class_name'];?>">
                          <button type="submit"><i class="icon wb-trash p-5" style="color: #76838f;" aria-hidden="true"></i></button>
                        </form>
                      </td>
                    </tr>
                    <?php } } else { ?>
                    <tr><td colspan="999" class="text-center">No records.</td></tr>
                    <?php } ?>
                  </tbody>
                </table>

                <div class="float-right mt-14">
                  <button type="button" class="btn btn-primary input-group-addon" data-target="#add-class-modal" data-toggle="modal">Add a Class</button>
                  <!-- Modal -->
                  <div class="modal fade modal-fade-in-scale-up" id="add-class-modal" aria-hidden="false" role="dialog" tabindex="-1">
                    <div class="modal-dialog modal-simple">
                      <form class="modal-content" action="" method="post">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                          </button>
                          <h4 class="modal-title">Add a Class</h4>
                        </div>
                        <div class="modal-body">
                          <div class="row">
                            <div class="col-xl-7 form-group mx-auto">
                              <input type="text" class="form-control" name="add_class" placeholder="Grade - Section">
                            </div>
                          </div>
                          <div class="row form-group">
                            <?php
                            $result = mysqli_query($dbCon, "SELECT * FROM subject ORDER BY subject_name");
                            if (mysqli_num_rows($result) > 0) { while($row_subjects = mysqli_fetch_assoc($result)) { ?>
                            <div class="col-xl-3">
                              <div class="checkbox-custom checkbox-primary">
                                <input data-name="add_class_subject_id_<?php echo $row_subjects['subject_id'];?>" type="checkbox" id="subject-check-<?php echo $row_subjects['subject_id'];?>" />
                                <label for="subject-check-<?php echo $row_subjects['subject_id'];?>"><?php echo $row_subjects['subject_name'];?></label>
                              </div>
                            </div>
                            <?php } } else { ?>
                            <div class="col-xl-12 text-center">
                              No subjects found.
                            </div>
                            <?php } ?>
                          </div>
                          <div class="float-right">
                            <button class="btn btn-default mr-10" data-dismiss="modal" type="button">Close</button>
                            <button class="btn btn-primary" type="submit">Add Class</button>
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>
                  <!-- End Modal -->
                </div>

              </div>
            </div>
            <!-- End Panel Accordion -->
          </div>

        </div>
      </div>
    </div>
    <!-- End Page -->

<?php include("../footer.php"); ?>