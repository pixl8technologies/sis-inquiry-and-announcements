<?php include("../header.php"); ?>

    <!-- Page -->
    <div class="page view-all-page">
      <div class="page-header">
        <h1 class="page-title">Banner</h1>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="<?php echo $root_dir; ?>/">Home</a></li>
          <li class="breadcrumb-item"><a href="<?php echo $root_dir; ?>/admin">Admin</a></li>
          <li class="breadcrumb-item">Home Page Settings</li>
          <li class="breadcrumb-item active">Banner</li>
        </ol>
      </div>
      <div class="page-content container-fluid">
        <div class="row" data-plugin="matchHeight" data-by-row="true">
          <div class="col-lg-12">
            <!-- Panel Accordion -->
            <div class="panel">
              <div class="panel-body">
                <div class="row">
                  <!-- Save images to uploads folder then set url in database -->
                  <?php if( isset($_POST['input-image-1']) ) {
                    $sql = "DELETE FROM `image_carousel`;";
                    if ( !(mysqli_query($dbCon, $sql)) ) { ?>
                      <div class="col-lg-12">
                        <div class="alert alert-warning alert-dismissible" role="alert">
                          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                          ERROR : <?php echo mysqli_error($dbCon); ?>
                        </div>
                      </div>
                    <?php }
                    $sql = "INSERT INTO `image_carousel` ( `img_url`, `img_order_number`) VALUES ";
                    foreach($_POST as $key => $value) {
                      if (substr($key, 0, 11) === 'input-image') {
                        if (substr($value, 0, 10) === 'data:image') {
                          $base64 = explode('base64,', $value)[1];
                          $filename = str_replace('input', 'carousel', $key) . '.' . explode('/', explode(';base64,', $value)[0])[1];
                        } else {
                          $base64 = base64_encode(file_get_contents( '../../assets/uploads/' . explode('/uploads/', $value)[1] ));
                          $filename = str_replace('input', 'carousel', $key) . '.' . explode('.', $value)[ count(explode('.', $value)) - 1 ];
                        }
                        file_put_contents('../../assets/uploads/' . $filename, base64_decode($base64));
                        $sql .= "('" . $filename . "', " . str_replace('input-image-', '', $key) . "), ";
                      }
                    }
                    $sql = substr($sql, 0, strlen($sql) - 2);
                    $sql .= ";";
                    if ( !(mysqli_query($dbCon, $sql)) ) { ?>
                      <div class="col-lg-12">
                        <div class="alert alert-warning alert-dismissible" role="alert">
                          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                          ERROR : <?php echo mysqli_error($dbCon); ?>
                        </div>
                      </div>
                    <?php } else { ?>
                      <div class="col-lg-12">
                        <div class="alert alert-success alert-dismissible" role="alert">
                          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                          SUCCESS : The slider images has been updated.
                        </div>
                      </div>
                    <?php }
                  } ?>
                </div>
                <form action="" method="post" id="home-banner-settings" class="form-images">
                  <div class="row">
                    <?php
                    $count = 1;
                    $sql = "SELECT * FROM image_carousel ORDER BY img_order_number";
                    $result = mysqli_query($dbCon, $sql);
                    if (mysqli_num_rows($result) > 0) { while($row = mysqli_fetch_assoc($result)) { ?>
                    <div class="col-lg-4 home-banner-item">
                      <div class="form-group">
                        <h5 class="text-center">Carousel Image <?php echo $count++; ?></h5>
                        <input class="input-image" type="file" data-plugin="dropify" data-max-file-size="2M" data-default-file="<?php echo $root_dir; ?>/assets/uploads/<?php echo $row['img_url']; ?>" />
                      </div>
                    </div>
                    <?php } } ?>
                    <div class="col-lg-4 home-banner-item">
                      <div class="form-group">
                        <h5 class="text-center">Carousel Image <?php echo $count++; ?></h5>
                        <input class="input-image" type="file" data-plugin="dropify" data-max-file-size="2M"/>
                      </div>
                    </div>
                    <?php while($count <= 12) { ?>
                    <div class="col-lg-4 home-banner-item d-none">
                      <div class="form-group">
                        <h5 class="text-center">Carousel Image <?php echo $count++; ?></h5>
                        <input class="input-image" type="file" data-plugin="dropify" data-max-file-size="2M"/>
                      </div>
                    </div>
                    <?php } ?>
                  </div>
                  <div class="row">
                    <div class="col-12">
                      <div class="form-group text-right">
                        <a href="" class="btn btn-default mr-10">Reset</a>
                        <a href="javascript:void(0)" class="submit-images btn btn-primary">Save</a>
                      </div>
                    </div>
                  </div>
                </form>
              </div>
            </div>
            <!-- End Panel Accordion -->
          </div>
        </div>
      </div>
    </div>
    <!-- End Page -->

<?php include("../footer.php"); ?>