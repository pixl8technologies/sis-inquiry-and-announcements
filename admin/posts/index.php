<?php include("../header.php"); ?>

    <!-- Page -->
    <div class="page view-all-page">
    <!--Header-->
      <div class="page-header">

        <h1 class="page-title">Posts</h1>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="<?php echo $root_dir; ?>/">Home</a></li>
          <li class="breadcrumb-item"><a href="<?php echo $root_dir; ?>/admin">Admin</a></li>
          <li class="breadcrumb-item active">Posts</li>
        </ol>
      </div>
    <!-- End Header-->
<!-- Modal -->
          <div class="modal fade" id="exampleFormModal" aria-hidden="false" aria-labelledby="exampleFormModalLabel"
            role="dialog" tabindex="-1">
            <div class="modal-dialog modal-simple modal-lg">
              <form action="post_add.php" id="formpost" method="post" class="admin-wysiwyg-form modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                  </button>
                  <h4 class="modal-title" id="exampleFormModalLabel">Create a new post</h4>
                </div>

                <div class="modal-body">

                  <div class="row">
                    <div class="col-md-7">
                      <div class="form-group form-material floating" data-plugin="formMaterial">
                        <input type="text" class="form-control form-control-lg" name="post_title" value="" />
                        <label class="floating-label">Title</label>
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <div class="input-daterange" data-date-format="yyyy-mm-dd" data-plugin="datepicker">
                          <div class="input-group" id="ifNo">
                            <span class="input-group-addon">
                              <i class="icon wb-calendar" aria-hidden="true"></i>
                            </span>
                            <input type="text" class="form-control" data-date-format="yy-mm-dd" name="post_date_start">
                          </div>
                          <div class="input-group" id="ifYes">
                            <span class="input-group-addon">to</span>
                            <input type="text" class="form-control" data-date-format="yy-mm-dd" name="post_date_end">
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="col-lg-6">
                      <div class="form-group">
                        <div class="radio-custom radio-primary">
                          <input type="radio" onclick="javascript:yesnoCheck();" value="1" id="check_announcement" name="type_check">
                          <label for="check_announcement">Announcement</label>
                        </div>
                        <div class="radio-custom radio-primary">
                          <input type="radio" onclick="javascript:yesnoCheck();" value="2" id="check_event" name="type_check" required>
                          <label for="check_event">Event</label>
                        </div>
                      </div>
                    </div>                    
                  </div>

                  <div class="row">
                    <div class="col-lg-12">
                      <div class="form-group">
                      
                      <textarea name="post_content" id="post_content" class=" form-control" rows="6"></textarea>
                      </div>
                      <div class="form-group text-right">
                        <button type="reset" class="btn btn-default mr-10">Reset</button>
                        <button name ="submit" class="admin-wysiwyg-submit btn btn-primary">Save</button>
                      </div>
                    </div>
                  </div>
                    
              </div>
              </form>
            </div>
          </div>
          <!-- End Modal -->
      
      <div class="page-content container-fluid">
        <div class="row" data-plugin="matchHeight" data-by-row="true">
          <div class="col-lg-12">

            <!-- Panel Accordion -->
            <div class="panel">
              <div class="panel-body collapse show" id="exampleFooAccordionPanel">
                <?php
                $rows_per_page = (isset($_GET['rows'])) ? $_GET['rows'] : 10;
                $post_count = mysqli_fetch_array( mysqli_query( $dbCon, "SELECT COUNT(`post_id`) FROM post" ) )[0];
                $page_count = ceil((int)$post_count / $rows_per_page);
                if (isset($_GET['page'])) {
                  if ( $_GET['page'] <= $page_count )
                    $current_page = $_GET['page'];
                  else {
                    $current_page = $page_count;
                    $_GET['page'] = $current_page;
                  }
                }
                else
                  $current_page = 1;
                ?>
                <div class="row mb-15">

                  <div class="col-md-6">
                    <div class="btn-group">
                    <button type="button" class="btn btn-outline btn-default" data-target="#exampleFormModal" data-toggle="modal">
                      <i class="icon wb-plus" aria-hidden="true"></i>
                    </button>
                    <div class="btn-group">
                        <button type="button" class="btn btn-outline dropdown-toggle btn-default" data-toggle="dropdown" aria-expanded="false"
                          aria-hidden="true" style="height: 38px;"><i class="icon wb-list-numbered" aria-hidden="true"></i></button>
                        <div class="dropdown-menu" role="menu" x-placement="top-start" style="position: absolute; transform: translate3d(0px, -139px, 0px); top: 0px; left: 0px; will-change: transform;">
                          <?php $query = $_GET; $query['rows'] = 10; $query_result = http_build_query($query); ?>
                          <a class="dropdown-item" href="<?php echo $_SERVER['PHP_SELF']; ?>?<?php echo $query_result; ?>" role="menuitem">10 rows per page</a>
                          <?php $query = $_GET; $query['rows'] = 20; $query_result = http_build_query($query); ?>
                          <a class="dropdown-item" href="<?php echo $_SERVER['PHP_SELF']; ?>?<?php echo $query_result; ?>" role="menuitem">20 rows per page</a>
                          <?php $query = $_GET; $query['rows'] = 30; $query_result = http_build_query($query); ?>
                          <a class="dropdown-item" href="<?php echo $_SERVER['PHP_SELF']; ?>?<?php echo $query_result; ?>" role="menuitem">30 rows per page</a>
                          <?php $query = $_GET; $query['rows'] = 50; $query_result = http_build_query($query); ?>
                          <a class="dropdown-item" href="<?php echo $_SERVER['PHP_SELF']; ?>?<?php echo $query_result; ?>" role="menuitem">50 rows per page</a>
                          <?php $query = $_GET; $query['rows'] = 100; $query_result = http_build_query($query); ?>
                          <a class="dropdown-item" href="<?php echo $_SERVER['PHP_SELF']; ?>?<?php echo $query_result; ?>" role="menuitem">100 rows per page</a>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="col-md-6 text-right">
                    <span class="label label-default mr-15">
                    <?php echo (($current_page - 1) * $rows_per_page) + 1; ?> to
                    <?php echo ($current_page == $page_count) ? (($current_page * $rows_per_page) - (($current_page * $rows_per_page) - $post_count)) : ($current_page * $rows_per_page); ?> &nbsp; of
                    <?php echo $post_count; ?>
                    </span>
                    <div class="btn-group" aria-label="Default button group" role="group">
                      <button type="button" class="btn btn-outline btn-default"><i class="icon wb-chevron-left-mini" aria-hidden="true"></i></button>
                      <?php for ($x = 1; $x <= $page_count; $x++) { ?>
                      <?php
                      $query = $_GET;
                      $query['page'] = $x;
                      $query_result = http_build_query($query);
                      ?>
                      <a class="btn <?php echo ($current_page == $x) ? ' btn-primary' : 'btn-outline btn-default'; ?>" href="<?php echo $_SERVER['PHP_SELF']; ?>?<?php echo $query_result; ?>"><?php echo $x; ?></a>
                      <?php } ?>
                      <button type="button" class="btn btn-outline btn-default"><i class="icon wb-chevron-right-mini" aria-hidden="true"></i></button>
                    </div>
                  </div>
                </div>

                <table class="table table-hover table-bordered table-striped toggle-arrow-tiny table-sm text-center" id="all-posts-table">
                  <thead>
                    <tr>
                      <th class="py-10">ID</th>
                      <th class="py-10">Title</th>
                      <th class="py-10">Content</th>
                      <th class="py-10" style="min-width: 130px;">Date Created</th>
                      <th class="py-10">Type</th>
                      <th class="py-10" style="min-width: 63px;">Actions</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php
                    $sql = "SELECT * FROM post ORDER BY post_id DESC LIMIT " . $rows_per_page . " OFFSET " . (($current_page - 1) * $rows_per_page);
                    $result = mysqli_query($dbCon, $sql);
                    if (mysqli_num_rows($result) > 0) {
                      while($row = mysqli_fetch_assoc($result)) { ?>
                      <tr>

                      <td><?php echo $row['post_id']; ?></td>
                      <td data-toggle="modal" data-target="#edit<?php echo $row['post_id']; ?>" class="cursor-pointer py-10 px-25"><?php echo $row['post_title']; ?></td>
                      <td data-toggle="modal" data-target="#edit<?php echo $row['post_id']; ?>" class="cursor-pointer py-10 px-25"><?php echo (strlen($row['post_content']) > 100 ) ? substr($row['post_content'], 0, 90)." ..." : $row['post_content']; ?></td>
                      <td data-toggle="modal" data-target="#edit<?php echo $row['post_id']; ?>" class="cursor-pointer py-10 px-25"><?php echo date('M d, Y', strtotime($row['post_date_created'])) ?></td>
                      <td data-toggle="modal" data-target="#edit<?php echo $row['post_id']; ?>" class="cursor-pointer py-10 px-25">
                        <?php
                          if($row['post_type'] == "an"){
                            echo "Announcement";
                             $ann = 'checked';
                             $eve = ' ';
                          }elseif($row['post_type'] == "ev"){
                            echo "Event";
                            $ann = ' ';
                            $eve = 'checked';
                          }
                          else{
                            $ann = ' ';
                            $eve = 'checked';
                          }

                        ?>
                      </td>
                      <td>
                        <a data-toggle="modal" href="#edit<?php echo $row['post_id']; ?>"><i class="icon wb-pencil p-5" style="color: #76838f;" aria-hidden="true"></i></a>
                        <a href="post_delete.php?id=<?php echo $row['post_id']; ?>"><i class="icon wb-trash p-5" onclick="return checkDelete()" style="color: #76838f;" aria-hidden="true"></i></a>

              <!--MODAL EDIT-->
                    <div class="modal fade text-left" id="edit<?php echo $row['post_id']; ?>" aria-hidden="false" aria-labelledby="exampleFormModalLabel"
                      role="dialog" tabindex="-1">
                      <div class="modal-dialog modal-simple modal-lg">
                        <form action="post_edit.php" method="post" class="modal-content" id="editsss">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">×</span>
                            </button>
                            <h4 class="modal-title" id="exampleFormModalLabel2">Edit a post</h4>
                          </div>

                          <div class="modal-body">
                              <div class="row">
                                <div class="col-md-7">
                                  <div class="form-group form-material floating" data-plugin="formMaterial">
                                    <input type="text" class="form-control form-control-lg" name="post_title" value="<?php echo $row['post_title']; ?>" />
                                    <label class="floating-label">Title</label>
                                  </div>
                                </div>
                              </div>

                              <div class="row">
                                <div class="col-md-6">
                                  <div class="form-group">
                                    <div class="input-daterange" data-date-format="yyyy-mm-dd" data-plugin="datepicker">
                                      <div class="input-group" id="">
                                        <span class="input-group-addon">
                                          <i class="icon wb-calendar" aria-hidden="true"></i>
                                        </span>
                                        <input type="text" class="form-control" data-date-format="yyyy-mm-dd" value="<?php echo $row['post_date_start']; ?>" name="post_date_start">
                                      </div>
                                      <div class="input-group" id="endDate-<?php echo $row['post_id']; ?>">
                                        <span class="input-group-addon">to</span>
                                        <input type="text" class="form-control" data-date-format="yyyy-mm-dd" value="<?php echo $row['post_date_end']; ?>" name="post_date_end">
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="col-lg-6">
                                  <div class="form-group">
                                  <div class="radio-custom radio-primary">
                                    <input type="radio" value="1" onclick="hideEndDate('endDate-<?php echo $row['post_id']; ?>')"  id="check_announcement2" name="type_check" <?php echo $ann; ?> >
                                    <label for="check_announcement2">Announcement</label>
                                  </div>
                                  <div class="radio-custom radio-primary">
                                    <input type="radio"  value="2" onclick="showEndDate('endDate-<?php echo $row['post_id']; ?>')" id="check_event2" name="type_check" <?php echo $eve; ?> >
                                    <label for="check_event2">Event</label>
                                  </div>
                                </div>
                              </div>
                              </div>

                              <div class="row">
                                <div class="col-lg-12">
                                  <div class="form-group">
                                  <input type="hidden" name="postId" value="<?php echo $row['post_id']?>">
                                  <textarea name="post_content" class=" form-control" rows="6"><?php echo $row['post_content']; ?></textarea>
                                  </div>
                                  <div class="form-group text-right">
                                    <button type="reset" class="btn btn-default mr-10">Reset</button>
                                    <button name ="submit" class="btn btn-primary">Save</button>
                                  </div>
                                </div>
                              </div>
                          </div>
                        </form>
                      </div>
                    </div>
            <!--end modal-->
                                        
                      </td>
                    </tr>
                    <?php } 
                    }?>
                  </tbody>
                </table>

              </div>
            </div>
            <!-- End Panel Accordion -->
          </div>
        </div>
      </div>
    </div>
    <!-- End Page -->


<?php include("../footer.php"); ?>