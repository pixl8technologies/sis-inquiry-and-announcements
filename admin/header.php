<?php
session_start();
$url1 = substr($_SERVER['REQUEST_URI'], strpos($_SERVER['REQUEST_URI'], 'admin/') );
$in_admin_home = false;
if ($url1 === 'admin/' || $url1 === 'admin' || $url1 === 'admin/index.php') $in_admin_home = true;
if(isset($_SESSION['u_id'])){
  if($_SESSION['user_type']== 1){
    $userId = $_SESSION['u_id'];
    $fName = $_SESSION['u_fname'];
  }else{
    header(($in_admin_home == true) ? 'Location: ../log-in/?notadmin' : 'Location: ../../log-in/?notadmin');
  }
}else{
  header(($in_admin_home == true) ? 'Location: ../log-in/?notlogged' : 'Location: ../../log-in/?notlogged');
}

include(($in_admin_home == true) ? '../config.php' : '../../config.php');
include(($in_admin_home == true) ? '../db_connection.php' : '../../db_connection.php');
$logOutDir = ($in_admin_home == true)? '../includes/logout.php' : '../../includes/logout.php'; 

?> 


<!DOCTYPE html>
<html class="no-js css-menubar" lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="bootstrap admin template">
    <meta name="author" content="">
    
    <title>Dashboard | Paco Catholic School</title>
    
    <link rel="apple-touch-icon" href="<?php echo $root_dir; ?>/admin/remark/base/assets/images/apple-touch-icon.png">
    <link rel="shortcut icon" href="<?php echo $root_dir; ?>/admin/remark/base/assets/images/favicon2.ico">
    
    <!-- Stylesheets -->
    <link rel="stylesheet" href="<?php echo $root_dir; ?>/admin/remark/global/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo $root_dir; ?>/admin/remark/global/css/bootstrap-extend.min.css">
    <link rel="stylesheet" href="<?php echo $root_dir; ?>/admin/remark/base/assets/css/site.min.css">
    
    <!-- Plugins -->
    <link rel="stylesheet" href="<?php echo $root_dir; ?>/admin/remark/global/vendor/animsition/animsition.css">
    <link rel="stylesheet" href="<?php echo $root_dir; ?>/admin/remark/global/vendor/asscrollable/asScrollable.css">
    <link rel="stylesheet" href="<?php echo $root_dir; ?>/admin/remark/global/vendor/switchery/switchery.css">
    <link rel="stylesheet" href="<?php echo $root_dir; ?>/admin/remark/global/vendor/intro-js/introjs.css">
    <link rel="stylesheet" href="<?php echo $root_dir; ?>/admin/remark/global/vendor/slidepanel/slidePanel.css">
    <link rel="stylesheet" href="<?php echo $root_dir; ?>/admin/remark/global/vendor/flag-icon-css/flag-icon.css">
    <link rel="stylesheet" href="<?php echo $root_dir; ?>/admin/remark/global/vendor/chartist/chartist.css">
    <link rel="stylesheet" href="<?php echo $root_dir; ?>/admin/remark/global/vendor/jvectormap/jquery-jvectormap.css">
    <link rel="stylesheet" href="<?php echo $root_dir; ?>/admin/remark/global/vendor/chartist-plugin-tooltip/hartist-plugin-tooltip.css">
    <link rel="stylesheet" href="<?php echo $root_dir; ?>/admin/remark/base/assets/examples/css/dashboard/v1.css">
    <link rel="stylesheet" href="<?php echo $root_dir; ?>/admin/remark/global/vendor/bootstrap-table/bootstrap-table.css">
    <link rel="stylesheet" href="<?php echo $root_dir; ?>/admin/remark/global/vendor/summernote/summernote.css">
    <link rel="stylesheet" href="<?php echo $root_dir; ?>/admin/remark/base/assets/examples/css/uikit/modals.css">
    <link rel="stylesheet" href="<?php echo $root_dir; ?>/admin/remark/global/vendor/bootstrap-datepicker/bootstrap-datepicker.css">
    <link rel="stylesheet" href="<?php echo $root_dir; ?>/admin/remark/global/vendor/timepicker/jquery-timepicker.css">
    <link rel="stylesheet" href="<?php echo $root_dir; ?>/admin/remark/base/assets/examples/css/pages/user.css">
    <link rel="stylesheet" href="<?php echo $root_dir; ?>/admin/remark/global/vendor/select2/select2.css">
    <link rel="stylesheet" href="<?php echo $root_dir; ?>/admin/remark/base/assets/examples/css/forms/advanced.css">
        
    <!--Upload-->
    <link rel="stylesheet" href="<?php echo $root_dir; ?>/admin/remark/global/vendor/blueimp-file-upload/jquery.fileupload.css">
    <link rel="stylesheet" href="<?php echo $root_dir; ?>/admin/remark/global/vendor/dropify/dropify.css">
    
    <!-- Fonts -->
    <link rel="stylesheet" href="<?php echo $root_dir; ?>/admin/remark/global/fonts/weather-icons/weather-icons.css">
    <link rel="stylesheet" href="<?php echo $root_dir; ?>/admin/remark/global/fonts/web-icons/web-icons.min.css">
    <link rel="stylesheet" href="<?php echo $root_dir; ?>/admin/remark/global/fonts/brand-icons/brand-icons.min.css">
    <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Roboto:300,400,500,300italic'>
    <link rel="stylesheet" href="<?php echo $root_dir; ?>/admin/remark/global/fonts/octicons/octicons.css">
    
    <!-- Custom styles -->
    <style>
    .form-button { display: inline; }
    .form-button button { background: none; border: none; padding: 0; cursor: pointer; }
    .cursor-pointer { cursor: pointer; }
    .ui-helper-hidden-accessible { display: none; }
    .modal-open .select2-container { z-index: 1; }
    .modal-open .modal .select2-container { z-index: 1; }
    button:focus { outline: none; box-shadow: none; }
    </style>

    <!-- Scripts -->
    <script language="JavaScript" type="text/javascript">
    function checkDelete(){
      return confirm('Are you sure?');
    }
    </script>
    <script src="<?php echo $root_dir; ?>/admin/remark/global/vendor/breakpoints/breakpoints.js"></script>
    <script>
      Breakpoints();
    </script>

    <script type="text/javascript">

    function yesnoCheck() {
        if (document.getElementById('check_event').checked) {
            document.getElementById('ifYes').style.visibility = 'visible';
        }
        else{ document.getElementById('ifYes').style.visibility = 'hidden'
        }

        if (document.getElementById('check_event2').checked) {
          document.getElementById('ifYes2').style.visibility = 'visible';
        }
        else{ document.getElementById('ifYes2').style.visibility = 'hidden';}
    }

    </script>
    <script language = "JavaScript" type="text/javascript">
    function hideEndDate(a){
      document.getElementById(a).style.display = 'none';
    }
    function showEndDate(a){
      document.getElementById(a).style.display = '';
    }
    </script>
  </head>
  <body class="animsition dashboard">

    <div id="home-url" class="d-none"><?php echo $root_dir; ?></div>

    <nav class="site-navbar navbar navbar-default navbar-fixed-top navbar-mega" role="navigation">
    
      <div class="navbar-header">
        <button type="button" class="navbar-toggler collapsed">
          <i class="icon wb-more-horizontal" aria-hidden="true"></i>
        </button>
        <div class="navbar-brand navbar-brand-center site-gridmenu-toggle">
          <img class="navbar-brand-logo" src="<?php echo $root_dir; ?>/admin/remark/base/assets/images/logo3.png" title="Remark">
          <span class="navbar-brand-text hidden-xs-down"> Dashboard </span>
        </div>
        <button type="button" class="navbar-toggler collapsed" data-target="#site-navbar-search"
          data-toggle="collapse">
          <span class="sr-only">Toggle Search</span>
          <i class="icon wb-search" aria-hidden="true"></i>
        </button>
      </div>
    
      <div class="navbar-container container-fluid">
        <!-- Navbar Collapse -->
        <div class="collapse navbar-collapse navbar-collapse-toolbar" id="site-navbar-collapse">
          <!-- Navbar Toolbar -->
          <ul class="nav navbar-toolbar">
            <li class="nav-item hidden-float" id="toggleMenubar">
              <a class="nav-link" data-toggle="menubar" href="#" role="button">
                <i class="icon hamburger hamburger-arrow-left">
                  <span class="sr-only">Toggle menubar</span>
                  <span class="hamburger-bar"></span>
                </i>
              </a>
            </li>
            <li class="nav-item hidden-sm-down" id="toggleFullscreen">
              <a class="nav-link icon icon-fullscreen" data-toggle="fullscreen" href="#" role="button">
                <span class="sr-only">Toggle fullscreen</span>
              </a>
            </li>
            <li class="nav-item hidden-float">
              <a class="nav-link icon wb-search" data-toggle="collapse" href="#" data-target="#site-navbar-search"
                role="button">
                <span class="sr-only">Toggle Search</span>
              </a>
            </li>
          </ul>
          <!-- End Navbar Toolbar -->
    
          <!-- Navbar Toolbar Right -->
          <?php
          $sql = "SELECT * FROM general_settings WHERE gs_id=1";
          $result = mysqli_query($dbCon, $sql);
          if (mysqli_num_rows($result) > 0) { $row = mysqli_fetch_assoc($result); }
          ?>
          <ul class="nav navbar-toolbar navbar-right navbar-toolbar-right">
            <li class="nav-item dropdown">
              <a class="nav-link" href="<?php echo $root_dir; ?>/" role="button">
                <h4 class="my-0"><?php echo isset($row['gs_name']) ? $row['gs_name'] . ' - ' : '' ?>Content Management System</h4>
              </a>
            </li>
            <li class="nav-item dropdown">
              <a href="<?php echo $root_dir; ?>/" role="button">
                <img class="py-10 pr-30 pl-5" style="height: 65px;" src="<?php echo $root_dir; ?>/assets/uploads/<?php echo $row['gs_logo_url'] ?>" alt="">
              </a>
            </li>
          </ul>
          <!-- End Navbar Toolbar Right -->

        </div>
      </div>
    </nav>    
    
    <div class="site-menubar">
      <div class="site-menubar-body">
        <div>
          <div>
            <ul class="site-menu" data-plugin="menu">
              <li class="site-menu-category">Website</li>

              <li class="site-menu-item has-sub">
                <a href="<?php echo $root_dir; ?>/admin/general">
                  <i class="site-menu-icon wb-settings" aria-hidden="true"></i>
                  <span class="site-menu-title">General</span>
                </a>
              </li>

              <li class="site-menu-item has-sub">
                <a href="javascript:void(0)">
                  <i class="site-menu-icon wb-home" aria-hidden="true"></i>
                  <span class="site-menu-title">Home Page</span>
                  <span class="site-menu-arrow"></span>
                    </a>
                <ul class="site-menu-sub">
                  <li class="site-menu-item">
                    <a class="animsition-link" href="<?php echo $root_dir; ?>/admin/banner">
                      <span class="site-menu-title">Banner</span>
                    </a>
                  </li>
                  <li class="site-menu-item">
                      <a class="animsition-link" href="<?php echo $root_dir; ?>/admin/featured">
                      <span class="site-menu-title">Featured</span>
                    </a>
                  </li>
                </ul>
              </li>

              <li class="site-menu-item has-sub">
                <a href="<?php echo $root_dir; ?>/admin/posts">
                  <i class="site-menu-icon wb-calendar" aria-hidden="true"></i>
                  <span class="site-menu-title">Calendar</span>
                </a>
              </li>

              <li class="site-menu-item has-sub">
                <a href="<?php echo $root_dir; ?>/admin/contact">
                    <i class="site-menu-icon wb-map" aria-hidden="true"></i>
                    <span class="site-menu-title">Contact Us Page</span>
                </a>
              </li>

              <li class="site-menu-item has-sub">
                <a href="javascript:void(0)">
                        <i class="site-menu-icon wb-info-circle" aria-hidden="true"></i>
                        <span class="site-menu-title">About Page</span>
                                <span class="site-menu-arrow"></span>
                    </a>
                <ul class="site-menu-sub">
                  <li class="site-menu-item">
                    <a class="animsition-link" href="<?php echo $root_dir; ?>/admin/history">
                      <span class="site-menu-title">History</span>
                    </a>
                  </li>
                  <li class="site-menu-item">
                    <a class="animsition-link" href="<?php echo $root_dir; ?>/admin/mission-vision">
                      <span class="site-menu-title">Mission &amp; Vision</span>
                    </a>
                  </li>
                  <li class="site-menu-item">
                    <a class="animsition-link" href="<?php echo $root_dir; ?>/admin/core-values">
                      <span class="site-menu-title">Core Values</span>
                    </a>
                  </li>
                  <li class="site-menu-item">
                    <a class="animsition-link" href="<?php echo $root_dir; ?>/admin/administration">
                      <span class="site-menu-title">Administration</span>
                    </a>
                  </li>
                  <li class="site-menu-item">
                    <a class="animsition-link" href="<?php echo $root_dir; ?>/admin/personnel">
                      <span class="site-menu-title">Personnel</span>
                    </a>
                  </li>
                </ul>
              </li>
              <li class="site-menu-category">Portal</li>

              <li class="site-menu-item has-sub">
                <a href="<?php echo $root_dir; ?>/admin/teachers">
                  <i class="site-menu-icon wb-user" aria-hidden="true"></i>
                  <span class="site-menu-title">Teachers</span>
                </a>
              </li>

              <li class="site-menu-item has-sub">
                <a href="<?php echo $root_dir; ?>/admin/students">
                  <i class="site-menu-icon wb-user-circle" aria-hidden="true"></i>
                  <span class="site-menu-title">Students</span>
                </a>
              </li>

              <li class="site-menu-item has-sub">
                <a href="<?php echo $root_dir; ?>/admin/subjects-and-classes">
                  <i class="site-menu-icon wb-library" aria-hidden="true"></i>
                  <span class="site-menu-title">Subjects & Classes</span>
                </a>
              </li>

          </ul>
     </div>
        </div>
      </div>
    
      <div class="site-menubar-footer">
        <a href="javascript: void(0);" class="fold-show" data-placement="top" data-toggle="tooltip"
          data-original-title="Settings">
          <span class="icon wb-settings" aria-hidden="true"></span>
        </a>
        <a href="javascript: void(0);" data-placement="top" data-toggle="tooltip" data-original-title="Lock">
          <span class="icon wb-eye-close" aria-hidden="true"></span>
        </a>
        <a href="<?php echo $logOutDir; ?>" data-placement="top" data-toggle="tooltip" data-original-title="Logout">
          <span class="icon wb-power" aria-hidden="true"></span>
        </a>
      </div></div>