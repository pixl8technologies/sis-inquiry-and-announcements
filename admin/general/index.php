<?php include("../header.php"); ?>

    <!-- Page -->
    <div class="page view-all-page">

      <div class="page-header">
        <h1 class="page-title">General</h1>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="<?php echo $root_dir; ?>/">Home</a></li>
          <li class="breadcrumb-item"><a href="<?php echo $root_dir; ?>/admin">Admin</a></li>
          <li class="breadcrumb-item active">General</li>
        </ol>
      </div>
      <div class="page-content container-fluid">
        <div class="row" data-plugin="matchHeight" data-by-row="true">

          <div class="col-md-4">
            <div class="panel">
              <div class="panel-body">
              
                <div class="row">
                  <!-- Save image to uploads folder then set url in database -->
                  <?php if( isset($_POST['input-image-1']) ) {
                    $sql = "INSERT INTO general_settings (gs_id, gs_logo_url) VALUES ";
                    if (substr($_POST['input-image-1'], 0, 10) === 'data:image') {
                      $base64 = explode('base64,', $_POST['input-image-1'])[1];
                      $filename = 'logo-img.' . explode('/', explode(';base64,', $_POST['input-image-1'])[0])[1];
                    } else {
                      $base64 = base64_encode(file_get_contents( '../../assets/uploads/' . explode('/uploads/', $_POST['input-image-1'])[1] ));
                      $filename = 'logo-img.' . explode('.', $_POST['input-image-1'])[ count(explode('.', $_POST['input-image-1'])) - 1 ];
                    }
                    file_put_contents('../../assets/uploads/' . $filename, base64_decode($base64));
                    $sql .= "(1, '" . $filename . "') ON DUPLICATE KEY UPDATE gs_logo_url = '" . $filename . "';";
                    if ( !(mysqli_query($dbCon, $sql)) ) { ?>
                      <div class="col-lg-12">
                        <div class="alert alert-warning alert-dismissible" role="alert">
                          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                          ERROR : <?php echo mysqli_error($dbCon); ?>
                        </div>
                      </div>
                    <?php } else { ?>
                      <div class="col-lg-12">
                        <div class="alert alert-success alert-dismissible" role="alert">
                          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                          SUCCESS : Logo has been set.
                        </div>
                      </div>
                    <?php }
                  } ?>
                </div>

                <form action="" method="post" class="form-images">
                  <div class="form-group">
                    <h5>Select a logo to be displayed in the site's header</h5>
                    <?php $result = mysqli_query($dbCon, "SELECT * FROM general_settings WHERE gs_id=1"); $roww = mysqli_fetch_assoc($result); ?>
                    <input class="input-image" type="file" data-plugin="dropify" data-max-file-size="5M" data-height="200" data-default-file="<?php echo isset($roww['gs_logo_url']) ? $root_dir . '/assets/uploads/' . $roww['gs_logo_url'] : ''; ?>"/>
                  </div>
                  <div class="form-group text-right">
                    <a href="" class="btn btn-default mr-10">Reset</a>
                    <a href="javascript:void(0)" class="submit-images btn btn-primary">Save</a>
                  </div>
                </form>

              </div>
            </div>
          </div>

          <div class="col-md-8">

            <?php if (isset($_POST['submit'])){
            $nname= mysqli_real_escape_string($dbCon, $_POST['gen_name']);
            $nsub= mysqli_real_escape_string($dbCon, $_POST['gen_subtitle']);
            $nadd= mysqli_real_escape_string($dbCon,$_POST['gen_address']);
            $ncon= mysqli_real_escape_string($dbCon,$_POST['gen_contact']);
            $sql ="INSERT INTO general_settings (gs_id, gs_name, gs_founded_content, gs_address, gs_contact_number) Values (1,'$nname','$nsub', '$nadd', '$ncon') ON DUPLICATE KEY UPDATE gs_name='$nname', gs_founded_content='$nsub', gs_address='$nadd', gs_contact_number='$ncon'";
            if( !(mysqli_query($dbCon, $sql)) ) { ?>
            <div class="alert alert-warning alert-dismissible" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>ERROR : <?php echo mysqli_error($dbCon); ?>
            </div>
            <?php } else { ?>
            <div class="alert alert-success alert-dismissible" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>SUCCESS : The General settings has been updated.
            </div>
            <?php } }?>

            <div class="panel">
              <div class="panel-body">
                <?php
                $sql = "SELECT * FROM general_settings WHERE gs_id=1";
                $result = mysqli_query($dbCon, $sql);
                if (mysqli_num_rows($result) > 0) { $row = mysqli_fetch_assoc($result); }
                else $row = array("gs_name"=>"", "gs_founded_content"=>"", "gs_address"=>"", "gs_contact_number"=>"");
                ?>

                <h5 class="mb-35">Edit website details</h5>
                
                <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
                  <div class="form-group form-material floating" data-plugin="formMaterial">
                    <input type="text" class="form-control" name="gen_name" value="<?php echo $row['gs_name']; ?>" />
                    <label class="floating-label">School Name</label>
                  </div>
                  <div class="form-group form-material floating" data-plugin="formMaterial">
                    <input type="text" class="form-control form-control-sm" name="gen_subtitle" value="<?php echo $row['gs_founded_content']; ?>" />
                    <label class="floating-label">Header Subtitle</label>
                  </div>
                  <div class="form-group form-material floating" data-plugin="formMaterial">
                    <input type="text" class="form-control" name="gen_address" value="<?php echo $row['gs_address']; ?>" />
                    <label class="floating-label">Address</label>
                  </div>
                  <div class="form-group form-material floating" data-plugin="formMaterial">
                    <input type="text" class="form-control" name="gen_contact" value="<?php echo $row['gs_contact_number']; ?>" />
                    <label class="floating-label">Contact No.</label>
                  </div>
                  <div class="form-group text-right">
                    <button type="reset" class="btn btn-default mr-10">Reset</button>
                    <button name="submit" type="submit" class="btn btn-primary">Save</button>
                  </div>
                </form>

              </div>
            </div>

          </div>

        </div>
      </div>
    </div>
    <!-- End Page -->

<?php include("../footer.php"); ?>