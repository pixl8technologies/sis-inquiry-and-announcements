

    <!-- Footer -->
    <footer class="site-footer">
      <div class="site-footer-legal">© 2018 <a href="http://themeforest.net/item/remark-responsive-bootstrap-admin-template/11989202">Admin Dashboard</a></div>
      <div class="site-footer-right">
        Created by <a href="mailto:pixl8technoloies@gmail.com">Pixl8 Technologies</a>
      </div>
    </footer>
    <!-- Core  -->
    <script src="<?php echo $root_dir; ?>/admin/remark/global/vendor/babel-external-helpers/babel-external-helpers.js"></script>
    <script src="<?php echo $root_dir; ?>/admin/remark/global/vendor/jquery/jquery.js"></script>
    <script src="<?php echo $root_dir; ?>/admin/remark/global/vendor/popper-js/umd/popper.min.js"></script>
    <script src="<?php echo $root_dir; ?>/admin/remark/global/vendor/bootstrap/bootstrap.js"></script>
    <script src="<?php echo $root_dir; ?>/admin/remark/global/vendor/animsition/animsition.js"></script>
    <script src="<?php echo $root_dir; ?>/admin/remark/global/vendor/mousewheel/jquery.mousewheel.js"></script>
    <script src="<?php echo $root_dir; ?>/admin/remark/global/vendor/asscrollbar/jquery-asScrollbar.js"></script>
    <script src="<?php echo $root_dir; ?>/admin/remark/global/vendor/asscrollable/jquery-asScrollable.js"></script>
    <script src="<?php echo $root_dir; ?>/admin/remark/global/vendor/ashoverscroll/jquery-asHoverScroll.js"></script>
    
    <!-- Plugins -->
    <script src="<?php echo $root_dir; ?>/admin/remark/global/vendor/switchery/switchery.js"></script>
    <script src="<?php echo $root_dir; ?>/admin/remark/global/vendor/intro-js/intro.js"></script>
    <script src="<?php echo $root_dir; ?>/admin/remark/global/vendor/screenfull/screenfull.js"></script>
    <script src="<?php echo $root_dir; ?>/admin/remark/global/vendor/slidepanel/jquery-slidePanel.js"></script>
        <script src="<?php echo $root_dir; ?>/admin/remark/global/vendor/skycons/skycons.js"></script>
        <!-- <script src="<?php echo $root_dir; ?>/admin/remark/global/vendor/chartist/chartist.min.js"></script>
        <script src="<?php echo $root_dir; ?>/admin/remark/global/vendor/chartist-plugin-tooltip/chartist-plugin-tooltip.js"></script> -->
        <script src="<?php echo $root_dir; ?>/admin/remark/global/vendor/aspieprogress/jquery-asPieProgress.min.js"></script>
        <script src="<?php echo $root_dir; ?>/admin/remark/global/vendor/jvectormap/jquery-jvectormap.min.js"></script>
        <script src="<?php echo $root_dir; ?>/admin/remark/global/vendor/jvectormap/maps/jquery-jvectormap-au-mill-en.js"></script>
        <script src="<?php echo $root_dir; ?>/admin/remark/global/vendor/matchheight/jquery.matchHeight-min.js"></script>

        <script src="<?php echo $root_dir; ?>/admin/remark/global/vendor/jquery-ui/jquery-ui.js"></script>
        <script src="<?php echo $root_dir; ?>/admin/remark/global/vendor/blueimp-tmpl/tmpl.js"></script>
        <script src="<?php echo $root_dir; ?>/admin/remark/global/vendor/blueimp-canvas-to-blob/canvas-to-blob.js"></script>
        <script src="<?php echo $root_dir; ?>/admin/remark/global/vendor/blueimp-load-image/load-image.all.min.js"></script>
        <script src="<?php echo $root_dir; ?>/admin/remark/global/vendor/blueimp-file-upload/jquery.fileupload.js"></script>
        <script src="<?php echo $root_dir; ?>/admin/remark/global/vendor/blueimp-file-upload/jquery.fileupload-process.js"></script>
        <script src="<?php echo $root_dir; ?>/admin/remark/global/vendor/blueimp-file-upload/jquery.fileupload-image.js"></script>
        <!--<script src="<?php echo $root_dir; ?>/admin/remark/global/vendor/blueimp-file-upload/jquery.fileupload-audio.js"></script>-->
        <!--<script src="<?php echo $root_dir; ?>/admin/remark/global/vendor/blueimp-file-upload/jquery.fileupload-video.js"></script>-->
        <script src="<?php echo $root_dir; ?>/admin/remark/global/vendor/blueimp-file-upload/jquery.fileupload-validate.js"></script>
        <script src="<?php echo $root_dir; ?>/admin/remark/global/vendor/blueimp-file-upload/jquery.fileupload-ui.js"></script>
        <script src="<?php echo $root_dir; ?>/admin/remark/global/vendor/dropify/dropify.min.js"></script>
        <script src="<?php echo $root_dir; ?>/admin/remark/global/vendor/jquery-placeholder/jquery.placeholder.js"></script>
        <script src="<?php echo $root_dir; ?>/admin/remark/global/vendor/bootstrap-table/bootstrap-table.min.js"></script>
        <script src="<?php echo $root_dir; ?>/admin/remark/global/vendor/bootstrap-table/extensions/mobile/bootstrap-table-mobile.js"></script>
        <script src="<?php echo $root_dir; ?>/admin/remark/global/vendor/summernote/summernote.min.js"></script>
        <script src="<?php echo $root_dir; ?>/admin/remark/global/vendor/bootstrap-datepicker/bootstrap-datepicker.js"></script>
        <script src="<?php echo $root_dir; ?>/admin/remark/global/vendor/timepicker/jquery.timepicker.min.js"></script>
        <script src="<?php echo $root_dir; ?>/admin/remark/global/vendor/aspaginator/jquery-asPaginator.min.js"></script>
        <script src="<?php echo $root_dir; ?>/admin/remark/global/js/Plugin/aspaginator.js"></script>
        <script src="<?php echo $root_dir; ?>/admin/remark/global/js/Plugin/responsive-tabs.js"></script>
        <script src="<?php echo $root_dir; ?>/admin/remark/global/js/Plugin/tabs.js"></script>
        <script src="<?php echo $root_dir; ?>/admin/remark/global/vendor/select2/select2.full.min.js"></script>
        <script src="<?php echo $root_dir; ?>/admin/remark/global/vendor/bootstrap-select/bootstrap-select.js"></script>
    
    <!-- Scripts -->
    <script src="<?php echo $root_dir; ?>/admin/remark/global/js/Component.js"></script>
    <script src="<?php echo $root_dir; ?>/admin/remark/global/js/Plugin.js"></script>
    <script src="<?php echo $root_dir; ?>/admin/remark/global/js/Base.js"></script>
    <script src="<?php echo $root_dir; ?>/admin/remark/global/js/Config.js"></script>
    
    <script src="<?php echo $root_dir; ?>/admin/remark/base/assets/js/Section/Menubar.js"></script>
    <script src="<?php echo $root_dir; ?>/admin/remark/base/assets/js/Section/GridMenu.js"></script>
    <script src="<?php echo $root_dir; ?>/admin/remark/base/assets/js/Section/Sidebar.js"></script>
    <script src="<?php echo $root_dir; ?>/admin/remark/base/assets/js/Section/PageAside.js"></script>
    <script src="<?php echo $root_dir; ?>/admin/remark/base/assets/js/Plugin/menu.js"></script>
    
    <script src="<?php echo $root_dir; ?>/admin/remark/global/js/config/colors.js"></script>
    <script src="<?php echo $root_dir; ?>/admin/remark/base/assets/js/config/tour.js"></script>
    <script>Config.set('assets', '<?php echo $root_dir; ?>/admin/remark/base/assets');</script>
    
    <!-- Page -->
    <script src="<?php echo $root_dir; ?>/admin/remark/base/assets/js/Site.js"></script>
    <script src="<?php echo $root_dir; ?>/admin/remark/global/js/Plugin/asscrollable.js"></script>
    <script src="<?php echo $root_dir; ?>/admin/remark/global/js/Plugin/slidepanel.js"></script>
    <script src="<?php echo $root_dir; ?>/admin/remark/global/js/Plugin/switchery.js"></script>
    <script src="<?php echo $root_dir; ?>/admin/remark/global/js/Plugin/matchheight.js"></script>
    <script src="<?php echo $root_dir; ?>/admin/remark/global/js/Plugin/jvectormap.js"></script>

    <script src="<?php echo $root_dir; ?>/admin/remark/base/assets/examples/js/dashboard/v1.js"></script>

    <script src="<?php echo $root_dir; ?>/admin/remark/global/js/Plugin/dropify.js"></script>
    <script src="<?php echo $root_dir; ?>/admin/remark/base/assets/examples/js/forms/uploads.js"></script>

    <script src="<?php echo $root_dir; ?>/admin/remark/global/js/Plugin/jquery-placeholder.js"></script>
    <script src="<?php echo $root_dir; ?>/admin/remark/global/js/Plugin/material.js"></script>
    <script src="<?php echo $root_dir; ?>/admin/remark/base/assets/examples/js/tables/bootstrap.js"></script>

    <script src="<?php echo $root_dir; ?>/admin/remark/global/js/Plugin/summernote.js"></script>

    <script src="<?php echo $root_dir; ?>/admin/remark/base/assets/examples/js/forms/editor-summernote.js"></script>
    <script src="<?php echo $root_dir; ?>/admin/remark/global/js/Plugin/select2.js"></script>
    <script src="<?php echo $root_dir; ?>/admin/remark/global/js/Plugin/bootstrap-select.js"></script>
    <script src="<?php echo $root_dir; ?>/admin/remark/base/assets/examples/js/forms/advanced.js"></script>
    
    <script>
      (function(document, window, $){
        'use strict';
    
        var Site = window.Site;
        $(document).ready(function(){
          Site.run();

          var homeURL = $('#home-url').html();

          /* For Wysiwyg Icons */
          var summertime = {
            "note-icon-magic": "wb-text",
            "note-icon-caret": "wb-chevron-down-mini",
            "note-icon-bold": "wb-bold",
            "note-icon-underline": "wb-underline",
            "note-icon-italic": "wb-italic",
            "note-icon-eraser": "wb-format-clear",
            "note-icon-unorderedlist": "wb-list-bulleted",
            "note-icon-orderedlist": "wb-list-numbered",
            "note-icon-table": "wb-table",
            "note-icon-link": "wb-link",
            "note-icon-picture": "wb-image",
            "note-icon-video": "wb-video",
            "note-icon-arrows-alt": "wb-expand",
            "note-icon-code": "wb-code-unfold",
            "note-icon-question": "wb-help",
          };
          $.each( summertime, function( key, value ) {
            $('.' + key).addClass(value);
            $('.' + value).removeClass(key);
          });

          /* For Wysiwyg Form Reset */
          $('.admin-wysiwyg').on('click', '.admin-wysiwyg-reset', function(e) {
            var wysiwyg = $(this).closest('.admin-wysiwyg');
            wysiwyg.find($('.panel-body')).html( wysiwyg.find($('.admin-wysiwyg-original')).html() );
            wysiwyg.find($('.panel-codable')).val( wysiwyg.find($('.admin-wysiwyg-original')).html() );
          });

          /* For Wysiwyg Form Submit */
          $('.admin-wysiwyg').on('click', '.admin-wysiwyg-submit', function(e) {
            if (confirm("Are you sure you want to overwrite the old content?")) {
              var wysiwyg = $(this).closest('.admin-wysiwyg');
              var wysiwygContent = wysiwyg.find($('.panel-body')).html();
              if ( wysiwyg.find($('.note-editor')).hasClass('codeview') == 'true' ) {
                wysiwygContent = wysiwyg.find($('.panel-codable')).val(); }           
              wysiwyg.find($('.admin-wysiwyg-textarea')).val(wysiwygContent);
              wysiwyg.find($('.admin-wysiwyg-form')).submit();
            }
          });

          /* For images submit */
          $('.form-images').on('click', '.submit-images', function(e) {
            // if (confirm("Are you sure you want to overwrite the old values?")) {
              var formImages = $(this).closest('.form-images');
              var newInputElements = '';
              var count = 1;
              formImages.find('.dropify-render img').each(function( index ) {
                newInputElements += "<input name='input-image-" + count + "' class='d-none' type='text' value='" +  $(this).attr("src") + "' />\n";
                count++;
              });
              formImages.append(newInputElements).submit();
            // }
          });

          /* Add dropify in settings */
          $('#home-banner-settings').on('click', '.home-banner-item:not(.d-none):last', function(e) {
            $(this).next().removeClass('d-none');
          });

          /* Toggle custom checkbox name attribute */
          $('form').on('click', '.checkbox-custom', function(e) {
            if ($(this).find('input[type=checkbox]').prop('checked')) {
              $(this).find('input[type=checkbox]').attr('name', $(this).find('input[type=checkbox]').attr('data-name'));
            } else {
              $(this).find('input[type=checkbox]').removeAttr('name');
            }
          });

          /* Add row in subjects handled in edit teacher modal */
          $('.teacher-subjects-handled-plus').on('click', function(e) {
            var table = $(this).closest('.teacher-subjects-handled-table');
            var string = '<tr><td class="px-20 text-left">' +
              table.find('.teacher-subjects-handled-class').find(':selected').text() + '</td><td>' +
              table.find('.teacher-subjects-handled-subject').find(':selected').text() + '</td><td>' +
              '<a class="teacher-subjects-handled-delete" href="#"><i class="icon wb-trash p-5" style="color: #76838f;" aria-hidden="true"></i></a>' + 
              '<input type="text" class="d-none" name="subjectshandled[]" value="' +
              table.find('.teacher-subjects-handled-class').find(':selected').val() + '-' +
              table.find('.teacher-subjects-handled-subject').find(':selected').val() + '"></td></tr>';
            $(string).insertBefore( table.find('.teacher-subjects-handled-class').closest('tr') );
            table.find('.teacher-subjects-handled-no-records').hide();
          });

          /* Delete row in subjects handled in edit teacher modal */
          $('.teacher-subjects-handled-table').on('click', '.teacher-subjects-handled-delete', function(e) {
            $(this).closest('tr').remove();
          });

          // Hide alert after 2 seconds
          $(".alert").delay(1000).fadeOut( 2000, function() {
              $(this).hide();
          });
          $('.alert').mouseover(function () { if($(this).is(':animated')) {
              $(this).stop().animate({opacity:'100'});
          }});

        });
      })(document, window, jQuery);
    </script>

  </body>
</html>
