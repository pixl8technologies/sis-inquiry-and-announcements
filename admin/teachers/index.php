<?php include("../header.php"); ?>

    <!-- Page -->
    <div class="page view-all-page">
      <div class="page-header">
        <h1 class="page-title">Teacher Accounts</h1>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="<?php echo $root_dir; ?>/">Home</a></li>
          <li class="breadcrumb-item"><a href="<?php echo $root_dir; ?>/admin">Admin</a></li>
          <li class="breadcrumb-item">Portal</li>
          <li class="breadcrumb-item active">Teachers</li>
        </ol>
       <div class="page-header-actions d-none">
        <form class="form-inline d-inline-block mr-10">
        <div class="form-group footable-filtering-search">
          <div class="input-group">
            <input type="text" class="form-control" placeholder="Search" style="padding: 18px;">
            <div class="input-group-btn">
              <button type="button" class="btn btn-primary"><i class="wb-search" aria-hidden="true"></i></button>
            </div>
          </div>
        </div>
        </form>
        </div>
      </div>
      <div class="page-content container-fluid">
        <div class="row" data-plugin="matchHeight" data-by-row="true">

          <div class="col-md-5">
            <!-- Panel Accordion -->
            <div class="panel">
              <div class="panel-body collapse show">

                <!-- Add Teacher -->
                <?php
                if (isset($_POST['add_teacher_username'])) {
                  $sql = "INSERT INTO teacher (first_name, last_name, middle_name, username, password, email, contact_number, user_type, default_pw) VALUES ('" . 
                    mysqli_real_escape_string( $dbCon , $_POST['add_teacher_first_name'] ) . "', '" .
                    mysqli_real_escape_string( $dbCon , $_POST['add_teacher_last_name'] ) . "', '" .
                    mysqli_real_escape_string( $dbCon , $_POST['add_teacher_middle_name'] ) . "', '" .
                    mysqli_real_escape_string( $dbCon , $_POST['add_teacher_username'] ) . "', '" .
                    mysqli_real_escape_string( $dbCon , $_POST['add_teacher_password'] ) . "', '" .
                    mysqli_real_escape_string( $dbCon , $_POST['add_teacher_email'] ) . "', '" .
                    mysqli_real_escape_string( $dbCon , $_POST['add_teacher_contact_number'] ) . "', 2, 1)";
                  if ( !(mysqli_query($dbCon, $sql)) ) { ?>
                  <div class="alert alert-warning alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    ERROR : <?php echo mysqli_error($dbCon); ?>
                  </div>
                  <?php } else { ?>
                  <div class="alert alert-success alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    SUCCESS : Teacher <strong><?php echo $_POST['add_teacher_first_name']; ?></strong> has been added.
                  </div>
                <?php } } ?>

                <h4 class="mb-25">Add a Teacher Account</h4>

                <form action="" method="post">
                  <div class="row text-left">
                    <div class="col-md-12">
                      <label class="form-control-label">Full Name</label>
                    </div>
                    <div class="col-md-4 form-group">
                      <input type="text" class="form-control" name="add_teacher_first_name" placeholder="First Name">
                    </div>
                    <div class="col-md-4 form-group">
                      <input type="text" class="form-control" name="add_teacher_middle_name" placeholder="Middle Name">
                    </div>
                    <div class="col-md-4 form-group">
                      <input type="text" class="form-control" name="add_teacher_last_name" placeholder="Last Name">
                    </div>
                    <div class="col-md-7 form-group">
                      <input type="email" class="form-control" name="add_teacher_email" placeholder="Email">
                    </div>
                    <div class="col-md-5 form-group">
                      <input type="text" class="form-control" name="add_teacher_contact_number" placeholder="Contact No.">
                    </div>
                    <div class="col-md-12">
                      <label class="form-control-label">Login Details</label>
                    </div>
                    <div class="col-md-6 form-group">
                      <input type="text" class="form-control" name="add_teacher_username" placeholder="Username" required>
                    </div>
                    <div class="col-md-6 form-group">
                      <input type="text" class="form-control" value="password" disabled >
                      <input type="text" class="d-none" name="add_teacher_password" value="password" >
                    </div>
                    <div class="col-md-12">
                      <label class="form-control-label">Subjects handling</label>
                    </div>
                    <div class="col-md-12">
                      <table class="table table-hover table-bordered table-striped table-sm text-center teacher-subjects-handled-table">
                        <thead>
                          <tr>
                            <th class="py-10">Class</th>
                            <th class="py-10">Subject</th>
                            <th class="py-10" style="min-width: 65px;">Actions</th>
                          </tr>
                        </thead>
                        <tbody class="teacher-subjects-handled-tbody">
                          <tr class="teacher-subjects-handled-no-records"><td colspan="999" class="text-center">No records.</td></tr>
                          <tr>
                            <td>
                              <select class="teacher-subjects-handled-class form-control" data-plugin="select2">
                                <?php $result2 = mysqli_query($dbCon, "SELECT * FROM class"); if (mysqli_num_rows($result2) > 0) { while($row = mysqli_fetch_assoc($result2)) { ?>
                                <option value="<?= $row['class_id'] ?>"><?= $row['class_name'] ?></option>
                                <?php } } ?>
                              </select>
                            </td>
                            <td>
                              <select class="teacher-subjects-handled-subject form-control" data-plugin="select2">
                                <?php $result3 = mysqli_query($dbCon, "SELECT * FROM subject"); if (mysqli_num_rows($result3) > 0) { while($row = mysqli_fetch_assoc($result3)) { ?>
                                <option value="<?= $row['subject_id'] ?>"><?= $row['subject_name'] ?></option>
                                <?php } } ?>
                              </select>
                            </td>
                            <td><a class="teacher-subjects-handled-plus" href="javascript:void(0)"><i class="icon wb-plus p-5" style="color: #76838f;" aria-hidden="true"></i></a></td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                    <div class="col-md-12 text-right">
                      <button class="btn btn-default mr-10" data-dismiss="modal" type="button">Cancel</button>
                      <button class="btn btn-primary" type="submit">Save</button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
            <!-- End Panel Accordion -->
          </div>

          <div class="col-md-7">
            <!-- Panel Accordion -->
            <div class="panel">
              <div class="panel-body collapse show">

                <!-- Edit Teacher -->
                <?php
                if (isset($_POST['edit_teacher'])) {
                  $error = "";
                  $sql = "UPDATE teacher SET " .
                    "first_name='" . $_POST['edit_teacher_first_name'] . "', " .
                    "middle_name='" . $_POST['edit_teacher_middle_name'] . "', " .
                    "last_name='" . $_POST['edit_teacher_last_name'] . "', " .
                    "email='" . $_POST['edit_teacher_email'] . "', " .
                    "contact_number='" . $_POST['edit_teacher_contact_number'] . "', " .
                    "username='" . $_POST['edit_teacher_username'] . "', " .
                    
                    "default_pw=0 WHERE teacher_id=" . $_POST['edit_teacher'];
                  if ( !(mysqli_query($dbCon, $sql)) ) $error .= mysqli_error($dbCon);
                 
                  if(isset($_POST['edit_teacher_password'])){
                    
                    $password = "password";
                    $sql = "UPDATE teacher SET password = 'password' WHERE teacher_id =" .$_POST['edit_teacher'];
                    if ( !(mysqli_query($dbCon, $sql)) ) $error .= mysqli_error($dbCon);
                  }
                  $sql = "DELETE FROM subject_teacher_class_relationship WHERE teacher_id=" . $_POST['edit_teacher'];
                  if ( !(mysqli_query($dbCon, $sql)) ) $error .= mysqli_error($dbCon);

                  if (isset($_POST['subjectshandled'])) {
                    $sql = "INSERT INTO subject_teacher_class_relationship (subject_id, teacher_id, class_id) VALUES ";
                    foreach ($_POST['subjectshandled'] as $key => $value) {
                      $sql .= "(" . explode( '-', $value )[1] . "," . $_POST['edit_teacher'] . "," . explode( '-', $value )[0] . "), ";
                    }
                    $sql = substr( $sql, 0, (strlen($sql) - 2) );
                    if ( !(mysqli_query($dbCon, $sql)) ) $error .= mysqli_error($dbCon);
                  }
                  if ( $error !== "" ) { ?>
                  <div class="alert alert-warning alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    ERROR : <?php echo $error; ?>
                  </div>
                  <?php } else { ?>
                  <div class="alert alert-success alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    SUCCESS : Teacher <strong><?php echo $_POST['edit_teacher_first_name']; ?></strong> (ID = <?php echo $_POST['edit_teacher']; ?>) data has been updated.
                  </div>
                  <?php }
                  ?>
                <?php } ?>

                <!-- Delete Teacher -->
                <?php
                if (isset($_POST['delete_teacher'])) {
                  $sql = "DELETE FROM teacher WHERE teacher_id=" . $_POST['delete_teacher'];
                  if ( !(mysqli_query($dbCon, $sql)) ) { ?>
                  <div class="alert alert-warning alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    ERROR : <?php echo mysqli_error($dbCon); ?>
                  </div>
                  <?php } else { ?>
                  <div class="alert alert-success alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    SUCCESS : Teacher <strong><?php echo $_POST['delete_teacher_name']; ?></strong> (ID = <?php echo $_POST['delete_teacher']; ?>) has been deleted.
                  </div>
                <?php } } ?>

                <h4 class="mb-25">All Teacher Accounts</h4>

                <table class="table table-hover table-bordered table-striped toggle-arrow-tiny table-sm text-center" id="teacher-delete-table">
                  <thead>
                    <tr>
                      <th class="py-10">ID</th>
                      <th class="py-10">Name</th>
                      <th class="py-10">Username</th>
                      <th class="py-10">Password</th>
                      <th class="py-10" style="min-width: 65px;">Actions</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php
                    $result = mysqli_query($dbCon, "SELECT * FROM teacher ORDER BY last_name");
                    if (mysqli_num_rows($result) > 0) { while($row_teachers = mysqli_fetch_assoc($result)) { ?>
                    <tr>
                      <td><?php echo $row_teachers['teacher_id'];?></td>
                      <td data-toggle="modal" data-target="#teacher-edit-modal-<?php echo $row_teachers['teacher_id'];?>" class="cursor-pointer py-10 px-25 text-left"><?php echo $row_teachers['last_name'];?>, <?php echo $row_teachers['first_name'];?> <?php echo $row_teachers['middle_name'];?></td>
                      <td data-toggle="modal" data-target="#teacher-edit-modal-<?php echo $row_teachers['teacher_id'];?>" class="cursor-pointer py-10 px-25"><?php echo $row_teachers['username'];?></td>
                      <td data-toggle="modal" data-target="#teacher-edit-modal-<?php echo $row_teachers['teacher_id'];?>" class="cursor-pointer py-10 px-25"><?php echo $row_teachers['password'] === 'password' ? 'default' : '********';?></td>
                      <td>
                        <a data-toggle="modal" data-target="#teacher-edit-modal-<?php echo $row_teachers['teacher_id'];?>" href="javascript:void(0)"><i class="icon wb-pencil p-5" style="color: #76838f;" aria-hidden="true"></i></a>
                        <!-- Modal -->
                        <div class="modal fade modal-fade-in-scale-up" id="teacher-edit-modal-<?php echo $row_teachers['teacher_id'];?>" aria-hidden="false" role="dialog" tabindex="-1">
                          <div class="modal-dialog modal-simple">
                            <form class="modal-content" action="" method="post">
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                  <span aria-hidden="true">×</span>
                                </button>
                                <h4 class="modal-title">Edit Teacher</h4>
                              </div>
                              <div class="modal-body">
                                <div class="row text-left">
                                  <input type="text" class="d-none" name="edit_teacher" value="<?php echo $row_teachers['teacher_id'];?>">
                                  <div class="col-md-12">
                                    <label class="form-control-label">Full Name</label>
                                  </div>
                                  <div class="col-md-4 form-group">
                                    <input type="text" class="form-control" name="edit_teacher_first_name" placeholder="First Name" value="<?php echo $row_teachers['first_name'];?>">
                                  </div>
                                  <div class="col-md-4 form-group">
                                    <input type="text" class="form-control" name="edit_teacher_middle_name" placeholder="Middle Name" value="<?php echo $row_teachers['middle_name'];?>">
                                  </div>
                                  <div class="col-md-4 form-group">
                                    <input type="text" class="form-control" name="edit_teacher_last_name" placeholder="Last Name" value="<?php echo $row_teachers['last_name'];?>">
                                  </div>
                                  <div class="col-md-7 form-group">
                                    <input type="email" class="form-control" name="edit_teacher_email" placeholder="Email" value="<?php echo $row_teachers['email'];?>">
                                  </div>
                                  <div class="col-md-5 form-group">
                                    <input type="text" class="form-control" name="edit_teacher_contact_number" placeholder="Contact No." value="<?php echo $row_teachers['contact_number'];?>">
                                  </div>
                                  <div class="col-md-12">
                                    <label class="form-control-label">Login Details</label>
                                  </div>
                                  <div class="col-md-6 form-group">
                                    <input type="text" class="form-control" name="edit_teacher_username" placeholder="Username" value="<?php echo $row_teachers['username'];?>" required>
                                  </div>
                                  <div class="col-md-6 form-group">
                                    <div class="checkbox-custom checkbox-primary">
                                      <input name="edit_teacher_password" type="checkbox" value='1' id="edit_teacher_password" />
                                      <label for="edit_teacher_password">Reset Password</label>
                                    </div>
                                  </div>
                                  <div class="col-md-12">
                                    <label class="form-control-label">Subjects handling</label>
                                  </div>
                                  <div class="col-md-12">
                                    <table class="table table-hover table-bordered table-striped table-sm text-center teacher-subjects-handled-table">
                                      <thead>
                                        <tr>
                                          <th class="py-10">Class</th>
                                          <th class="py-10">Subject</th>
                                          <th class="py-10" style="min-width: 65px;">Actions</th>
                                        </tr>
                                      </thead>
                                      <tbody class="teacher-subjects-handled-tbody">
                                        <?php
                                        $result4 = mysqli_query($dbCon, "SELECT * FROM subject_teacher_class_relationship WHERE teacher_id=".$row_teachers['teacher_id']);
                                        if (mysqli_num_rows($result4) > 0) { while($row_subjects_handled = mysqli_fetch_assoc($result4)) { ?>
                                        <tr>
                                          <td class="px-20 text-left">
                                            <?php $result5 = mysqli_query($dbCon, "SELECT class_name FROM class WHERE class_id=".$row_subjects_handled['class_id']);
                                            echo mysqli_fetch_assoc($result5)['class_name']; ?>
                                          </td>
                                          <td>
                                            <?php $result6 = mysqli_query($dbCon, "SELECT subject_name FROM subject WHERE subject_id=".$row_subjects_handled['subject_id']);
                                            echo mysqli_fetch_assoc($result6)['subject_name']; ?>
                                          </td>
                                          <td>
                                            <a class="teacher-subjects-handled-delete" href="javascript:void(0)"><i class="icon wb-trash p-5" style="color: #76838f;" aria-hidden="true"></i></a>
                                            <input type="text" class="d-none" name="subjectshandled[]" value="<?php echo $row_subjects_handled['class_id'] . '-' . $row_subjects_handled['subject_id']; ?>">
                                          </td>
                                        </tr>
                                        <?php } } else { ?>
                                        <tr class="teacher-subjects-handled-no-records"><td colspan="999" class="text-center">No records.</td></tr>
                                        <?php } ?>
                                        <tr>
                                          <td>
                                            <select class="teacher-subjects-handled-class form-control" sdata-plugin="select2">
                                              <?php $result2 = mysqli_query($dbCon, "SELECT * FROM class"); if (mysqli_num_rows($result2) > 0) { while($row = mysqli_fetch_assoc($result2)) { ?>
                                              <option value="<?= $row['class_id'] ?>"><?= $row['class_name'] ?></option>
                                              <?php } } ?>
                                            </select>
                                          </td>
                                          <td>
                                            <select class="teacher-subjects-handled-subject form-control" sdata-plugin="select2">
                                              <?php $result3 = mysqli_query($dbCon, "SELECT * FROM subject"); if (mysqli_num_rows($result3) > 0) { while($row = mysqli_fetch_assoc($result3)) { ?>
                                              <option value="<?= $row['subject_id'] ?>"><?= $row['subject_name'] ?></option>
                                              <?php } } ?>
                                            </select>
                                          </td>
                                          <td><a class="teacher-subjects-handled-plus" href="javascript:void(0)"><i class="icon wb-plus p-5" style="color: #76838f;" aria-hidden="true"></i></a></td>
                                        </tr>
                                      </tbody>
                                    </table>
                                  </div>
                                  <div class="col-md-12 text-right">
                                    <button class="btn btn-default mr-10" data-dismiss="modal" type="button">Cancel</button>
                                    <button class="btn btn-primary" type="submit">Save</button>
                                  </div>
                                </div>
                              </div>
                            </form>
                          </div>
                        </div>
                        <!-- End Modal -->
                        <form action="" method="post" class="form-button" >
                          <input type="text" class="d-none" name="delete_teacher" value="<?php echo $row_teachers['teacher_id'];?>">
                          <input type="text" class="d-none" name="delete_teacher_name" value="<?php echo $row_teachers['first_name'];?>">
                          <button type="submit"><i class="icon wb-trash p-5" style="color: #76838f;" aria-hidden="true"></i></button>
                        </form>
                      </td>
                    </tr>
                    <?php } } else { ?>
                    <tr><td colspan="999" class="text-center">No records.</td></tr>
                    <?php } ?>
                  </tbody>
                </table>

              </div>
            </div>
            <!-- End Panel Accordion -->
          </div>

        </div>
      </div>
    </div>
    <!-- End Page -->

<?php include("../footer.php"); ?>