<?php include("../header.php"); ?>

    <!-- Page -->
    <div class="page view-all-page">
      <div class="page-header">
        <h1 class="page-title">Contact</h1>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="<?php echo $root_dir; ?>/">Home</a></li>
          <li class="breadcrumb-item"><a href="<?php echo $root_dir; ?>/admin">Admin</a></li>
          <li class="breadcrumb-item active">Contact</li>
        </ol>
        <div class="page-header-actions">
          <button type="button" class="btn btn-sm btn-icon btn-default btn-outline btn-round"
            data-toggle="tooltip" data-original-title="Edit">
            <i class="icon wb-pencil" aria-hidden="true"></i>
          </button>
          <button type="button" class="btn btn-sm btn-icon btn-default btn-outline btn-round"
            data-toggle="tooltip" data-original-title="Refresh">
            <i class="icon wb-refresh" aria-hidden="true"></i>
          </button>
          <button type="button" class="btn btn-sm btn-icon btn-default btn-outline btn-round"
            data-toggle="tooltip" data-original-title="Setting">
            <i class="icon wb-settings" aria-hidden="true"></i>
          </button>
        </div>
      </div>
      <div class="page-content container-fluid">
        <div class="row" data-plugin="matchHeight" data-by-row="true">
          <div class="col-lg-12">

            <?php if (isset($_POST['submit'])){
            $nemail= mysqli_real_escape_string($dbCon, $_POST['contact_email']);
            $nmap= mysqli_real_escape_string($dbCon, $_POST['post_desc']);
            $sql ="INSERT INTO general_settings (gs_id, gs_contactUs_recepient_email, gs_map_iframe) Values (1,'$nemail','$nmap') ON DUPLICATE KEY UPDATE gs_contactUs_recepient_email='$nemail', gs_map_iframe='$nmap'";
            if( !(mysqli_query($dbCon, $sql)) ) { ?>
            <div class="alert alert-warning alert-dismissible" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>ERROR : <?php echo mysqli_error($dbCon); ?>
            </div>
            <?php } else { ?>
            <div class="alert alert-success alert-dismissible" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>SUCCESS : The Contact-Us has been updated.
            </div>
            <?php } } ?>
         
            <div class="row">

              <div class="col-md-7">
                <!-- Panel Accordion -->
                <div class="panel">
                  <div class="panel-body">
                    <?php
                    $sql = "SELECT * FROM general_settings WHERE gs_id=1";
                    $result = mysqli_query($dbCon, $sql);
                    if (mysqli_num_rows($result) > 0) { $row = mysqli_fetch_assoc($result); }
                    else $row = array("gs_contactUs_recepient_email"=>"", "gs_map_iframe");
                    $receiver = $row['gs_contactUs_recepient_email'];
                    $imap = isset($row['gs_map_iframe']) ? $row['gs_map_iframe'] : "";
                    ?>
                    <form method='post' action="<?php echo $_SERVER['PHP_SELF']; ?>">
                      <div class="form-group form-material floating" data-plugin="formMaterial">
                        <input type="email" class="form-control" name="contact_email" value="<?php echo $receiver ?>" />
                        <label class="floating-label">Email</label>
                      </div>
                      <div class="form-group form-material floating" data-plugin="formMaterial">
                        <textarea class="form-control" rows="7" name="post_desc"><?php echo $imap ?></textarea>
                        <label class="floating-label">Map iframe code</label>
                      </div>
                      <div class="col-md-12 text-right">
                        <div class="form-group">
                          <button type="reset" class="btn btn-default mr-10">Reset</button>
                          <button name="submit" type="submit" class="btn btn-primary">Save</button>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
                <!-- End Panel Accordion -->
              </div>

              <div class="col-md-5">
                <div class="panel">
                  <div class="panel-body">
                    <h4>Notes</h4>
                    <p>The email address set in this page will receive inquiries sent through the <a href="<?php echo $root_dir; ?>/contact-us">contact us page</a> form.</p>
                    <p>The <a href="<?php echo $root_dir; ?>/contact-us">contact us page</a> also displays a map from google which can be set in this page. To edit the map,
                    (1) go to <a href="https://www.google.com/maps" target="_blank">Google Maps</a>.
                    (2) Zoom in to the preferred location and click on it.
                    (3) Click menu on the top left corner > Share or embed > Embed a map.
                    (4) Copy the html and paste it here.</p>
                  </div>
                </div>
              </div>

            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- End Page -->

<?php include("../footer.php"); ?>