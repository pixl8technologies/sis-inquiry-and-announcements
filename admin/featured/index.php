<?php include("../header.php"); ?>

    <!-- Page -->
    <div class="page view-all-page">
      <div class="page-header">
        <h1 class="page-title">Featured Article</h1>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="<?php echo $root_dir; ?>/">Home</a></li>
          <li class="breadcrumb-item"><a href="<?php echo $root_dir; ?>/admin">Admin</a></li>
          <li class="breadcrumb-item">Home Page Settings</li>
          <li class="breadcrumb-item active">Featured Article</li>
        </ol>
      </div>
      
      <div class="page-content container-fluid">
        <div class="row" data-plugin="matchHeight" data-by-row="true">
        
          <div class="col-md-5">
            <div class="panel">
              <div class="panel-body">
              
                <div class="row">
                  <!-- Save image to uploads folder then set url in database -->
                  <?php if( isset($_POST['input-image-1']) ) {
                    $sql = "INSERT INTO general_settings (gs_id, gs_home_featured_img_url) VALUES ";
                    if (substr($_POST['input-image-1'], 0, 10) === 'data:image') {
                      $base64 = explode('base64,', $_POST['input-image-1'])[1];
                      $filename = 'home-featured-img.' . explode('/', explode(';base64,', $_POST['input-image-1'])[0])[1];
                    } else {
                      $base64 = base64_encode(file_get_contents( '../../assets/uploads/' . explode('/uploads/', $_POST['input-image-1'])[1] ));
                      $filename = 'home-featured-img.' . explode('.', $_POST['input-image-1'])[ count(explode('.', $_POST['input-image-1'])) - 1 ];
                    }
                    file_put_contents('../../assets/uploads/' . $filename, base64_decode($base64));
                    $sql .= "(1, '" . $filename . "') ON DUPLICATE KEY UPDATE gs_home_featured_img_url = '" . $filename . "';";
                    if ( !(mysqli_query($dbCon, $sql)) ) { ?>
                      <div class="col-lg-12">
                        <div class="alert alert-warning alert-dismissible" role="alert">
                          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                          ERROR : <?php echo mysqli_error($dbCon); ?>
                        </div>
                      </div>
                    <?php } else { ?>
                      <div class="col-lg-12">
                        <div class="alert alert-success alert-dismissible" role="alert">
                          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                          SUCCESS : The featured image has been set.
                        </div>
                      </div>
                    <?php }
                  } ?>
                </div>

                <form action="" method="post" class="form-images">
                  <div class="form-group">
                    <h5>Left side image</h5>
                    <?php $sql = "SELECT * FROM general_settings WHERE gs_id=1";
                      $result = mysqli_query($dbCon, $sql);
                      if (mysqli_num_rows($result) > 0) { $roww = mysqli_fetch_assoc($result); } ?>
                    <input class="input-image" type="file" data-plugin="dropify" data-max-file-size="5M" data-height="300" data-default-file="<?php echo isset($roww['gs_home_featured_img_url']) ? $root_dir . '/assets/uploads/' . $roww['gs_home_featured_img_url'] : ''; ?>"/>
                  </div>
                  <div class="form-group text-right">
                    <a href="" class="btn btn-default mr-10">Reset</a>
                    <a href="javascript:void(0)" class="submit-images btn btn-primary">Save</a>
                  </div>
                </form>

              </div>
            </div>
          </div>

          <div class="col-md-7">
            <div class="panel">
              <div class="panel-body">
              
                <h5>Right side text contents</h5>

                <!-- Insert/Update the database and display the result -->
                <div class="row">
                  <?php if( isset($_POST['admin-wysiwyg-textarea']) ) {
                    $sql = "INSERT INTO general_settings (gs_id, gs_home_featured_content) VALUES (1, '";
                    $sql .= mysqli_real_escape_string( $dbCon , $_POST['admin-wysiwyg-textarea'] );
                    $sql .= "') ON DUPLICATE KEY UPDATE gs_home_featured_content='";
                    $sql .= mysqli_real_escape_string( $dbCon , $_POST['admin-wysiwyg-textarea'] );
                    $sql .= "'";
                    if ( !(mysqli_query($dbCon, $sql)) ) { ?>
                      <div class="col-lg-12">
                        <div class="alert alert-warning alert-dismissible" role="alert">
                          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                          ERROR : <?php echo mysqli_error($dbCon); ?>
                        </div>
                      </div>
                      <?php } else { ?>
                      <div class="col-lg-12">
                        <div class="alert alert-success alert-dismissible" role="alert">
                          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                          SUCCESS : The featured article content has been updated.
                        </div>
                      </div>
                    <?php } } ?>
                </div>
                
                <!-- The wysiwyg form -->
                <div class="row">
                  <div class="admin-wysiwyg col-lg-12" data-php="<?php echo $root_dir; ?>/featured/featured.php">
                    <div data-plugin="summernote" data-plugin-options='{"toolbar":[["style", ["bold", "italic", "underline", "clear"]],["color", ["color"]],["para", ["ul", "ol", "paragraph"]]]}'><?php
                      $sql = "SELECT gs_home_featured_content FROM general_settings WHERE gs_id=1";
                      $result = mysqli_query($dbCon, $sql);
                      if (mysqli_num_rows($result) > 0) { $row = mysqli_fetch_assoc($result); }
                      else $row = array("gs_home_featured_content"=>"");
                      echo $row['gs_home_featured_content'];
                      ?></div>
                    <div class="form-group text-right">
                      <button class="admin-wysiwyg-reset btn btn-default mr-10">Reset</button>
                      <button class="admin-wysiwyg-submit btn btn-primary">Save</button>
                    </div>
                    <div class="admin-wysiwyg-original d-none"><?php echo $row['gs_home_featured_content']; ?></div>
                    <form class="admin-wysiwyg-form d-none" action="" method="post">
                      <textarea name="admin-wysiwyg-textarea" class="admin-wysiwyg-textarea"></textarea>
                    </form>
                  </div>
                </div>

              </div>
            </div>
          </div>
          
        </div>
      </div>
    </div>
    <!-- End Page -->

<?php include("../footer.php"); ?>