<?php
session_start();
include ('../config.php');
include ('../db_connection.php');
if(isset($_SESSION['username'])){
    $userName = $_SESSION['username'];
    $utypee = $_SESSION['user_type'];
    }else{
      header("Location: ../log-in/index.php?notloggedin");
    }
if(isset($_POST['submit'])){
	$pwd = mysqli_real_escape_string($dbCon, $_POST['newPass']);
	$pwd2 = mysqli_real_escape_string($dbCon, $_POST['newPassConfirm']);
	//check if both fields are empty
	if (empty($pwd2) || empty ($pwd)){
		//nooo
		header("Location: ../log-in/changepwd_index.php?login=emptyfield");
		exit();
		}		
	else {
		//check if passwords are the same
		if ($pwd != $pwd2){
			header("Location: ../log-in/changepwd_index.php?login=unmatched");
			exit();
		} else {
			//query three tables
			$tables = array("", "admin", "teacher", "student");
			$sql = "SELECT password, default_pw FROM " . $tables[$utypee] . " WHERE username = $userName";
			
			$result = mysqli_query($dbCon, $sql);
			$row = mysqli_fetch_assoc($result);
			$hashedPwd = password_hash($pwd2, PASSWORD_BCRYPT); //verify against db password
			$defpw = 0;
			$queryChange = mysqli_query($dbCon, "UPDATE " . $tables[$utypee] . " SET password = '$hashedPwd',default_pw = '$defpw' WHERE username='$userName'");
			if($utypee == 1){
				header("Location: ../admin/");
			}else if($utypee == 2){
				header("Location: ../teachers/");
			}else if($utypee == 3){
				header("Location: ../students/");
			}
		}
	}
} else {
	header("Location: ../log-in/index.php?submitbuttonfail");
	exit();
}
?>