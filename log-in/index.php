<?php include("../partials/header.php"); ?>

    <section id="log-in" class="b-grey">
      <div class="container py-5">
        <h2 class="text-center">Information Portal</h2>
        <div class="row" style="max-width: 350px; margin: 60px auto;">
        	<div class="col-12">
          	 <form action="../includes/signin_include.php" method="post">
              <div class="form-group">
                <label for="form-username">Username</label>
                <input type="text" class="form-control" id="form-username" name="username" value="">
              </div>
              <div class="form-group">
                <label for="form-password">Password</label>
                <input type="password" class="form-control" id="form-password" name="password" value="">
              </div>
              <?php if( isset($_GET["login"]) ) { ?>
              <div class="form-group">
                <p class="text-danger"><?php
                  if ($_GET["login"] === 'noDatainDB') echo 'Username is not registered.';
                  else if ($_GET["login"] === 'error') echo 'Wrong password.';
                  else if ($_GET["login"] === 'emptyfield') echo 'Oops! Empty fields.'; ?></p>
              </div>
              <?php } ?>
              <div class="text-right mt-4"><button type="submit" name="submit" class="btn btn-primary bg-theme-color px-4">Login</button></div>
            </form>
       		</div>
    	</div>
      </div>
    </section>

<?php include("../partials/footer.php"); ?>