<?php
  session_start();
  include ('../config.php');
  include ('../db_connection.php');
  if(isset($_SESSION['username'])){
    $userName = $_SESSION['username'];
    $utypee = $_SESSION['user_type'];
  }else{
    header("Location: ../log-in/?notloggedin");
  }

include("../partials/header.php"); ?>

    <section id="log-in" class="b-grey">
      <div class="container py-5">
        <h2 class="text-center">Information Portal</h2>
        <div class="row" style="max-width: 350px; margin: 60px auto;">
        	<div class="col-12">
          	 <form action="changepwd.php" method="post">
              <div class="form-group">
                <p class="text-success">This is a default account. Set its password.</p>
              </div>
              <div class="form-group">
                <label for="form-username">New Password</label>
                <input type="password" class="form-control" id="form-username" name="newPass" placeholder="Enter your new password" value="">
              </div>
              <div class="form-group">
                <label for="form-password">Password Confirmation</label>
                <input type="password" class="form-control" id="form-password" name="newPassConfirm" placeholder="Re-enter your new password" value="">
              </div>
              <?php if( isset($_GET["login"]) ) { ?>
              <div class="form-group">
                <p class="text-danger"><?php
                  if ($_GET["login"] === 'emptyfield') echo 'Oops! Empty fields.';
                  if ($_GET["login"] === 'unmatched') echo 'Passwords don\'t match.'; ?></p>
              </div>
              <?php } ?>
              <div class="text-right mt-4"><button type="submit" name="submit" class="btn btn-primary bg-theme-color px-4">Login</button></div>
            </form>
       		</div>
    	</div>
      </div>
    </section>

<?php include("../partials/footer.php"); ?>