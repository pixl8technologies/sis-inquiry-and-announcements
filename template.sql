

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";

CREATE TABLE `admin` (
  `admin_id` int primary key auto_increment NOT NULL,
  `username` varchar(25) NOT NULL,
  `password` varchar(90) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `middle_name` varchar(50) NOT NULL,
  `user_type` int(1) NOT NULL DEFAULT '1',
  `default_pw` int(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `admin` (`username`, `password`, `last_name`, `first_name`, `middle_name`, `user_type`, `default_pw`) VALUES
 ('admin',  'password', 'Admin',  'Admin',  'Admin',  1, 1),
 ('admin1', 'password', 'Admin1', 'Admin1', 'Admin1', 1, 1),
 ('admin2', 'password', 'Admin2', 'Admin2', 'Admin2', 1, 1),
 ('admin3', 'password', 'Admin3', 'Admin3', 'Admin3', 1, 1),
 ('admin4', 'password', 'Admin4', 'Admin4', 'Admin4', 1, 1),
 ('admin5', 'password', 'Admin5', 'Admin5', 'Admin5', 1, 1);

CREATE TABLE `class` (
  `class_id` int primary key auto_increment NOT NULL,
  `class_name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `student` (
  `student_id` int primary key auto_increment NOT NULL,
  `first_name` text NOT NULL,
  `last_name` text NOT NULL,
  `middle_name` text NOT NULL,
  `username` varchar(25) NOT NULL,
  `password` varchar(90) NOT NULL,
  `email` varchar(50) NOT NULL,
  `contact_number` varchar(20) NOT NULL,
  `user_type` int(1) NOT NULL DEFAULT '3',
  `default_pw` int(1) NOT NULL DEFAULT '1',
  `class_id` int NOT NULL,
  foreign key (`class_id`) REFERENCES class(`class_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `student_class_relationship` (
  `studentclassr_id` int primary key auto_increment NOT NULL,
  `class_id` int NOT NULL,
  foreign key (`class_id`) REFERENCES class(`class_id`),
  `student_id` int NOT NULL,
  foreign key (`student_id`) REFERENCES student(`student_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `teacher` (
  `teacher_id` int primary key auto_increment NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `middle_name` varchar(50) NOT NULL,
  `username` varchar(25) NOT NULL,
  `password` varchar(60) NOT NULL,
  `email` varchar(50) NOT NULL,
  `contact_number` varchar(20) NOT NULL,
  `user_type` int(1) NOT NULL DEFAULT '2',
  `default_pw` int(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `subject` (
  `subject_id` int primary key auto_increment NOT NULL,
  `subject_name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `subject_class_relationship` (
  `subjectclassr_id` int primary key auto_increment NOT NULL,
  `subject_id` int NOT NULL,
  foreign key (`subject_id`) REFERENCES subject(`subject_id`),
  `class_id` int NOT NULL,
  foreign key (`class_id`) REFERENCES class(`class_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `subject_teacher_class_relationship` (
  `subjectteacherr_id` int primary key auto_increment NOT NULL,
  `subject_id` int NOT NULL,
  foreign key (`subject_id`) REFERENCES subject(`subject_id`),
  `teacher_id` int NOT NULL,
  foreign key (`teacher_id`) REFERENCES teacher(`teacher_id`),
  `class_id` int NOT NULL,
  foreign key (`class_id`) REFERENCES class(`class_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `general_settings` (
  `gs_id` int primary key auto_increment NOT NULL,
  `gs_name` varchar(50) NOT NULL,
  `gs_logo_url` varchar(50) NOT NULL,
  `gs_logo_alt_name` varchar(50) NOT NULL,
  `gs_founded_content` varchar(500) NOT NULL,
  `gs_address` varchar(50) NOT NULL,
  `gs_contact_number` varchar(50) NOT NULL,
  `gs_facebook_link` varchar(50) NOT NULL,
  `gs_twitter_link` varchar(50) NOT NULL,
  `gs_instagram_link` varchar(50) NOT NULL,
  `gs_about_history_content` blob NOT NULL,
  `gs_about_mv_content` blob NOT NULL,
  `gs_about_corevalues_content` blob NOT NULL,
  `gs_about_administration_content` blob NOT NULL,
  `gs_about_personnel_content` blob NOT NULL,
  `gs_map_iframe` blob NOT NULL,
  `gs_contactUs_recepient_email` varchar(50) NOT NULL,
  `gs_home_featured_img_url` blob NOT NULL,
  `gs_home_featured_content` blob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `grade` (
  `grade_id` int primary key auto_increment NOT NULL,
  `student_id` int NOT NULL,
  `teacher_id` int NOT NULL,
  `subject_id` int NOT NULL,
  `grade_value` varchar(100) NOT NULL,
  `term` varchar(50) NOT NULL,
  foreign key (`student_id`) REFERENCES student(`student_id`),
  foreign key (`teacher_id`) REFERENCES teacher(`teacher_id`),
  foreign key (`subject_id`) REFERENCES subject(`subject_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `image_carousel` (
  `img_id` int primary key auto_increment NOT NULL,
  `img_url` varchar(50) NOT NULL,
  `img_order_number` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `post` (
  `post_id` int primary key auto_increment NOT NULL,
  `post_title` varchar(100) NOT NULL,
  `post_short_description` varchar(500) NOT NULL,
  `post_content` text NOT NULL,
  `post_type` varchar(2) NOT NULL,
  `post_date_start` date NOT NULL,
  `post_date_end` date NOT NULL,
  `post_date_created` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




