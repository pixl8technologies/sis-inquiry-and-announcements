(function($) {
  "use strict"; // Start of use strict

  // Smooth scrolling using jQuery easing
  $('a.js-scroll-trigger[href*="#"]:not([href="#"])').click(function() {
    if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
      if (target.length) {
        $('html, body').animate({
          scrollTop: (target.offset().top - 54)
        }, 1000, "easeInOutExpo");
        return false;
      }
    }
  });

  // Closes responsive menu when a scroll trigger link is clicked
  $('.js-scroll-trigger').click(function() {
    $('.navbar-collapse').collapse('hide');
  });

  // Activate scrollspy to add active class to navbar items on scroll
  $('body').scrollspy({
    target: '#mainNav',
    offset: 54
  });

  // Change map is loading statement after the map is loaded.
  $(window).on('load', function() {
    $('#contact-us').find('#map-is-loading').addClass('d-none');
    $('#contact-us').find('#map-is-loaded').removeClass('d-none');
  });

  // Add min-height to page body
  if ( $('body > .page-content').outerHeight() < window.innerHeight - ( $('body > footer').outerHeight() + $('body > .navbar').outerHeight() ) )
    $('body > .page-content > section').css('min-height', window.innerHeight - ( $('body > footer').outerHeight() + $('body > .navbar').outerHeight() ));
  $('body').css('margin-top', $('body > .navbar').outerHeight());

})(jQuery); // End of use strict
  