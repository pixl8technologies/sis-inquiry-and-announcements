<?php

// Create database if it doesn't exist
try { $mysqli = new mysqli($servername, $dbusername, $dbpassword); }
catch (\Exception $e) { echo $e->getMessage(), PHP_EOL; }
if ($mysqli->select_db($dbname) === false) {
	$conn = mysqli_connect($servername, $dbusername, $dbpassword);
	$sql = "CREATE DATABASE `" . $dbname . "`";
	if (!($conn->query($sql) === TRUE)) {
		echo "Error creating database: " . $conn->error;
		die();
	} else {
		$filename = $_SERVER['DOCUMENT_ROOT'] . $root_dir . '/template.sql';
		$sql = '';
		$lines = file($filename);
		foreach ($lines as $line) {
			if (substr($line, 0, 2) == '--' || $line == '') continue;
			$sql .= $line;
			if (substr(trim($line), -1, 1) == ';') {
				mysqli_select_db($conn, $dbname) or print('Error performing query \'<strong>' . $sql . '\': ' . mysqli_error($conn) . '<br /><br />');
				mysqli_query($conn, $sql) or print('Error performing query \'<strong>' . $sql . '\': ' . mysqli_error($conn) . '<br /><br />');
				$sql = '';
			}
		}
	}
}

$dbCon = mysqli_connect($servername, $dbusername, $dbpassword, $dbname);

if (mysqli_connect_errno()){
	echo "Failed to connect" . mysqli_connect_error();
}