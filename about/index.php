<?php include("../partials/header.php"); ?>

<section id="about">

  <header id="about-header" class="bg-grey">
    <div class="container text-center">
      <h2 class="py-5 mt-5">About</h2>
      <ul class="nav nav-tabs d-block">
        <li><a class="p-1 pt-4 mx-4 active" data-toggle="tab" href="#about-history">History</a></li>
        <li><a class="p-1 pt-4 mx-4" data-toggle="tab" href="#about-mission-vision">Mission & Vision</a></li>
        <li><a class="p-1 pt-4 mx-4" data-toggle="tab" href="#about-core-values">Core Values</a></li>
        <li><a class="p-1 pt-4 mx-4" data-toggle="tab" href="#about-administration">Administration</a></li>
        <li><a class="p-1 pt-4 mx-4" data-toggle="tab" href="#about-personnel">Personnel</a></li>
      </ul>
    </div>
  </header>

  <div class="container">
    <div class="tab-content">

      <div id="about-history" class="tab-pane active">
        <div class="py-5">
          <?php
          $sql = "SELECT gs_about_history_content FROM general_settings WHERE gs_id=1";
          $result = mysqli_query($dbCon, $sql);
          if (mysqli_num_rows($result) > 0) { $row = mysqli_fetch_assoc($result); }
          else $row = array("gs_about_history_content"=>"");
          echo $row['gs_about_history_content'];
          ?>        
        </div>
      </div>

      <div id="about-mission-vision" class="tab-pane">
        <div class="py-5">
          <?php
          $sql = "SELECT gs_about_mv_content FROM general_settings WHERE gs_id=1";
          $result = mysqli_query($dbCon, $sql);
          if (mysqli_num_rows($result) > 0) { $row = mysqli_fetch_assoc($result); }
          else $row = array("gs_about_mv_content"=>"");
          echo $row['gs_about_mv_content'];
          ?>
        </div>
      </div>

      <div id="about-core-values" class="tab-pane">
        <div class="py-5">
          <?php
          $sql = "SELECT gs_about_corevalues_content FROM general_settings WHERE gs_id=1";
          $result = mysqli_query($dbCon, $sql);
          if (mysqli_num_rows($result) > 0) { $row = mysqli_fetch_assoc($result); }
          else $row = array("gs_about_corevalues_content"=>"");
          echo $row['gs_about_corevalues_content'];
          ?>
        </div>
      </div>

      <div id="about-administration" class="tab-pane">
        <div class="py-5">
          <?php
          $sql = "SELECT gs_about_administration_content FROM general_settings WHERE gs_id=1";
          $result = mysqli_query($dbCon, $sql);
          if (mysqli_num_rows($result) > 0) { $row = mysqli_fetch_assoc($result); }
          else $row = array("gs_about_administration_content"=>"");
          echo $row['gs_about_administration_content'];
          ?>
        </div>
      </div>

      <div id="about-personnel" class="tab-pane">
        <div class="py-5">
          <?php
          $sql = "SELECT gs_about_personnel_content FROM general_settings WHERE gs_id=1";
          $result = mysqli_query($dbCon, $sql);
          if (mysqli_num_rows($result) > 0) { $row = mysqli_fetch_assoc($result); }
          else $row = array("gs_about_personnel_content"=>"");
          echo $row['gs_about_personnel_content'];
          ?>
        </div>
      </div>

    </div>
  </div>
  
</section>

<?php include("../partials/footer.php"); ?>