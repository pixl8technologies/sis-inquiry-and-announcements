<?php include("partials/header.php"); ?>

  <header id="home-banner">
    <div id="demo" class="carousel slide" data-ride="carousel">
      <!-- The slideshow -->
      <div class="carousel-inner">
        <?php
        $sql = "SELECT * FROM image_carousel ORDER BY img_order_number";
        $result = mysqli_query($dbCon, $sql);
        $index = 0;
        if (mysqli_num_rows($result) > 0) { while($row = mysqli_fetch_assoc($result)) { ?>
        <div class="carousel-item<?php echo $index == 0 ? ' active' : ''; ?>">
          <div class="img-container" style="background-image: url(<?php echo $root_dir; ?>/assets/uploads/<?php echo $row["img_url"]; ?>)"></div>
        </div>
        <?php $index++; } } ?>
      </div>
      <!-- Indicators -->
      <ul class="carousel-indicators">
        <?php for ($x = 0; $x < $index; $x++) { ?>
        <li data-target="#demo" data-slide-to="<?php echo $x; ?>" class="<?php echo ($x == 0) ? ' active' : ''; ?>"></li>
        <?php } ?>
      </ul>
      <!-- Left and right controls -->
      <a class="carousel-control-prev" href="#demo" data-slide="prev">
        <span class="fa fa-chevron-left"></span>
      </a>
      <a class="carousel-control-next" href="#demo" data-slide="next">
        <span class="fa fa-chevron-right"></span>
      </a>
    </div>
  </header> 

  <section id="home-featured" class="py-5">
    <?php
    $sql = "SELECT * FROM general_settings WHERE gs_id=1";
    $result = mysqli_query($dbCon, $sql);
    if (mysqli_num_rows($result) > 0) { $row2 = mysqli_fetch_assoc($result); }
    ?>
    <div class="container">
    <h2 class="text-center">Featured</h2>
      <div class="row">
        <div class="col-lg-6">
          <img src="<?php echo $root_dir; ?>/assets/uploads/<?php echo $row2["gs_home_featured_img_url"]; ?>" alt="" class="img-thumbnail img-fluid">
        </div> 
        <div class="col-lg-6">
          <p class="lead"><?php echo $row2["gs_home_featured_content"] ?>
        </div>
      </div>
    </div>
  </section>

  <section id="home-announcements" class="py-5">
    <div class="container">
      <h2 class="text-center">Announcements</h2>
      <div class="row">
        <?php
        $sqlAn = "SELECT * FROM post WHERE post_type='an' LIMIT 6";
        $resultAns = mysqli_query($dbCon, $sqlAn);
        $Anscount = 0;
        while ($ans = mysqli_fetch_array($resultAns)){ ?>

        <div class="col-lg-4 mx-auto mt-4">
          <h5 class="text-center mb-0"><?php echo $ans['post_title']; ?></h5>
          <div class="lead mb-1"><small><?php echo date_format(date_create($ans['post_date_start']),"F j, Y"); ?></small></div>
          <p class="lead"><?php echo $ans['post_short_description'] !== '' ? $ans['post_short_description'] : $ans['post_content']; ?></p>
        </div>

        <?php $anscount++; } if ($anscount == 0) { ?>
        <div class="col-lg-6 mx-auto mb-4">
          <p class="lead text-center">No announcements available.</p>
        </div> 
        <?php } ?>
      </div>
      <div class="row lead text-center">
        <div class="col-12">
          See all in the <a class="color-theme-color" href="<?php echo $root_dir; ?>/calendar">calendar</a>.
        </div>
      </div>
    </div>
  </section>

  <section id="home-news" class="py-5">
    <div class="container">
      <h2 class="text-center">Latest Events</h2>
      <div class="row">
        <?php
        $sqlEv = "SELECT * FROM post WHERE post_type='ev' LIMIT 6";
        $resultEvs = mysqli_query($dbCon, $sqlEv);
        $Evscount = 0;
        while ($evs = mysqli_fetch_array($resultEvs)){ ?>

        <div class="col-lg-4 mx-auto mt-4">
          <div class="row">
            <div class="col-lg-4">
              <img class="img-fluid" src="<?php echo $root_dir; ?>/assets/news.png" alt="">
            </div>
            <div class="col-lg-8">
              <h5 class="mb-0"><?php echo $evs['post_title']; ?></h5>
              <div class="lead mb-1"><small><?php echo date_format(date_create($evs['post_date_start']),"F j, Y"); ?></small></div>
              <p class="lead"><?php echo $evs['post_short_description'] !== '' ? $evs['post_short_description'] : $evs['post_content']; ?></p>
            </div>
          </div>
        </div>

        <?php $evscount++; } if ($evscount == 0) { ?>
        <div class="col-lg-6 mx-auto mb-4">
          <p class="lead text-center">No announcements available.</p>
        </div>
        <?php } ?>
      </div>
      <div class="row lead text-center">
        <div class="col-12">
          See all in the <a class="color-theme-color" href="<?php echo $root_dir; ?>/calendar">calendar</a>.
        </div>
      </div>
    </div>
  </section>

<?php include("partials/footer.php"); ?>