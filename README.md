# Student Information System, Inquiry and Announcement System for Paco Catholic School
#### Created by Pixel8Technologies

## Installation:
1. Create a `config.php` file and copy the sample `config.php.sample` to it.
2. Change the `$root_dir` value to `""` if the web root of your website is the current directory.
3. Create a database if `sis-inquiry-and-announcements`is not there yet and import `template.sql`to it. Don't forget to change the `wp-config.php`with the correct database info.
4. Open the website and login using `admin` as username and `password` as password. You will be asked to change this to your new password.