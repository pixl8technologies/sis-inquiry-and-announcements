<?php 
error_reporting(E_ALL &~ E_NOTICE);
session_start();
if(isset($_SESSION['u_id'])){
  if($_SESSION['user_type'] == 3){
    $userId = $_SESSION['u_id'];
      $fName = $_SESSION['u_fname'];
  }else{
    header("Location: ../log-in/index.php?notastudent");
  }
}else{
  header("Location: ../log-in/index.php?notloggedin");
}
include("../partials/header.php"); ?>



    <section id="responsive-tab" style="padding-top:25px;">
      <div class="container">
      <div class="row">
        <div class="col-md-2">
           <ul id="tabs" class="nav nav-tabs nav-tabs--vertical nav-tabs--left" role="tablist">

              <li class="nav-item">
                  <a id="tab-A" href="#pane-A" class="nav-link active" data-toggle="tab" role="tab">1st Quarter</a>
              </li>
              <li class="nav-item">
                  <a id="tab-B" href="#pane-B" class="nav-link" data-toggle="tab" role="tab">2nd Quarter</a>
              </li>
              <li class="nav-item">
                  <a id="tab-C" href="#pane-C" class="nav-link" data-toggle="tab" role="tab">3rd Quarter</a>
              </li>
              <li class="nav-item">
                  <a id="tab-D" href="#pane-D" class="nav-link" data-toggle="tab" role="tab">4th Quarter</a>
              </li>
          </ul>
        </div>

        <div class="col-md-9">
       <div id="content" class="tab-content" role="tablist">
        <div id="pane-A" class="card tab-pane fade show active" role="tabpanel" aria-labelledby="tab-A">
          <div class="card-header" role="tab" id="heading-A">
            <h5 class="mb-0">
              <a data-toggle="collapse" href="#collapse-A" data-parent="#content" aria-expanded="true" aria-controls="collapse-A">
                1st Quarter
              </a>
            </h5>
          </div>
          <div id="collapse-A" class="collapse show" role="tabpanel" aria-labelledby="heading-A">
            <div class="card-body">
              <h4> 1Q </h4>
              <table class="table" id="t01">
              <thead>
                <tr>
                  <th> </th>
                  <th> Teacher</th>
                  <th> Grades</th>
                </tr>
              </thead>
              <?php
              $sql = mysqli_query($dbCon, "SELECT * FROM student WHERE student_id = $_SESSION[u_id]") or die(mysqli_error($dbCon));
              $result_sql = mysqli_fetch_assoc($sql);
              $student_class = $result_sql['class_id'];
              $sql2 = mysqli_query($dbCon, "SELECT * FROM subject_class_relationship WHERE class_id = $student_class");
              if(mysqli_num_rows($sql2)>0){
                while($row1 = mysqli_fetch_assoc($sql2)){

              ?> 
              <tbody>
                <tr>
                  <!-- SUBJECT NAME -->
                  <td> 
                  <?php
                  $sql3 = mysqli_query($dbCon, "SELECT * FROM subject WHERE subject_id = $row1[subject_id]");
                  $result3 = mysqli_fetch_assoc($sql3);
                  $subject_name = $result3['subject_name'];
                  echo $subject_name;
                  ?></td>

                  <!-- TEACHER NAME -->
                  <td>
                  <?php
                  $sql4 = mysqli_query($dbCon, "SELECT teacher_id FROM subject_teacher_class_relationship WHERE subject_id = $row1[subject_id] AND class_id = $student_class");
                  $result4 = mysqli_fetch_assoc($sql4);
                  $teacher_id = $result4['teacher_id'];
                  if(isset($teacher_id)){
                    $sql5 = mysqli_query($dbCon, "SELECT * FROM teacher WHERE teacher_id = $teacher_id");
                  $result5 = mysqli_fetch_assoc($sql5);
                  $teacher_fname = $result5['first_name'];
                  $teacher_lname = $result5['last_name'];
                  echo $teacher_lname;
                  echo ", ";
                  echo $teacher_fname;
                  }else{
                    echo "no professors tagged";
                  }
                  
                  ?>
                  
                  </td>
                  <!-- GRADE -->
                    <td >
                      <?php
                      $sqla = mysqli_query($dbCon, "SELECT teacher_id FROM subject_teacher_class_relationship WHERE subject_id = $row1[subject_id] AND class_id = $student_class");
                      $resulta = mysqli_fetch_assoc($sqla);
                      $teacher_ida = $result4['teacher_id'];
                      $sql6 = mysqli_query($dbCon, "SELECT grade_value FROM grade WHERE student_id = $_SESSION[u_id] AND subject_id = $row1[subject_id] AND term = '1'") or die(mysqli_error($dbCon));
                      if(mysqli_num_rows($sql6)==0){
                        echo "NOT SET";
                      }else{
                        $result6 = mysqli_fetch_assoc($sql6);
                        $grade_value = $result6['grade_value'];
                        if($grade_value == 0){
                          
                          echo "NOT SET";
                        }else{
                          if(isset($teacher_id)){
                            echo $grade_value;
                          }else{
                            echo "NOT SET";
                          }
                          
                        }  
                      }
                      ?>
                    </td>
                </tr><?php }} ?>
              </tbody>
             </table>
            </div>
          </div>
        </div>

        <div id="pane-B" class="card tab-pane fade" role="tabpanel" aria-labelledby="tab-B">
          <div class="card-header" role="tab" id="heading-B">
            <h5 class="mb-0">
              <a class="collapsed" data-toggle="collapse" href="#collapse-B" data-parent="#content" aria-expanded="false" aria-controls="collapse-B">
                2nd Quarter
              </a>
            </h5>
          </div>
          <div id="collapse-B" class="collapse" role="tabpanel" aria-labelledby="heading-B">
            <div class="card-body">
              <h4> 2Q </h4>
              <table class="table" id="t01">
              <thead>
                <tr>
                  <th> </th>
                  <th> Teacher</th>
                  <th> Grades</th>
                </tr>
              </thead> 
              <?php
              $sql = mysqli_query($dbCon, "SELECT * FROM student WHERE student_id = $_SESSION[u_id]") or die(mysqli_error($dbCon));
              $result_sql = mysqli_fetch_assoc($sql);
              $student_class = $result_sql['class_id'];
              $sql2 = mysqli_query($dbCon, "SELECT * FROM subject_class_relationship WHERE class_id = $student_class");
              if(mysqli_num_rows($sql2)>0){
                while($row1 = mysqli_fetch_assoc($sql2)){

              ?> 
              <tbody>
                <tr>
                  <!-- SUBJECT NAME -->
                  <td> 
                  <?php
                  $sql3 = mysqli_query($dbCon, "SELECT * FROM subject WHERE subject_id = $row1[subject_id]");
                  $result3 = mysqli_fetch_assoc($sql3);
                  $subject_name = $result3['subject_name'];
                  echo $subject_name;
                  ?></td>

                  <!-- TEACHER NAME -->
                  <td>
                  <?php
                  $sql4 = mysqli_query($dbCon, "SELECT teacher_id FROM subject_teacher_class_relationship WHERE subject_id = $row1[subject_id] AND class_id = $student_class");
                  $result4 = mysqli_fetch_assoc($sql4);
                  $teacher_id = $result4['teacher_id'];
                  if(isset($teacher_id)){
                    $sql5 = mysqli_query($dbCon, "SELECT * FROM teacher WHERE teacher_id = $teacher_id");
                  $result5 = mysqli_fetch_assoc($sql5);
                  $teacher_fname = $result5['first_name'];
                  $teacher_lname = $result5['last_name'];
                  echo $teacher_lname;
                  echo ", ";
                  echo $teacher_fname;
                  }else{
                    echo "no professors tagged";
                  }
                  
                  ?>
                  
                  </td>
                  <!-- GRADE -->
                    <td >
                      <?php
                      $sqla = mysqli_query($dbCon, "SELECT teacher_id FROM subject_teacher_class_relationship WHERE subject_id = $row1[subject_id] AND class_id = $student_class");
                      $resulta = mysqli_fetch_assoc($sqla);
                      $teacher_ida = $result4['teacher_id'];
                      $sql6 = mysqli_query($dbCon, "SELECT grade_value FROM grade WHERE student_id = $_SESSION[u_id] AND subject_id = $row1[subject_id] AND term = '2'") or die(mysqli_error($dbCon));
                      if(mysqli_num_rows($sql6)==0){
                        echo "NOT SET";
                      }else{
                        $result6 = mysqli_fetch_assoc($sql6);
                        $grade_value = $result6['grade_value'];
                        if($grade_value == 0){
                          
                          echo "NOT SET";
                        }else{
                          if(isset($teacher_id)){
                            echo $grade_value;
                          }else{
                            echo "NOT SET";
                          }
                        }
                      }
                      ?>
                    </td>
                </tr><?php }} ?>
              </tbody>
             </table>
            </div>
          </div>
        </div>

        <div id="pane-C" class="card tab-pane fade" role="tabpanel" aria-labelledby="tab-C">
          <div class="card-header" role="tab" id="heading-C">
            <h5 class="mb-0">
              <a class="collapsed" data-toggle="collapse" href="#collapse-C" data-parent="#content" aria-expanded="false" aria-controls="collapse-C">
                3rd Quarter
              </a>
            </h5>
          </div>
          <div id="collapse-C" class="collapse" role="tabpanel" aria-labelledby="heading-C">
            <div class="card-body">
              <h4> 3Q </h4>
              <table class="table" id="t01">
              <thead>
                <tr>
                  <th> </th>
                  <th> Teacher</th>
                  <th> Grades</th>
                </tr>
              </thead> 
              <?php
              $sql = mysqli_query($dbCon, "SELECT * FROM student WHERE student_id = $_SESSION[u_id]") or die(mysqli_error($dbCon));
              $result_sql = mysqli_fetch_assoc($sql);
              $student_class = $result_sql['class_id'];
              $sql2 = mysqli_query($dbCon, "SELECT * FROM subject_class_relationship WHERE class_id = $student_class");
              if(mysqli_num_rows($sql2)>0){
                while($row1 = mysqli_fetch_assoc($sql2)){

              ?> 
              <tbody>
                <tr>
                  <!-- SUBJECT NAME -->
                  <td> 
                  <?php
                  $sql3 = mysqli_query($dbCon, "SELECT * FROM subject WHERE subject_id = $row1[subject_id]");
                  $result3 = mysqli_fetch_assoc($sql3);
                  $subject_name = $result3['subject_name'];
                  echo $subject_name;
                  ?></td>

                  <!-- TEACHER NAME -->
                  <td>
                  <?php
                  $sql4 = mysqli_query($dbCon, "SELECT teacher_id FROM subject_teacher_class_relationship WHERE subject_id = $row1[subject_id] AND class_id = $student_class");
                  $result4 = mysqli_fetch_assoc($sql4);
                  $teacher_id = $result4['teacher_id'];
                  if(isset($teacher_id)){
                    $sql5 = mysqli_query($dbCon, "SELECT * FROM teacher WHERE teacher_id = $teacher_id");
                  $result5 = mysqli_fetch_assoc($sql5);
                  $teacher_fname = $result5['first_name'];
                  $teacher_lname = $result5['last_name'];
                  echo $teacher_lname;
                  echo ", ";
                  echo $teacher_fname;
                  }else{
                    echo "no professors tagged";
                  }
                  
                  ?>
                  
                  </td>
                  <!-- GRADE -->
                    <td >
                      <?php
                      $sqla = mysqli_query($dbCon, "SELECT teacher_id FROM subject_teacher_class_relationship WHERE subject_id = $row1[subject_id] AND class_id = $student_class");
                      $resulta = mysqli_fetch_assoc($sqla);
                      $teacher_ida = $result4['teacher_id'];
                      $sql6 = mysqli_query($dbCon, "SELECT grade_value FROM grade WHERE student_id = $_SESSION[u_id] AND subject_id = $row1[subject_id] AND term = '3'") or die(mysqli_error($dbCon));
                      if(mysqli_num_rows($sql6)==0){
                        echo "NOT SET";
                      }else{
                        $result6 = mysqli_fetch_assoc($sql6);
                        $grade_value = $result6['grade_value'];
                        if($grade_value == 0){
                          
                          echo "NOT SET";
                        }else{
                          if(isset($teacher_id)){
                            echo $grade_value;
                          }else{
                            echo "NOT SET";
                          }
                        }
                      }
                      ?>
                    </td>
                </tr><?php }} ?>
              </tbody>
             </table>
            </div>
          </div>
        </div>

        <div id="pane-D" class="card tab-pane fade" role="tabpanel" aria-labelledby="tab-D">
          <div class="card-header" role="tab" id="heading-D">
            <h5 class="mb-0">
              <a class="collapsed" data-toggle="collapse" href="#collapse-D" data-parent="#content" aria-expanded="false" aria-controls="collapse-D">
                4th Quarter
              </a>
            </h5>
          </div>
          <div id="collapse-D" class="collapse" role="tabpanel" aria-labelledby="heading-D">
            <div class="card-body">
              <h4> 4Q </h4>
              <table class="table" id="t01">
              <thead>
                <tr>
                  <th> </th>
                  <th> Teacher</th>
                  <th> Grades</th>
                </tr>
              </thead> 
              <?php
              $sql = mysqli_query($dbCon, "SELECT * FROM student WHERE student_id = $_SESSION[u_id]") or die(mysqli_error($dbCon));
              $result_sql = mysqli_fetch_assoc($sql);
              $student_class = $result_sql['class_id'];
              $sql2 = mysqli_query($dbCon, "SELECT * FROM subject_class_relationship WHERE class_id = $student_class");
              if(mysqli_num_rows($sql2)>0){
                while($row1 = mysqli_fetch_assoc($sql2)){

              ?> 
              <tbody>
                <tr>
                  <!-- SUBJECT NAME -->
                  <td> 
                  <?php
                  $sql3 = mysqli_query($dbCon, "SELECT * FROM subject WHERE subject_id = $row1[subject_id]");
                  $result3 = mysqli_fetch_assoc($sql3);
                  $subject_name = $result3['subject_name'];
                  echo $subject_name;
                  ?></td>

                  <!-- TEACHER NAME -->
                  <td>
                  <?php
                  $sql4 = mysqli_query($dbCon, "SELECT teacher_id FROM subject_teacher_class_relationship WHERE subject_id = $row1[subject_id] AND class_id = $student_class");
                  $result4 = mysqli_fetch_assoc($sql4);
                  $teacher_id = $result4['teacher_id'];
                  if(isset($teacher_id)){
                    $sql5 = mysqli_query($dbCon, "SELECT * FROM teacher WHERE teacher_id = $teacher_id");
                  $result5 = mysqli_fetch_assoc($sql5);
                  $teacher_fname = $result5['first_name'];
                  $teacher_lname = $result5['last_name'];
                  echo $teacher_lname;
                  echo ", ";
                  echo $teacher_fname;
                  }else{
                    echo "no professors tagged";
                  }
                  
                  ?>
                  
                  </td>
                  <!-- GRADE -->
                    <td >
                      <?php
                      $sqla = mysqli_query($dbCon, "SELECT teacher_id FROM subject_teacher_class_relationship WHERE subject_id = $row1[subject_id] AND class_id = $student_class");
                      $resulta = mysqli_fetch_assoc($sqla);
                      $teacher_ida = $result4['teacher_id'];
                      $sql6 = mysqli_query($dbCon, "SELECT grade_value FROM grade WHERE student_id = $_SESSION[u_id] AND subject_id = $row1[subject_id] AND term = '4'") or die(mysqli_error($dbCon));
                      if(mysqli_num_rows($sql6)==0){
                        echo "NOT SET";
                      }else{
                        $result6 = mysqli_fetch_assoc($sql6);
                        $grade_value = $result6['grade_value'];
                        if($grade_value == 0){
                          
                          echo "NOT SET";
                        }else{
                          if(isset($teacher_id)){
                            echo $grade_value;
                          }else{
                            echo "NOT SET";
                          }
                        }
                      }
                      ?>
                    </td>
                </tr><?php }} ?>
              </tbody>
             </table>
            </div>
          </div>
        </div>
    </div>
    </div>
    </div>
    </div>
    </section>

<?php include("../partials/footer.php"); ?>
