<?php 
error_reporting(E_ALL &~ E_NOTICE);
session_start();
if(isset($_SESSION['u_id'])){
	if($_SESSION['user_type'] == 2){
		$userId = $_SESSION['u_id'];
    	$fName = $_SESSION['u_fname'];
	}else{
		header("Location: ../log-in/index.php?notateacher");
	}
}else{
	header("Location: ../log-in/index.php?notloggedin");
}

include("../partials/header.php"); ?>



    <section id="responsive-tab" style="padding-top:25px;">
      <div class="container">
      <div class="row">
        <div class="col-md-2">
           <ul id="tabs" class="nav nav-tabs nav-tabs--vertical nav-tabs--left" role="tablist">
              <li class="nav-item">
                  <a id="tab-settings" href="#pane-settings" class="nav-link" data-toggle="tab" role="tab">User Settings</a>
              </li>
              <?php 
              $subject_count = mysqli_query($dbCon, "SELECT * FROM subject_teacher_class_relationship WHERE teacher_id = $_SESSION[u_id]") or die(mysqli_error($dbCon)); 

              if(mysqli_num_rows($subject_count)>0){
                $y=0;
              	while($row = mysqli_fetch_assoc($subject_count)){ 
              		;?>

              <li class="nav-item">
                  <a id="tab-<?php echo $row['subject_id'] ?>" href="#pane-<?php echo $row['subject_id'] ?>" class="nav-link  <?php if($y==0){
                    echo "active";} ?>" data-toggle="tab" role="tab">
                  	<?php
                  	$subject_name = mysqli_query($dbCon, "SELECT subject_name FROM subject WHERE subject_id = $row[subject_id]") or die(mysqli_error($dbCon));
                  	$row_subjName = mysqli_fetch_assoc($subject_name);
                  	$subject_name = $row_subjName['subject_name'];
                  	echo $subject_name;
                  	?>
                  </a>
              </li>
              <?php  $y = $y+1;}}else{
                echo "NO SUBJECTS TAGGED";
              } ?>
              
          </ul>
        </div>

        <!-- TABLES -->
       	

       	<div class="col-md-9">
        	
       	<div id="content" class="tab-content" role="tablist">
       	<?php 
	      $subject_count = mysqli_query($dbCon, "SELECT * FROM subject_teacher_class_relationship WHERE teacher_id = $_SESSION[u_id]") or die(mysqli_error($dbCon)); 

	      if(mysqli_num_rows($subject_count)>0){
          $x = 0;
	      	while($row = mysqli_fetch_assoc($subject_count)) {
	      	 ?>
        <div id="pane-<?php echo $row['subject_id']; ?>" class="card tab-pane fade <?php if($x==0){echo "show active";} ?>" role="tabpanel" aria-labelledby="tab-<?php echo $row['subject_id']; ?>">
          <div class="card-header" role="tab" id="heading-<?php echo $row['subject_id']; ?>">
            <h5 class="mb-0">
              <a data-toggle="collapse" href="#collapse-<?php echo $row['subject_id']; ?>" data-parent="#content" aria-expanded="true;" aria-controls="collapse-<?php echo $row['subject_id']; ?>">
                <?php

                  	$subject_name = mysqli_query($dbCon, "SELECT subject_name FROM subject WHERE subject_id = $row[subject_id]") or die(mysqli_error($dbCon));
                  	$row_subjName = mysqli_fetch_assoc($subject_name);
                  	$subject_name = $row_subjName['subject_name'];
                  	echo $subject_name;
                  	?>
              </a>
            </h5>
          </div>
			
          <div id="collapse-<?php echo $row['subject_id']; ?>" class="collapse show" role="tabpanel" aria-labelledby="heading-<?php echo $row['subject_id']; ?>">
            <div class="card-body">
              <h4> <?php echo $subject_name; ?> </h4>
              
              <form action="grade_add.php" method="post">
              <table class="table" id="t01">
              <thead>
                <tr>
                  <th> Name </th>
                  <th> Student ID</th>
                  <th> Section </th>
                  <th> 1st Q</th>
                  <th> 2nd Q</th>
                  <th> 3rd Q</th>
                  <th> 4th Q</th>
                </tr>
              </thead> 
              <tbody>
              	<?php 
              	$sqla = "SELECT * FROM student WHERE class_id = $row[class_id] ORDER BY last_name";
              	$result = mysqli_query($dbCon, $sqla) or die (mysqli_error($dbCon));
              	if(mysqli_num_rows($result)>0){
              		while($rowS = mysqli_fetch_assoc($result)) {?>
                <tr>
                  <td><?php echo $rowS['last_name']; ?> , <?php echo $rowS['first_name']; ?> <?php echo $rowS['middle_name']; ?></td>
                  <td> <?php echo $rowS['student_id']; ?> </td>
                  <td> <?php
                  		$classId = $rowS['class_id'];
                  		$sqlb = mysqli_query($dbCon, "SELECT class_name FROM class WHERE class_id = '$classId'");
                  		$row_cname = mysqli_fetch_assoc($sqlb);
                  		$class_name = $row_cname['class_name'];
                  		echo $class_name;	
                  ?> </td>
                  <?php
                  $sqlc = mysqli_query($dbCon, "SELECT * FROM grade WHERE teacher_id = $_SESSION[u_id] AND student_id = $rowS[student_id] AND subject_id = $row[subject_id] AND term = '1' ") or die(mysqli_error($dbCon));
                  $row_grade = mysqli_fetch_assoc($sqlc);
                  $grade_val = $row_grade['grade_value'];
                  ?>
                  <td ><input type="number" name="grade1q[<?php echo $rowS['student_id']; ?>]" value="<?php echo $grade_val; ?>" min="70" max="100"></td>
                  <?php
                  $sqlc = mysqli_query($dbCon, "SELECT * FROM grade WHERE teacher_id = $_SESSION[u_id] AND student_id = $rowS[student_id] AND subject_id = $row[subject_id] AND term = '2' ") or die(mysqli_error($dbCon));
                  $row_grade = mysqli_fetch_assoc($sqlc);
                  $grade_val = $row_grade['grade_value'];
                  ?>
                  <td ><input type="number" name="grade2q[<?php echo $rowS['student_id']; ?>]" value="<?php echo $grade_val; ?>" min="70" max="100"></td>
                  <?php
                  $sqlc = mysqli_query($dbCon, "SELECT * FROM grade WHERE teacher_id = $_SESSION[u_id] AND student_id = $rowS[student_id] AND subject_id = $row[subject_id] AND term = '3' ") or die(mysqli_error($dbCon));
                  $row_grade = mysqli_fetch_assoc($sqlc);
                  $grade_val = $row_grade['grade_value'];
                  ?>
                  <td ><input type="number" name="grade3q[<?php echo $rowS['student_id']; ?>]" value="<?php echo $grade_val; ?>" min="70" max="100"></td>
                  <?php
                  $sqlc = mysqli_query($dbCon, "SELECT * FROM grade WHERE teacher_id = $_SESSION[u_id] AND student_id = $rowS[student_id] AND subject_id = $row[subject_id] AND term = '4' ") or die(mysqli_error($dbCon));
                  $row_grade = mysqli_fetch_assoc($sqlc);
                  $grade_val = $row_grade['grade_value'];
                  ?>
                  <td ><input type="number" name="grade4q[<?php echo $rowS['student_id']; ?>]" value="<?php echo $grade_val; ?>" min="70" max="100">
                      <input type="hidden" name="teacher_id" value="<?php echo $_SESSION['u_id'];?>">
                      <input type="hidden" name="subject_id" value="<?php echo $row['subject_id'];?>">
                  </td>
                </tr>
                
            <?php }} ?>
              </tbody>

             </table>

              <input type="submit" name="submit" value="SAVE">
              
            </form>

            </div>

          </div>
          
        </div>
        
        
		<div id="pane-settings" class="card tab-pane fade" role="tabpanel" aria-labelledby="tab-settings">
          <div id="collapse-setings" class="collapse" role="tabpanel" aria-labelledby="heading-D">
            <div class="card-body">
              <h4> Settings </h4>
              	<!--content here-->
              	
            </div>
          </div>
        </div>
        <?php $x = $x+1;}
        }else{
              echo "<br>";
              echo "<br>";
              echo "<br>";
              echo "<br>";
              echo "NO SUBJECTS TAGGED, PLEASE CONTACT YOUR IT ADMIN";
            } ?>
    </div>
    </div>
    </div>
    </div>
    </section>

<?php include("../partials/footer.php"); ?>
